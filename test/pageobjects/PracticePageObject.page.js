class PracticePageObject {

    get Master() {
        const elem = $("//a[.=' Master ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Practice() {
        const elem = $("//a[text()='Practice']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }


    get AddNewButton() {
        const elem = $(".btn.btn-default.round-btn.float-right")
        elem.waitForDisplayed({ timeout: 80000 });
        return elem
    }
    get PracticeName() {
        const elem = $("//input[@id='name']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get StreetAddress() {
        const elem = $("//input[@id='address']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Suite() {
        const elem = $("//input[@formcontrolname='floor']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get City() {
        const elem = $("//input[@id='city']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Zip() {
        const elem = $("//input[@id='zip']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get PhoneNumber() {

        const elem = $("//input[@id='phone']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Extension() {
        const elem = $("//input[@id='ext_no']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get CellNo() {

        const elem = $("//input[@id='cell_no']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Fax() {

        const elem = $("//input[@id='fax']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Email() {

        const elem = $("//input[@id='email']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get NPINumber() {
        const elem = $("//input[@id='npi_number']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }

    get SameAsProvider() {

        const elem = $("//label[@for=' same_as_provider']//span[@class='fake-input position-absolute']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get TaxID() {
        const elem = $("//span[@for='id']/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get TinNumber() {
        const elem = $("//input[@id='tin_number']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get SignatureTitle() {
        const elem = $("//input[@placeholder='Signature Title*']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get AddLocation() {
        const elem = $("//button[.=' Add Location ']")
        elem.waitForDisplayed({ timeout: 80000 });
        return elem

    }
    get LocationName() {

        const elem = $("(//input[@formcontrolname='name'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get LocationPhoneNumber() {


        const elem = $("(//input[@formcontrolname='phone'])[3]")
        elem.waitForDisplayed({ timeout: 80000 });
        return elem
    }
    get LocationFax() {
        const elem = $("(//input[@formcontrolname='fax'])[3]")
        elem.waitForDisplayed({ timeout: 80000 });
        return elem
    }
    get LocationEmail() {
        const elem = $("(//input[@formcontrolname='email'])[3]")
        elem.waitForDisplayed({ timeout: 80000 });
        return elem

    }
    get LocationStreetAddress() {

        const elem = $("(//input[@formcontrolname='address'])[3]")
        elem.waitForDisplayed({ timeout: 80000 });
        return elem
    }
    get LocationSuite() {

        const elem = $("(//input[@formcontrolname='floor'])[3]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get LocationCity() {
        const elem = $("(//input[@formcontrolname='city'])[3]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem;

    }
    get LocationZip() {

        const elem = $("(//input[@formcontrolname='zip'])[3]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem;
    }
    get SelectDays() {

        const elem = $("//button[.=' Tuesday ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem;
    }
    get SaveAndContinue() {

        const elem = $("(//button[.=' Save & Continue '])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem;
    }
    get FilterPracticeName() {

        const elem = $("//input[@id='FirstName']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem;
    }
    get FilterPhoneNumber() {

        const elem = $("//input[@id='phoneNo']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem;
    }
    get Filter() {

        const elem = $("//button[.=' Filter ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem;
    }
    get Edit(){
        const elem = $("//span[@class='icon-pencil']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem;
        
    }
    get Update() {
        
        const elem = $("//button[.=' Update ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem;
    }





    async MainMenu() {
        await this.Master.click();
        await this.Practice.click();
        await this.AddNewButton.click();
    }
    async AddNewPractice(name, address, suite, city) {
        await this.PracticeName.setValue(name);
        await this.StreetAddress.setValue(address);
        await this.Suite.setValue(suite);
        await this.City.setValue(city)

    }
    async AddPracticeData(zip, phone, ext, cell, fax, email, npi, tin, title) {
        await this.Zip.setValue(zip);
        await this.PhoneNumber.setValue(phone);
        await this.Extension.setValue(ext);
        await this.CellNo.setValue(cell);
        await this.Fax.setValue(fax);
        await this.Email.setValue(email);
        await this.NPINumber.setValue(npi)
        await this.SameAsProvider.click();
        await this.TaxID.click();
        await this.TinNumber.setValue(tin);
        await this.SignatureTitle.setValue(title);

    }
    async moveToElement(element) {
        await element.waitForDisplayed();
        await element.moveTO();

    }
    async AddLocationData(name, phone, fax, email, address, suite, city) {
        await this.LocationName.setValue(name);
        await this.LocationPhoneNumber.setValue(phone);
        await this.LocationFax.setValue(fax);
        await this.LocationEmail.setValue(email);
        await this.LocationStreetAddress.setValue(address);
        await this.LocationSuite.setValue(suite);
        await this.LocationCity.setValue(city);



    }
    async FilterPractice(name, phone) {
        await this.FilterPracticeName.setValue(name);
        await this.FilterPhoneNumber.setValue(phone);
        await this.Filter.click();

    }
    async EditNewPractice(name, address, suite, city) {
        await this.Edit.click();
        await this.PracticeName.clearValue();
        await this.PracticeName.setValue(name);
        await this.StreetAddress.clearValue();
        await this.StreetAddress.setValue(address);
        await this.Suite.clearValue();
        await this.Suite.setValue(suite);
        await this.City.clearValue();
        await this.City.setValue(city)

    }
    async EditPracticeData(zip, phone, ext, cell, fax, email, npi, tin, title) {
        await this.Zip.clearValue();
        await this.Zip.setValue(zip);
        await this.PhoneNumber.clearValue();
        await this.PhoneNumber.setValue(phone);
        await this.Extension.clearValue();
        await this.Extension.setValue(ext);
        await this.CellNo.clearValue();
        await this.CellNo.setValue(cell);
        await this.Fax.clearValue();
        await this.Fax.setValue(fax);
        await this.Email.clearValue();
        await this.Email.setValue(email);
        await this.NPINumber.clearValue();
        await this.NPINumber.setValue(npi)
       // await this.TaxID.click();
        await this.TinNumber.clearValue();
        await this.TinNumber.setValue(tin);
        await this.SignatureTitle.clearValue();
        await this.SignatureTitle.setValue(title);
       

    }
}

module.exports = new PracticePageObject();