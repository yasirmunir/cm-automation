class PlanNameObject {

    get Master() {
        const elem = $("//a[.=' Master ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }

    get Billing() {
        
        const elem = $("//a[.='Billing']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get PlanNameButton() {
        
        const elem = $("//a[.='Plan Name']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get AddNew() {
        
        const elem = $("//button[.='Add New ']")
        elem.waitForDisplayed({ timeout: 80000 });
        return elem
    }
    get PlanName() {
        
        const elem = $("(//input[@formcontrolname='plan_name'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Comments() {
        const elem = $("//textarea[@formcontrolname='comments']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get SaveAndContinue() {
        
        const elem = $("//button[.='Save & Continue']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get FilterName() {
        
        const elem = $("//input[@id='plan_name']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FilterComments() {
       
        const elem = $("//input[@id='comments']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Filter() {
        
        const elem = $("//button[.=' Filter ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Edit() {
        
        const elem = $("//i[@class='icon-pencil']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Update() {
        
        const elem = $("//button[.='Update']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }

   async  MainMenu() {
       await this.Master.click();
       await this.Billing.click();
       await this.PlanNameButton.click();
       await this.AddNew.click();
        
    }
    async AddPlanName(name,com) {
        
        await this.PlanName.setValue(name);
        await this.Comments.setValue(com);
        await this.SaveAndContinue.click();
    }
   async FilterPlanName(name,com) {
    await this.FilterName.setValue(name);
    await this.FilterComments.setValue(com);
    await this.Filter.click();

    }
   async  EditPlanName(name,com) {
    await this.Edit.click();
    await this.PlanName.clearValue();
    await this.PlanName.setValue(name);
    await this.Comments.clearValue();
    await this.Comments.setValue(com);
    await this.Update.click();

    }


}

module.exports = new PlanNameObject();