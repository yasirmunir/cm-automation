class ReferralsObject {


    get RefferedPhysician() {
       
        const elem = $("//span[.='Were you referred by a physician?*']/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FirstName() {
        const elem = $("//label[.='First Name *']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get MiddleName() {
        const elem = $("//label[.='Middle Name ']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get LastName() {
       
        const elem = $("//label[.='Last Name *']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get ClinicName() {
        
        const elem = $("//label[.='Clinic Name']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Email() {
        const elem = $("//label[.='Email']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get PhoneNumber() {
        const elem = $("//label[.='Phone Number *']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get Extension() {
        const elem = $("//label[.='Extention']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
       
    }
    get Address() {
        const elem = $("//label[.='Address']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get Suite() {
        const elem = $("//label[.='Suite / Floor']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get City() {
        
        const elem = $("//label[.='City']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Zip() {
        
        const elem = $("//label[.='Zip']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get PrimaryPhysician() {
        const elem = $("//span[.='Do you have a primary care physician?*']/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        

    }
    get PFirstName() {
        const elem = $("(//label[.='First Name *'])[2]/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get PMiddleName() {
        const elem = $("(//label[.='Middle Name '])[2]/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get PLastName() {
        
        const elem = $("(//label[.='Last Name *'])[2]/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get PClinicName() {
        const elem = $("(//label[.='Clinic Name'])[2]/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get PEmail() {
        const elem = $("(//label[.='Email'])[2]/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get PPhoneNumber() {
        const elem = $("(//label[.='Phone Number *'])[2]/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get PExtension() {
        
        const elem = $("(//label[.='Extention'])[2]/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get PAddress() {
        const elem = $("(//label[.='Address'])[2]/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get PSuite() {
        const elem = $("(//label[.='Suite / Floor'])[2]/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get PCity() {
        const elem = $("(//label[.='City'])[2]/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get PZip() {
       
        const elem = $("(//label[.='Zip'])[2]/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SaveAndContinue() {
        const elem = $("//button[.='Save & Continue']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
       
    }
   async AddRefferedPhysician(fname,mname,lname,cname,email,phone,ext,address,suite,city){
        await this.RefferedPhysician.click();
        await this.FirstName.setValue(fname);
        await this.MiddleName.setValue(mname);
        await this.LastName.setValue(lname);
        await this.ClinicName.setValue(cname);
        await this.Email.setValue(email);
        await this.PhoneNumber.setValue(phone);
        await this.Extension.setValue(ext);
        await this.Address.setValue(address);
        await this.Suite.setValue(suite);
        await this.City.setValue(city);
    }
    async EditRefferedPhysician(fname,mname,lname,cname,email,phone,ext,address,suite,city){
        
        await this.FirstName.clearValue();
        await this.FirstName.setValue(fname);
        await this.MiddleName.clearValue();
        await this.MiddleName.setValue(mname);
        await this.LastName.clearValue();
        await this.LastName.setValue(lname);
        await this.ClinicName.clearValue();
        await this.ClinicName.setValue(cname);
        await this.Email.clearValue();
        await this.Email.setValue(email);
        await this.PhoneNumber.clearValue();
        await this.PhoneNumber.setValue(phone);
        await this.Extension.clearValue();
        await this.Extension.setValue(ext);
        await this.Address.clearValue();
        await this.Address.setValue(address);
        await this.Suite.clearValue();
        await this.Suite.setValue(suite);
        await this.City.clearValue();
        await this.City.setValue(city);
    }
    async AddPrimaryPhysician(fname,mname,lname,cname,email,phone,ext,address,suite,city) {
        await this.PFirstName.setValue(fname);
        await this.PMiddleName.setValue(mname);
        await this.PLastName.setValue(lname);
        await this.PClinicName.setValue(cname);
        await this.PEmail.setValue(email);
        await this.PPhoneNumber.setValue(phone);
        await this.PExtension.setValue(ext);
        await this.PAddress.setValue(address);
        await this.PSuite.setValue(suite);
        await this.PCity.setValue(city);
    }
    async EditPrimaryPhysician(fname,mname,lname,cname,email,phone,ext,address,suite,city) {
        await this.PFirstName.clearValue();
        await this.PFirstName.setValue(fname);
        await this.PMiddleName.clearValue();
        await this.PMiddleName.setValue(mname);
        await this.PLastName.clearValue();
        await this.PLastName.setValue(lname);
        await this.PClinicName.clearValue();
        await this.PClinicName.setValue(cname);
        await this.PEmail.clearValue();
        await this.PEmail.setValue(email);
        await this.PPhoneNumber.clearValue();
        await this.PPhoneNumber.setValue(phone);
        await this.PExtension.clearValue();
        await this.PExtension.setValue(ext);
        await this.PAddress.clearValue();
        await this.PAddress.setValue(address);
        await this.PSuite.clearValue();
        await this.PSuite.setValue(suite);
        await this.PCity.clearValue();
        await this.PCity.setValue(city);
    }
}

module.exports = new ReferralsObject();