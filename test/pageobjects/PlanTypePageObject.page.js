class PlanTypePageObject {
    get Master() {
        const elem = $("//a[.=' Master ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }

    get Billing() {
        
        const elem = $("//a[.='Billing']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get PlanTypeButton() {
        const elem = $("//a[.='Plan Type']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get AddNew() {
       
        const elem = $("//button[.=' Add New ']")
        elem.waitForDisplayed({ timeout: 80000 });
        return elem
    }
    get PlanTypeName() {
        const elem = $("(//input[@formcontrolname='plan_type'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get Comments() {
        
        const elem = $("//textarea[@formcontrolname='comments']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SaveAndContinue() {
        
        const elem = $("//button[.='Save & Continue']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FilterPlanTypeName() {
       
        const elem = $("//input[@id='plan_type']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FilterComments() {
       
       const elem = $("//input[@id='comments']")
       elem.waitForDisplayed({ timeout: 30000 });
       return elem


    }
    get Filter() {
        
        const elem = $("//button[.=' Filter ']")
       elem.waitForDisplayed({ timeout: 30000 });
       return elem
    }

    get Edit() {
        
        const elem = $("//i[@class='icon-pencil']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Update() {
       
        const elem = $("//button[.='Update']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }

    async MainMenu() {
        await this.Master.click();
        await this.Billing.click();
        await this.PlanTypeButton.click();
        await this.AddNew.click();
    }
    async AddPlanType(name,com) {
        await this.PlanTypeName.setValue(name);
        await this.Comments.setValue(com)
        await this.SaveAndContinue.click();
    }
    async FilterPlanType(name,com) {
        await this.FilterPlanTypeName.setValue(name);
        await this.FilterComments.setValue(com);
        await this.Filter.click();

    }
   async EditPlanType(name,com) {
    await this.Edit.click();
    await this.PlanTypeName.clearValue();
    await this.PlanTypeName.setValue(name);
    await this.Comments.clearValue();
    await this.Comments.setValue(com)
    await this.Update.click();

    }
}

module.exports = new PlanTypePageObject();