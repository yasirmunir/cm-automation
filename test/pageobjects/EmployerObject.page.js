class EmployerObject {

    get CourseofEmployment() {

        const elem = $("//span[.='At the time of your accident,were you in the course of employment?*']/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get PatientOccupation() {
        const elem = $("(//input[@type='text'])[11]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get HiringDate() {

        const elem = $("//label[.='Hiring Date']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get LooseEmployment() {

        const elem = $("//span[.='Did you loose time from work at other employments as a result of your injury? *']/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FirstName() {
        const elem = $("//label[.='First Name']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem


    }
    get MiddleName() {
        const elem = $("(//label[.='Middle Name'])[2]/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get LastName() {

        const elem = $("//label[.='Last Name']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SaveAndContinue() {

        const elem = $("//button[.='Save & Continue']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SecondryEmployer() {
        const elem = $("//span[.='Do you have a secondary employer?']/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get SecondryPatientOccupation() {
        const elem = $("(//input[@type='text'])[11]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get SecondryHiringDate() {

        const elem = $("//label[.='Hiring Date']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SecondrySaveAndContinue() {

        const elem = $("//button[.='Save & Continue']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get YearlyEmployer() {
        const elem = $("//span[.='Do you have a yearly employer?']/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get YearlyPatientOccupation() {
        const elem = $("(//input[@type='text'])[11]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get YearlyHiringDate() {

        const elem = $("//label[.='Hiring Date']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get YearlyEndDate() {
        const elem = $("//label[.='End Date']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get YearlySaveAndContinue() {

        const elem = $("//button[.='Save & Continue']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get JobDescription() {

        const elem = $("//label[.='Job Title / Description *']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get TypeOfActivities() {

        const elem = $("//label[.='Type of Activities *']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get YourJob() {


        const elem = $("//span[.='Was your job? *']/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get GrossPay() {
        const elem = $("//label[.='Your gross pay (before taxes) per pay period?']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get WeeklyEarning() {

        const elem = $("//label[.='Weekly Earning']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get WeekDays() {

        const elem = $("//label[.='Days/Week']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get HoursDay() {
        const elem = $("//label[.='Hours/Day']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get LoadingTips() {
        const elem = $("//span[.='Did you receive lodging & tips in addition to your pay?']/following::span/following::span/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get StopWork() {

        const elem = $("//span[.='Did you stop work because of your injury/illness? *']/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get onWhatDate() {
        const elem = $("//label[.='On What Date?']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get ReturnOfWork() {
        const elem = $("//span[.='Have you returned to work? *']/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get DateOfReturnOfWork() {

        const elem = $("//label[.='Date of Return To Work *']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }

    get SameEmployer() {

        const elem = $("//span[.='If you have returned to work, who are you working for now?*']/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get TypeOfAssignment() {
        const elem = $("//span[.='Type of New Assignment *']/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get NoticeOfInjury() {
        const elem = $("//span[.='Have you given your employer(or Supervisor) notice of injury/illness? *']/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem


    }
    get NoticeFirstName() {
        const elem = $("//label[.='First Name *']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get NoticeMiddleName() {
        const elem = $("//label[.='Middle Name']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem


    }
    get NoticeLastName() {

        const elem = $("//label[.='Last Name *']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get NoticeGiven() {
        const elem = $("//span[.='Notice was given *']/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem


    }
    get DateOfNotice() {
        const elem = $("//label[.='Date of Notice']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get EmployerSaveAndContinue() {

        const elem = $("//button[.=' Save & Continue']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get RefferingComapany() {
        const elem = $("//span[.='Do you have a referring company?']/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get CurrentlyEmployed() {
        const elem = $("//span[.='Are you currently employed?*']/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }



}

module.exports = new EmployerObject();