class Designation {
    get Master() {
        const elem = $("//a[.=' Master ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get User() {

        const elem = $("//a[.='User']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get DesignationButton() {
        const elem = $("//a[.='Designation']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get AddNew() {

        const elem = $("//a[.=' Add New ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get DesignationName() {

        const elem = $("(//input[@formcontrolname='name'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Comments() {
        const elem = $("//textarea[@formcontrolname='description']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SaveAndContinue() {
        const elem = $("//button[.='Save & Continue']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get FilterDesignationName() {

        const elem = $("//input[@id='Designation']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FilterComments() {

        const elem = $("//input[@id='comments']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Filter() {

        const elem = $("//button[.=' Filter ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Update() {

        const elem = $("//button[.='Update']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Edit() {
        const elem = $("//i[@class='icon-pencil']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }

    async MainMenu() {
        await this.Master.click();
        await this.User.click();
        await this.DesignationButton.click();
        await this.AddNew.click();
    }
    async AddDesignation(name, cm) {
        await this.DesignationName.setValue(name);
        await this.Comments.setValue(cm);
        await this.SaveAndContinue.click();
    }
    async FilterDesignation(name, cm) {
        await this.FilterDesignationName.setValue(name);
        await this.FilterComments.setValue(cm);
        await this.Filter.click();
    }
    async EditDesignation(name, cm) {
        await this.Edit.click();
        await this.DesignationName.clearValue();
        await this.DesignationName.setValue(name);
        await this.Comments.clearValue();
        await this.Comments.setValue(cm);
        await this.Update.click();
    }

}

module.exports = new Designation();