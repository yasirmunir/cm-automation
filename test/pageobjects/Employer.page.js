class EmployerPageObject {
    get Master() {
        const elem = $("//a[.=' Master ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }

    get Billing() {
        
        const elem = $("//a[.='Billing']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Employer() {
        const elem = $("//a[.='Employer']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get AddNew() {
        const elem = $("//a[.=' Add New ']")
        elem.waitForDisplayed({ timeout: 80000 });
        return elem

    }
    get EmployerName() {
        const elem = $("//input[@formcontrolname='employer_name']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get StreetAddress() {

        const elem = $("(//input[@formcontrolname='street_address'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }

    get Suite() {

        const elem = $("(//input[@formcontrolname='apartment_suite'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get City() {

        const elem = $("(//input[@formcontrolname='city'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Zip() {
        const elem = $("(//input[@formcontrolname='zip'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get PhoneNumber() {
        const elem = $("(//input[@formcontrolname='phone_no'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Extension() {
        const elem = $("(//input[@formcontrolname='ext'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem


    }
    get Email() {
        const elem = $("(//input[@formcontrolname='email'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Fax() {
        const elem = $("(//input[@formcontrolname='fax'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get FirstName() {
        const elem = $("//input[@formcontrolname='first_name']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get MiddleName() {
        const elem = $("//input[@formcontrolname='middle_name']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get LastName() {

        const elem = $("//input[@formcontrolname='last_name']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get CPPhoneNo() {

        const elem = $("(//input[@formcontrolname='phone_no'])[3]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get CPExtension() {
        const elem = $("//input[@formcontrolname='extension']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get CPEmail() {
        const elem = $("(//input[@formcontrolname='email'])[3]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get CPFax() {
        const elem = $("(//input[@formcontrolname='fax'])[3]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get SaveAndContinue() {

        const elem = $("//button[.='Save & Continue']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FilterEmployerName() {

        const elem = $("//input[@formcontrolname='name']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FilterStreetAddress() {

        const elem = $("//input[@id='street_address']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FilterCity() {
        const elem = $("//input[@id='city']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem


    }
    get FilterState() {

        const elem = $("//input[@id='state']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FilterZip() {

        const elem = $("//input[@id='zip']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FilterPhoneNo() {

        const elem = $("//input[@id='phone_no']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FilterExtension() {

        const elem = $("//input[@id='ext']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FilterEmail() {
        const elem = $("//input[@id='email']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get FilterFax() {
        const elem = $("//input[@id='fax']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get ContactPersonName() {

        const elem = $("//input[@id='contact_person_name']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FilterSuite() {

        const elem = $("//input[@id='apartment_suite']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Filter() {
        const elem = $("//button[.=' Filter ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get PlusIcon() {
        const elem = $("//button[@aria-expanded='true']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Update() {

        const elem = $("//button[.='Update']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Edit() {
        const elem = $("//i[@class='icon-pencil']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    



    async MianMenu() {
        await this.Master.click();
        await this.Billing.click();
        await this.Employer.click();
        await this.AddNew.click();

    }

    async AddEmployer(name, address, suite, city) {
        await this.EmployerName.setValue(name);
        await this.StreetAddress.setValue(address);
        await this.Suite.setValue(suite);
        await this.City.setValue(city);

    }
    async AddEmployerData(zip, phone, ext, email, fax) {
        await this.Zip.setValue(zip);
        await this.PhoneNumber.setValue(phone);
        await this.Extension.setValue(ext);
        await this.Email.setValue(email);
        await this.Fax.setValue(fax);


    }
    async AddContactPersonData(fname, mname, lname, phone, ext, email, fax) {
        await this.FirstName.setValue(fname);
        await this.MiddleName.setValue(mname);
        await this.LastName.setValue(lname);
        await this.CPPhoneNo.setValue(phone);
        await this.CPExtension.setValue(ext);
        await this.CPEmail.setValue(email);
        await this.CPFax.setValue(fax);
        await this.SaveAndContinue.click();

    }
    async FilterEmployer(emp, address, city, state) {
        await this.FilterEmployerName.setValue(emp);
        await this.FilterStreetAddress.setValue(address);
        await this.FilterCity.setValue(city);
        await this.FilterState.setValue(state);

    }
    async FilterEmployerData(zip, phone, ext, email, fax, con, suite) {
        await this.FilterZip.setValue(zip);
        await this.FilterPhoneNo.setValue(phone);
        await this.FilterExtension.setValue(ext);
        await this.FilterEmail.setValue(email);
        await this.FilterFax.setValue(fax);
        await this.ContactPersonName.setValue(con);
        await this.FilterSuite.setValue(suite);
        await this.Filter.click();

    }
    async EditEmployer(name, address, suite, city) {
        await this.EmployerName.clearValue();
        await this.EmployerName.setValue(name);
        await this.StreetAddress.clearValue();
        await this.StreetAddress.setValue(address);
        await this.Suite.clearValue();
        await this.Suite.setValue(suite);
        await this.City.clearValue();
        await this.City.setValue(city);

    }
    async EditEmployerData(zip, phone, ext, email, fax) {
        await this.Zip.clearValue();
        await this.Zip.setValue(zip);
        await this.PhoneNumber.clearValue();
        await this.PhoneNumber.setValue(phone);
        await this.Extension.clearValue();
        await this.Extension.setValue(ext);
        await this.Email.clearValue();
        await this.Email.setValue(email);
        await this.Fax.clearValue();
        await this.Fax.setValue(fax);


    }
    async EditContactPersonData(fname, mname, lname, phone, ext, email, fax) {
        await this.FirstName.clearValue();
        await this.FirstName.setValue(fname);
        await this.MiddleName.clearValue();
        await this.MiddleName.setValue(mname);
        await this.LastName.clearValue();
        await this.LastName.setValue(lname);
        await this.CPPhoneNo.clearValue();
        await this.CPPhoneNo.setValue(phone);
        await this.CPExtension.clearValue();
        await this.CPExtension.setValue(ext);
        await this.CPEmail.clearValue();
        await this.CPEmail.setValue(email);
        await this.CPFax.clearValue();
        await this.CPFax.setValue(fax);
        await this.Update.click();

    }






}

module.exports = new EmployerPageObject();