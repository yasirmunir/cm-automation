class InsuranceObject {
    get SetAsPrimary() {
        const elem = $("//span[.='Set as primary']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get DoPrivateInsurance() {
        const elem = $("//span[.='Do you have private insurance?*']/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get InsuredBy() {
        const elem = $("//span[.='Self']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get MemberID() {
        const elem = $("//label[.='Member ID* ']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }

    get GroupNo() {
        const elem = $("//label[.='Group No* ']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem


    }
    get PriorAuthNo() {
        const elem = $("//label[.='Prior Authorization No']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get NoFaultAccident() {
        const elem = $("//span[.='Are you the Policy holder for the vehicle that was in the accident?']/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get NoFaultFirstName() {
        const elem = $("//label[.='First Name']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get NoFaultMiddleName() {

        const elem = $("(//label[.='Middle Name'])[2]/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get NoFaultLastName() {

        const elem = $("//label[.='Last Name']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get NoFaultClaimNo() {

        const elem = $("//label[.='Claim No']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get NoFaultPolicyNo() {
        const elem = $("//label[.='Policy No']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem


    }
    get NoFaultWcbNo() {

        const elem = $("//label[.='WCB No']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get NoFaultPriorAuth() {

        const elem = $("(//label[.='Prior Authorization No'])[2]/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SecondryFirstName() {
        const elem = $("(//label[.='First Name'])[2]/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem


    }
    get DrugSecondryFirstName() {
        const elem = $("//label[.='First Name']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem


    }
    get SecondryMiddleName() {
        const elem = $("(//label[.='Middle Name'])[3]/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get DrugSecondryMiddleName() {
        const elem = $("(//label[.='Middle Name'])[2]/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get SecondryLastName() {
        const elem = $("(//label[.='Last Name'])[2]/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get DrugSecondryLastName() {
        const elem = $("//label[.='Last Name']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }

    get SecondryClaimNo() {
        const elem = $("(//label[.='Claim No'])[2]/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get DrugSecondryClaimNo() {
        const elem = $("//label[.='Claim No']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get SecondryPolicyNo() {

        const elem = $("(//label[.='Policy No'])[2]/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get DrugSecondryPolicyNo() {

        const elem = $("//label[.='Policy No']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SecondryWcbNo() {

        const elem = $("(//label[.='WCB No'])[2]/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get DrugSecondryWcbNo() {

        const elem = $("//label[.='WCB No']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get DrugSecondryPriorityAut() {
        const elem = $("(//label[.='Prior Authorization No'])[2]/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get SecondryPriorityAut() {
        const elem = $("(//label[.='Prior Authorization No'])[3]/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get TertiaryInsurance() {

        const elem = $("//span[.='Do you have tertiary insurance?*']/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get DrugTertiaryFirstName() {
        const elem = $("(//label[.='First Name'])[2]/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get TertiaryFirstName() {
        const elem = $("(//label[.='First Name'])[3]/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get DrugTertiaryMiddleName() {
        const elem = $("(//label[.='Middle Name'])[3]/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get TertiaryMiddleName() {
        const elem = $("(//label[.='Middle Name'])[4]/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get DrugTertiaryLastName() {

        const elem = $("(//label[.='Last Name'])[2]/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get TertiaryLastName() {

        const elem = $("(//label[.='Last Name'])[3]/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get DrugTertiaryClaimNo() {

        const elem = $("(//label[.='Claim No'])[2]/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get TertiaryClaimNo() {

        const elem = $("(//label[.='Claim No'])[3]/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get DrugTertiaryPolicyNo() {
        const elem = $("(//label[.='Policy No'])[2]/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get TertiaryPolicyNo() {
        const elem = $("(//label[.='Policy No'])[3]/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get DrugTertiaryWcbNo() {

        const elem = $("(//label[.='WCB No'])[2]/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get TertiaryWcbNo() {

        const elem = $("(//label[.='WCB No'])[3]/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get DrugTertiaryPriorAuthNo() {
        const elem = $("(//label[.='Prior Authorization No'])[3]/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get TertiaryPriorAuthNo() {
        const elem = $("(//label[.='Prior Authorization No'])[4]/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get SaveAndContinue() {
        const elem = $("//button[.=' Save & Continue']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }


    async AddPrivateHealthInsurance() {
        await this.SetAsPrimary.click();
        await this.DoPrivateInsurance.click();
        await this.InsuredBy.click();
    }
    async EditPrivateHealthInsuranceData(member, groupNo, auth) {
        await this.MemberID.clearValue();
        await this.MemberID.setValue(member);
        await this.GroupNo.clearValue();
        await this.GroupNo.setValue(groupNo);
        await this.PriorAuthNo.clearValue();
        await this.PriorAuthNo.setValue(auth);
    }
    async AddPrivateHealthInsuranceData(member, groupNo, auth) {
        await this.MemberID.setValue(member);
        await this.GroupNo.setValue(groupNo);
        await this.PriorAuthNo.setValue(auth);
    }
    async AddNoFaultPolicyHolder(fname, mname, lname, claim, policy, wcb, auth) {
        await this.NoFaultFirstName.setValue(fname);
        await this.NoFaultMiddleName.setValue(mname);
        await this.NoFaultLastName.setValue(lname);
        await this.NoFaultClaimNo.setValue(claim);
        await this.NoFaultPolicyNo.setValue(policy);
        await this.NoFaultWcbNo.setValue(wcb);
        await this.NoFaultPriorAuth.setValue(auth);
    }
    async EditNoFaultPolicyHolder(fname, mname, lname, claim, policy, wcb, auth) {
        await this.NoFaultFirstName.clearValue();
        await this.NoFaultFirstName.setValue(fname);
        await this.NoFaultMiddleName.clearValue();
        await this.NoFaultMiddleName.setValue(mname);
        await this.NoFaultLastName.clearValue();
        await this.NoFaultLastName.setValue(lname);
        await this.NoFaultClaimNo.clearValue();
        await this.NoFaultClaimNo.setValue(claim);
        await this.NoFaultPolicyNo.clearValue();
        await this.NoFaultPolicyNo.setValue(policy);
        await this.NoFaultWcbNo.clearValue();
        await this.NoFaultWcbNo.setValue(wcb);
        await this.NoFaultPriorAuth.clearValue();
        await this.NoFaultPriorAuth.setValue(auth);
    }
    async AddSecondryPolicyHolderInformation(fname, mname, lname, cm, policy, wcb, auth) {
        await this.SecondryFirstName.setValue(fname);
        await this.SecondryMiddleName.setValue(mname);
        await this.SecondryLastName.setValue(lname);
        await this.SecondryClaimNo.setValue(cm);
        await this.SecondryPolicyNo.setValue(policy);
        await this.SecondryWcbNo.setValue(wcb);
        await this.SecondryPriorityAut.setValue(auth);
    }
    async EditSecondryPolicyHolderInformation(fname, mname, lname, cm, policy, wcb, auth) {
        await this.SecondryFirstName.clearValue();
        await this.SecondryFirstName.setValue(fname);
        await this.SecondryMiddleName.clearValue();
        await this.SecondryMiddleName.setValue(mname);
        await this.SecondryLastName.clearValue();
        await this.SecondryLastName.setValue(lname);
        await this.SecondryClaimNo.clearValue();
        await this.SecondryClaimNo.setValue(cm);
        await this.SecondryPolicyNo.clearValue();
        await this.SecondryPolicyNo.setValue(policy);
        await this.SecondryWcbNo.clearValue();
        await this.SecondryWcbNo.setValue(wcb);
        await this.SecondryPriorityAut.clearValue();
        await this.SecondryPriorityAut.setValue(auth);
    }
    async AddTertiaryPolicyHolderInformation(fname, mname, lname, cm, policy, wcb, auth) {
        await this.TertiaryFirstName.setValue(fname);
        await this.TertiaryMiddleName.setValue(mname);
        await this.TertiaryLastName.setValue(lname);
        await this.TertiaryClaimNo.setValue(cm);
        await this.TertiaryPolicyNo.setValue(policy);
        await this.TertiaryWcbNo.setValue(wcb);
        await this.TertiaryPriorAuthNo.setValue(auth);
    }
    async EditTertiaryPolicyHolderInformation(fname, mname, lname, cm, policy, wcb, auth) {
        await this.TertiaryFirstName.clearValue();
        await this.TertiaryFirstName.setValue(fname);
        await this.TertiaryMiddleName.clearValue();
        await this.TertiaryMiddleName.setValue(mname);
        await this.TertiaryLastName.clearValue();
        await this.TertiaryLastName.setValue(lname);
        await this.TertiaryClaimNo.clearValue();
        await this.TertiaryClaimNo.setValue(cm);
        await this.TertiaryPolicyNo.clearValue();
        await this.TertiaryPolicyNo.clearValue();
        await this.TertiaryPolicyNo.setValue(policy);
        await this.TertiaryWcbNo.clearValue();
        await this.TertiaryWcbNo.setValue(wcb);
        await this.TertiaryPriorAuthNo.clearValue();
        await this.TertiaryPriorAuthNo.setValue(auth);
    }
    async AddDrugSecondryPolicyHolderInformation(fname, mname, lname, cm, policy, wcb, auth) {
        await this.DrugSecondryFirstName.setValue(fname);
        await this.DrugSecondryMiddleName.setValue(mname);
        await this.DrugSecondryLastName.setValue(lname);
        await this.DrugSecondryClaimNo.setValue(cm);
        await this.DrugSecondryPolicyNo.setValue(policy);
        await this.DrugSecondryWcbNo.setValue(wcb);
        await this.DrugSecondryPriorityAut.setValue(auth);
    }
    async AddDrugTertiaryPolicyHolderInformation(fname, mname, lname, cm, policy, wcb, auth) {
        await this.DrugTertiaryFirstName.setValue(fname);
        await this.DrugTertiaryMiddleName.setValue(mname);
        await this.DrugTertiaryLastName.setValue(lname);
        await this.DrugTertiaryClaimNo.setValue(cm);
        await this.DrugTertiaryPolicyNo.setValue(policy);
        await this.DrugTertiaryWcbNo.setValue(wcb);
        await this.DrugTertiaryPriorAuthNo.setValue(auth);
    }
}
module.exports = new InsuranceObject();