class SpecialtyPageObject {

    get Master() {
        const elem = $("//a[.=' Master ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Provider() {
        const elem = $("//a[.='Provider']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get AddNewSpecialtyButton() {
        const elem = $("//a[text()=' Add New ']")
        elem.waitForDisplayed({ timeout: 60000 });
        return elem

    }
    get SpecialtyName() {
        return $("//label[.='Specialty Name*']/following::input");
    }
    get TimeSlot() {
        return $("//label[.='Time Slot*']/following::input")
    }
    get OverBooking() {
        return $("//label[.='Over Booking No.*']/following::input")
    }
    get CreatAppointment() {
        return $("//span[@for='createappt']")
    }
    get Save() {
        return $("//button[normalize-space()='Save & Continue']")
    }
    get Commments() {
        return $("//textarea[@id='comments']")
    }
    get SaveAndContinue() {
        return $("//button[.='Save & Continue ']")
    }
    get FilterSpecialtyName() {
        return $("//input[@id='name']");
    }
    get FilterTimeSlot() {
        return $("//input[@id='time_slot']")

    }
    get FilterOverBooking() {
        return $("//input[@id='over_booking']");
    }
    get Filter() {
        return $("(//button[text()=' Filter '])[2]");
    }

    get Edit() {
        return $("//i[@class='icon-pencil']");
    }
    get Update() {
        return $("//button[text()='Update ']");
    }


   async MainPage() {
        await this.Master.click();
        await this.Provider.click();



    }
   async AddSpecialtyName(name) {
    await this.AddNewSpecialtyButton.click();
    await this.SpecialtyName.waitForDisplayed();
    await this.SpecialtyName.setValue(name);

    }
    async AddSpecialtyData(timeSlot, overBooking, comment) {
        await this.TimeSlot.waitForDisplayed();
        await this.TimeSlot.setValue(timeSlot);
        await this.OverBooking.waitForDisplayed();
        await this.OverBooking.setValue(overBooking);
        await this.CreatAppointment.waitForDisplayed();
        await this.CreatAppointment.click();
        await this.Commments.waitForDisplayed();
        await this.Commments.setValue(comment);
        await this.SaveAndContinue.waitForDisplayed();
        await this.SaveAndContinue.click();

    }
   async FilterSpecialty(name, timeslot, overbooking) {
    await this.FilterSpecialtyName.waitForDisplayed();
    await this.FilterSpecialtyName.setValue(name);
    await this.FilterTimeSlot.waitForDisplayed();
    await this.FilterTimeSlot.setValue(timeslot);
    await this.FilterOverBooking.waitForDisplayed();
    await this.FilterOverBooking.setValue(overbooking);
    await this.Filter.waitForDisplayed();
    await this.Filter.click();
    }
    async EditSpecialtyName(name) {
        await this.Edit.click();
        await this.SpecialtyName.clearValue();
        await this.SpecialtyName.setValue(name);

    }
    async EditSpecialtyData(timeSlot, overBooking, comment) {
        await this.TimeSlot.clearValue();
        await this.TimeSlot.setValue(timeSlot);
        await this.OverBooking.clearValue();
        await this.OverBooking.setValue(overBooking);
        await this.Commments.clearValue();
        await this.Commments.setValue(comment);
        await this.Update.waitForDisplayed();
        await this.Update.click();



    }




}

module.exports = new SpecialtyPageObject();