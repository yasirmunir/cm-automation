class BillingEmploymentTypePageObject {
    get Master() {
        const elem = $("//a[.=' Master ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }

    get Billing() {

        const elem = $("//a[.='Billing']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SubBilling() {
        
        const elem = $("(//a[.='Billing'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get BillingEmploymentType() {
        const elem = $("//a[.='Billing Employment Type']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get AddNew() {
        
        const elem = $("//a[.=' Add New ']")
        elem.waitForDisplayed({ timeout: 80000 });
        return elem
    }
    get BillingEmploymentTypeName() {
        
        const elem = $("(//input[@formcontrolname='name'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
 
    get Comments() {
        
        const elem = $("//textarea[@formcontrolname='description']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SaveAndContinue() {
        
        const elem = $("//button[.='Save & Continue']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FilterBillingEmploymentTypeName() {
        const elem = $("//input[@id='name']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
  
    get FilterComments() {
        
        const elem = $("//input[@id='description']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Filter() {
        const elem = $("//button[.=' Filter ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get Edit() {
        
        const elem = $("//i[@class='icon-pencil']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Update() {
        const elem = $("//button[.='Update']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }

    async MainMenu() {
        await this.Master.click();
        await this.Billing.click();
        await this.SubBilling.click();
        await this.BillingEmploymentType.click();
        await this.AddNew.click();
    }
    async AddBillingEmploymentType(name,com) {
        await this.BillingEmploymentTypeName.setValue(name);
        await this.Comments.setValue(com);
        await this.SaveAndContinue.click();
    }
    async FilterBillingEmploymentType(name,com){
        await this.FilterBillingEmploymentTypeName.setValue(name);
        await this.FilterComments.setValue(com);
        await this.Filter.click();

    }
    async EditBillingEmploymentType(name,com) {
        await this.Edit.click();
        await this.BillingEmploymentTypeName.clearValue();
        await this.BillingEmploymentTypeName.setValue(name);
        await this.Comments.clearValue();
        await this.Comments.setValue(com);
        await this.Update.click();

    }
}
module.exports = new BillingEmploymentTypePageObject();