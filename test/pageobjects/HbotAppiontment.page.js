class HbotAppiontmentObject {

    get Scheduler() {
        const elem = $("//a[.=' Scheduler ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get ProviderCalender() {
        const elem = $("//a[.='Provider Calendar']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get PracticeLocation() {
        const elem = $("//div[.=' Practice-Location ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get MonthView() {
        const elem = $("//button[.='Month']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Provider() {

        const elem = $("//div[.=' Provider ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get CaseNo() {

        const elem = $("(//input[@placeholder='Case No.'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Comments() {
        const elem = $("//textarea[@placeholder='No Comment Yet']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get SaveAndContinue() {
        const elem = $("//button[.=' Save & Continue']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get PatientName() {
        const elem = $("(//input[@placeholder='Patient Name'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get ChartID() {
        const elem = $("(//input[@placeholder='Chart ID'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get PreviousDate() {

        const elem = $("//div[@class='previous-date']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Specialty() {
        const elem = $("//div[.=' Specialty ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get EditEval() {

        const elem = $("//button[.=' Edit Eval']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Astma() {

        const elem = $("//span[.='Asthma']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Pregnancy() {

        const elem = $("//span[.='Pregnancy']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get untreated() {
        const elem = $("//span[.='Untreated Pneumothorax']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Cancer() {

        const elem = $("//span[.='Cancer']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get PaceMaker() {

        const elem = $("//span[.='Pacemaker']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem


    }
    get Doxorubicin() {
        const elem = $("//span[.='Doxorubicin (Adriamycin)']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Smoker() {

        const elem = $("//span[.='A smoker']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get hbotSaveAndContinue() {
        const elem = $("//button[.=' Save & Continue']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem


    }
    get NoteComments() {
        const elem = $("//label[.='Comments']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get ChronicSinusitis() {
        const elem = $("//span[normalize-space()='Chronic Sinusitis']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get UpperRespiratoryInfection() {

        const elem = $("//span[.='Upper Respiratory Infection']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get SicklecellAniema() {
        const elem = $("//span[.='Sickle Cell Anemia']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get EmphysemawithCo2retention() {

        const elem = $("//span[.='Emphysema with Co2 retention']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SeizureDisorder() {
        const elem = $("//span[.='Seizure Disorder']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get OpticNeuritis() {
        const elem = $("//span[normalize-space()='Optic Neuritis']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Uncontrolledhighfever() {
        const elem = $("//span[.='Uncontrolled high fever']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get CongenitalSpherocytosis() {
        const elem = $("//span[.='Congenital Spherocytosis']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Reconstructiveearsurgery() {

        const elem = $("//span[.='Reconstructive ear surgery']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get ThoracicSurgery() {

        const elem = $("//span[.='Thoracic Surgery']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Disulfiram() {
        const elem = $("//span[.='Disulfiram (Antabuse)']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Cisplatin() {

        const elem = $("//span[.='Cisplatin']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get MafenideAcetate() {
        const elem = $("//span[.='Mafenide Acetate (Sulfamylon)']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Claustrophobic() {
        const elem = $("//span[.='Claustrophobic']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get PSI() {
        const elem = $("//label[.='PSI']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get ATA() {
        const elem = $("//label[.='ATA']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem


    }
    get EarPlanes() {

        const elem = $("//label[.='Ear Planes']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get EarPlanes() {

        const elem = $("//label[.='Ear Planes']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get TimeStarted() {

        const elem = $("//label[.='Time Started']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get TimeMaxPSI() {
        const elem = $("//label[.='Time to Max PSI']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get TimeStartDown() {
        const elem = $("//input[@id='time_started_down']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get TimeSpentAtMax() {

        const elem = $("//label[.='Time spent at MAX']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get TimeToZero() {
        const elem = $("//input[@id='time_to_zero']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get TotaltimeinChamber() {
        const elem = $("//label[.='Total Time in Chamber']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get PointFiveStartTime() {

        const elem = $("//input[@id='start_time']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }

    get PointFiveEndTime() {

        const elem = $("//input[@id='end_time']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get PointOneStartTime() {
        const elem = $("(//input[@id='start_time'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get PointOneEndTime() {

        const elem = $("(//input[@id='end_time'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get PointOnePointFiveStartTime() {
        const elem = $("(//input[@id='start_time'])[3]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get PointOnePointFiveEndTime() {

        const elem = $("(//input[@id='end_time'])[3]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get PointTwoStartTime() {
        const elem = $("(//input[@id='start_time'])[4]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get PointTwoEndTime() {

        const elem = $("(//input[@id='end_time'])[4]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get PointTwoPointFiveStartTime() {
        const elem = $("(//input[@id='start_time'])[5]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get PointTwoPointFiveEndTime() {

        const elem = $("(//input[@id='end_time'])[5]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get PointThreeStartTime() {
        const elem = $("(//input[@id='start_time'])[6]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get PointThreeEndTime() {

        const elem = $("(//input[@id='end_time'])[6]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get PointThreePointFiveStartTime() {
        const elem = $("(//input[@id='start_time'])[7]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get PointThreePointFiveEndTime() {

        const elem = $("(//input[@id='end_time'])[7]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get PointFourStartTime() {
        const elem = $("(//input[@id='start_time'])[8]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get PointFourEndTime() {

        const elem = $("(//input[@id='end_time'])[8]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FourPointFourStartTime() {
        const elem = $("(//input[@id='start_time'])[9]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get FourPointFourEndTime() {

        const elem = $("(//input[@id='end_time'])[9]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get HbotNotesComments() {
        const elem = $("//label[.='Comments']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get DoctorReviewed() {

        const elem = $("//label[.='Doctor Reviewed']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SaveForLater() {
        const elem = $("//button[.=' Save For Later ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get ChoosePrevious() {
        
        const elem = $("//button[.='Choose Previous']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Back() {
        const elem = $("//button[.=' Back ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get ProvderSignature() {
        
        const elem = $("//div[@class='signature-pad']//canvas")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get LocationArea() {
        const elem = $$("//span[@tooltipclass='table-custom-tooltip']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get ProviderList() {
        const elem = $$("//span[@tooltipclass='table-custom-tooltip']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get CalenderDay() {
        const elem = $$("//span[.=contains(@class,'cal-day-number')]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get Case() {
        
        const elem = $$("//a[@class='ng-star-inserted']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Billable() {
        const elem = $("//select[@formcontrolname='billable']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get Mask() {
        const elem = $("//select[@container='body']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get PSIEars() {
        const elem = $("(//select[@container='body'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get ICDCode() {
        const elem = $("//input[@placeholder='ICD Codes:']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get ICDCodeList() {
        const elem = $$("//span[@class='mat-option-text']/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get WeekView() {
        const elem = $("//button[.='Week']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get DayView() {
        
        const elem = $("//button[.='Day']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get WeekEvaluation() {
        const elem = $("//button[.=' Edit Evalutaion']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get PatientName() {
        
        const elem = $("//label[.='Patient Name']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get ChoosePrevious() {
        const elem = $("//button[.='Choose Previous']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get ChooseSignature() {
        
        const elem = $("(//button[.='Choose '])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SaveForLater() {
        const elem = $("(//button[.=' Save For Later '])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

        
    }
    get AddNote() {
        
        const elem = $("//button[.='Add Note']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get AddNoteContent() {
        const elem = $("//textarea[@type='text']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get SaveNote() {
        
        const elem = $("//button[.='Save']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get NoteDate() {
        const elem = $("//input[@placeholder='Enter Date']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get Unavailability() {
        const elem = $("//button[.='Unavailability']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get Subject() {
        const elem = $("//input[@formcontrolname='subject']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get Description() {
        const elem = $("//textarea[@type='textarea']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get AddUnavailabilty() {
        const elem = $("//button[.='Add']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }


    async MainMenu() {
        await this.Scheduler.click();
        await this.ProviderCalender.click();
        await this.PracticeLocation.click();
    }

    async Evaluation() {
        await this.Astma.click();
        await this.Pregnancy.click();
        await this.untreated.click();
        await this.ChronicSinusitis.click();
        await this.UpperRespiratoryInfection.click();
        await this.SicklecellAniema.click();
        await this.EmphysemawithCo2retention.click();
        await this.SeizureDisorder.click();
        await this.Uncontrolledhighfever.click();
        await this.CongenitalSpherocytosis.click();
        await this.Reconstructiveearsurgery.click();
        await this.ThoracicSurgery.click();
        await this.OpticNeuritis.click();
        await this.Cancer.click();
        await this.PaceMaker.click();
        await this.Doxorubicin.click();
        await this.Disulfiram.click();
        await this.Cisplatin.click();
        await this.MafenideAcetate.click();
        await this.Smoker.click();
        await this.Claustrophobic.click();
        await this.hbotSaveAndContinue.click();

    }
    async Time(tmp, tsd, max, ttz, tsam, tic) {
        await this.TimeMaxPSI.setValue(tmp);
        await this.TimeStartDown.setValue(tsd);
        await this.TimeSpentAtMax.setValue(max);
        await this.TimeToZero.setValue(ttz);
        await this.TimeSpentAtMax.setValue(tsam);
        await this.TotaltimeinChamber.setValue(tic);
    }
    async TimeData(st, et, st1, et1, st2, et2, st3, et3, st4, et4, st5, et5, st6,et6, st7, et7, st8, et8) {
        await this.PointFiveStartTime.setValue(st);
        await this.PointFiveEndTime.setValue(et);
        await this.PointOneStartTime.setValue(st1);
        await this.PointOneEndTime.setValue(et1);
        await this.PointOnePointFiveStartTime.setValue(st2);
        await this.PointOnePointFiveEndTime.setValue(et2);
        await this.PointTwoStartTime.setValue(st3);
        await this.PointTwoEndTime.setValue(et3);
        await this.PointTwoPointFiveStartTime.setValue(st4);
        await this.PointTwoPointFiveEndTime.setValue(et4);
        await this.PointThreeStartTime.setValue(st5);
        await this.PointThreeEndTime.setValue(et5);
        await this.PointThreePointFiveStartTime.setValue(st6);
        await this.PointThreePointFiveEndTime.setValue(et6);
        await this.PointFourStartTime.setValue(st7);
        await this.PointFourEndTime.setValue(et7);
        await this.FourPointFourStartTime.setValue(st8);
        await this.FourPointFourEndTime.setValue(et8);
    }




}

module.exports = new HbotAppiontmentObject();