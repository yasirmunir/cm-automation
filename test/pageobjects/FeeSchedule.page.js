class FeeSchedule {
    get Master() {
        const elem = $("//a[.=' Master ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Billing() {

        const elem = $("//a[.='Billing']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Codes() {

        const elem = $("//a[.='Codes']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FeeScheduleButton() {
        const elem = $("//a[.='Fee Schedule']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get AddNew() {

        const elem = $("//a[.=' Add New ']")
        elem.waitForDisplayed({ timeout: 80000 });
        return elem
    }
    get ClickCodeType() {
        const elem = $("//label[.=' Code Type* ']/following::ng-select")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get SelectCodeType() {

        const elem = $$("//div[@role='option']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get ClickCodeName() {
        const elem = $("//label[.=' Code Name* ']/following::ng-select")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SelectCodeName() {

        const elem = $$("//span[@container='body']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get BasePrice() {
        const elem = $("(//input[@formcontrolname='base_price'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get ExpectedReimbursement() {
        const elem = $("//input[@formcontrolname='expected_reimbursement']");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Units() {
        const elem = $("(//input[@formcontrolname='units'])[2]");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get ClickFeeType() {

        const elem = $("(//label[.=' Fee Type '])[2]/following::ng-select");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SelectFeeType() {

        const elem = $$("//div[@role='option']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get ClickCaseType() {
        const elem = $("//label[.=' Case Type ']/following::ng-select")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get SelectCaseType() {

        const elem = $$("//span[@container='body']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get ClickVisitType() {

        const elem = $("//label[.=' Visit Type ']/following::ng-select")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get SelectVisitType() {

        const elem = $$("//div[@role='option']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get ClickProviver() {
        const elem = $("//label[.=' Provider ']/following::ng-select")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get SelectProvider() {

        const elem = $$("//div[@role='option']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get ClickSpecialty() {

        const elem = $("(//label[.=' Specialty '])[2]/following::ng-select")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SelectSpecialty() {

        const elem = $$("//span[@container='body']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get ClickModifiers() {

        const elem = $("(//label[.=' Modifiers '])[2]/following::ng-select")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SelectModifiers() {

        const elem = $$("//div[@role='option']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get ClickModifierCategoryOne() {
        const elem = $("(//label[@for='bill_id'])[15]/following::ng-select")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get SelectModifiersCategoryOne() {

        const elem = $$("//div[@role='option']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get ClickModifierCategoryTwo() {
        const elem = $("(//label[@for='bill_id'])[16]/following::ng-select")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get SelectModifiersCategoryTwo() {

        const elem = $$("//div[@role='option']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get ClickPractice() {
        const elem = $("//label[.=' Practice ']/following::ng-select")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get SelectPractice() {

        const elem = $$("//span[@container='body']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get ClickPlaceOfService() {

        const elem = $("//label[.=' Place of Service ']/following::ng-select")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SelectPlaceOfService() {

        const elem = $$("//div[@role='option']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get ClickRegion() {
        const elem = $("//label[.=' Region ']/following::ng-select")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get SelectRegion() {

        const elem = $$("//div[@role='option']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get ClickEmployer() {
        const elem = $("(//label[.=' Employer '])[2]/following::ng-select")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get SelectEmployer() {

        const elem = $$("//div[@role='option']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get EmployerStartDate() {

        const elem = $("//label[@for='employer_start_date']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get EmployerEndDate() {
        const elem = $("//label[@for='employer_end_date']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get ClickInsurance() {
        const elem = $("//label[.=' Insurance ']/following::ng-select")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get SelectInsurance() {

        const elem = $$("//div[@role='option']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }

    get InsuranceStartDate() {
        const elem = $("//input[@id='remindDate3']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get InsuranceEndDate() {
        const elem = $("//label[@for='insurance_end_date']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get ClickPlanName() {

        const elem = $("//label[.=' Plan Name ']/following::ng-select")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SelectPlanName() {


        const elem = $$("//div[@role='option']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Comments() {
        const elem = $("//textarea[@formcontrolname='comments']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get SaveAndContinue() {
        const elem = $("//button[.='Save & Continue']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get FilterCodeName() {
        
        const elem = $("//input[@id='code_name']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get PlusIcon() {
        
        const elem = $("//span[@class='icon-plus']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Filter() {
        
        const elem = $("//button[.=' Filter ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FilterCodeDescription() {
        const elem = $("//input[@id='description']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        

    }
    get ClickFilterFeeType() {
        
        const elem = $("//label[normalize-space()='Fee Type']/following::ng-select")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FilterSelectFeeType() {
        const elem = $$("//div[@role='option']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get FilterBasePrice() {
        const elem = $("//input[@id='base_price']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get FilterUnits() {
        
        const elem = $("//input[@id='Units']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FilterClickModifier() {
        
        const elem = $("//label[.=' Modifiers ']/following::ng-select")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get FilterSelectModifier() {
        const elem = $$("//div[@role='option']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get ClickFilterProviderName() {
        const elem = $("//label[.=' Provider Name ']/following::ng-select")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get FilterSelectProviderName() {
        const elem = $$("//div[@role='option']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get ClickFilterSpecialty() {
        
        const elem = $("//label[.=' Specialty ']/following::ng-select")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FilterSelectSpecialty() {
        const elem = $$("//div[@role='option']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get ClickFilterPracticeLocation() {
        
        const elem = $("//label[.=' Practice-Location ']/following::ng-select")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FilterSelectPracticeLocation() {
        const elem = $$("//div[@role='option']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get ClickFilterEmployer() {
       
       const elem = $("//label[.=' Employer ']/following::ng-select")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FilterSelectEmployer() {
        const elem = $$("//div[@role='option']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Edit() {
        const elem = $("//i[@class='icon-pencil']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
       
    }
    get CancelCodeType() {
        
        const elem = $("(//span[@class='ng-arrow-wrapper'])[7]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }



    async MainMenu() {
        await this.Master.click();
        await this.Billing.click();
        await this.Codes.click();
        await this.FeeScheduleButton.click();
        await this.AddNew.click();

    }



}
module.exports = new FeeSchedule();