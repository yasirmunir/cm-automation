class CPTCodePageObject {
    get Master() {
        const elem = $("//a[.=' Master ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }

    get Billing() {

        const elem = $("//a[.='Billing']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Codes() {
        
        const elem = $("//a[.='Codes']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get CPTCode() {
        const elem = $("//a[.='CPT Codes']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get AddNew() {
        
        const elem = $("//a[.=' Add New ']")
        elem.waitForDisplayed({ timeout: 80000 });
        return elem
    }
    get CPTCodeName() {
        
        const elem = $("(//input[@formcontrolname='name'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Description() {
        
        const elem = $("//textarea[@formcontrolname='description']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Comments() {
        
        const elem = $("//textarea[@formcontrolname='comments']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SaveAndContinue() {
        
        const elem = $("//button[.='Save & Continue']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FilterCPTCodeName() {
        const elem = $("//input[@id='name']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get FilterDescription() {
        const elem = $("//input[@id='description']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get FilterComments() {
        
        const elem = $("//input[@id='comments']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Filter() {
        const elem = $("//button[.=' Filter ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get Edit() {
        
        const elem = $("//i[@class='icon-pencil']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Update() {
        const elem = $("//button[.='Update']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }

    async MainMenu() {
        await this.Master.click();
        await this.Billing.click();
        await this.Codes.click();
        await this.CPTCode.click();
        await this.AddNew.click();
    }
    async AddCPTCode(name,des,com) {
        await this.CPTCodeName.setValue(name);
        await this.Description.setValue(des);
        await this.Comments.setValue(com);
        await this.SaveAndContinue.click();
    }
    async FilterCPTCode(name,des,com){
        await this.FilterCPTCodeName.setValue(name);
        await this.FilterDescription.setValue(des);
        await this.FilterComments.setValue(com);
        await this.Filter.click();

    }
    async EditCPTCode(name,des,com) {
        await this.Edit.click();
        await this.CPTCodeName.clearValue();
        await this.CPTCodeName.setValue(name);
        await this.Description.clearValue();
        await this.Description.setValue(des);
        await this.Comments.clearValue();
        await this.Comments.setValue(com);
        await this.Update.click();
    }

    

}

module.exports = new CPTCodePageObject();