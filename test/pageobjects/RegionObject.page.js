class RegionObject {
    get Master() {
        const elem = $("//a[.=' Master ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }

    get Billing() {

        const elem = $("//a[.='Billing']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SubBilling() {
        
        const elem = $("(//a[.='Billing'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Region() {
        const elem = $("//a[.='Region']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }

}

module.exports = new RegionObject();