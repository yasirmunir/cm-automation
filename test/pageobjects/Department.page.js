class Department {
    get Master() {
        const elem = $("//a[.=' Master ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get User() {

        const elem = $("//a[.='User']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get DepartmentButton() {
        const elem = $("//a[.='Department']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get AddNew() {

        const elem = $("//a[.=' Add New ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get DepartmentName() {

        const elem = $("(//input[@formcontrolname='name'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Comments() {

        const elem = $("//textarea[@formcontrolname='description']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Save() {
        const elem = $("//button[.='Save & Continue']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get FilterDepartmentName() {

        const elem = $("//input[@id='Departure']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FilterComments() {

        const elem = $("//input[@id='comments']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Filter() {

        const elem = $("//button[.=' Filter ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Update() {

        const elem = $("//button[.='Update']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Edit() {
        const elem = $("//i[@class='icon-pencil']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }


    async MainMenu() {
        await this.Master.click();
        await this.User.click();
        await this.DepartmentButton.click();
        await this.AddNew.click();


    }
    async AddDepartment(name, cm) {
        await this.DepartmentName.setValue(name);
        await this.Comments.setValue(cm);
        await this.Save.click();
    }
    async FilterDepartment(name, cm) {
        await this.FilterDepartmentName.setValue(name);
        await this.FilterComments.setValue(cm);
        await this.Filter.click();
    }
    async EditDepartment(name, cm) {
        await this.Edit.click();
        await this.DepartmentName.clearValue();
        await this.DepartmentName.setValue(name);
        await this.Comments.clearValue();
        await this.Comments.setValue(cm);
        await this.Update.click();
    }

}

module.exports = new Department();