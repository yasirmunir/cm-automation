class LoginPageObject {
    
    get useremail() {
        return $("input[placeholder='Email']")
    }
    get password() {
        return $('//input[@placeholder="Password"]')
    }
    get SignIn() {
        return $('//button[text()="Sign In"]')
    }
    async Login(useremail, password) {
        await this.useremail.setValue(useremail)
        await this.password.setValue(password)
        await this.SignIn.click()
    }
}
module.exports = new LoginPageObject();