class MedicalTreatmentObject {
    get FirstTreatment() {

        const elem = $("//span[.='Did you have any first treatment?']/following::span");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get FirstTreatmentDate() {
       
        const elem = $("//label[.='First Treatment Date (mm/dd/yyyy)']/following::input");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get TreatedOnSite() {
        
        const elem = $("//span[.='Were you treated on site? *']/following::span");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get DateOfTreatment() {
        
        const elem = $("//label[.='Date Of Treatment (mm/dd/yyyy)']/following::input");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get OffSiteTreatment() {
        const elem = $("//span[.='Were you treated off site? *']/following::span");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get TreatmentForInjury() {
        const elem = $("//span[.='Where did you receive your first off site medical treatment for your injury/illness? *']/following::span");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get DateOfAdmission() {
        
        const elem = $("//label[.='Date Of Admission']/following::input");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get TreatedForThisInjury() {
        const elem = $("//span[.='Are you still being treated for this injury/illness *']/following::span");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get DoctorFirstName() {
        const elem = $("(//input[@type='text'])[10]");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get DoctorMiddleName() {
        const elem = $("(//input[@type='text'])[11]");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get DoctorLastName() {
        
        const elem = $("(//input[@type='text'])[12]");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get DoctorStreetAddress() {
        
        const elem = $("(//label[.='Street Address'])[2]/following::input");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get DoctorSuite() {
        
        const elem = $("(//input[@type='text'])[13]");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get DoctorCity() {
        
        const elem = $("(//input[@type='text'])[14]");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get DoctorState() {
        
        const elem = $("(//input[@type='text'])[15]");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get DoctorZip() {
        
        const elem = $("(//input[@type='text'])[15]");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get SaveDoctor() {
        
        const elem = $("//button[.='Save']");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SameBodyPart() {
        const elem = $("//span[.='Do you remember having another injury to the same body part or a similar illness? *']/following::span");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get TreatedByDoctor() {
        const elem = $("//span[.='If yes, were you treated by a doctor? *']/following::span/following::span/following::span");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get InjuryWorkRelated() {
        const elem = $("//span[.='Was this previous injury/illness work related? *']/following::span");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get SameEmployer() {
        const elem = $("//span[.='If yes, were you working for the same employer that you work for now *']/following::span");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get IMEExamination() {
        const elem = $("//span[.='Have you had an independent medical examination(IME)? *']/following::span");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get NewYorkStateDisability() {
       
        const elem = $("//span[.='Due to this accident have you received or are you eligible for payments under any of the follo]/followwing'ing::span");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get AmountOfHealth() {
        const elem = $("//label[.='$ Amount of health bills to date']/following::input");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get AnyOtherExpanses() {
        const elem = $("//span[.='As a result of your injury, have you had any other expenses? *']/following::span/following::span/following::span");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get HealthTreatment() {
        
        const elem = $("//span[.='Will you have more health treatment(s)? *']/following::span");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
     get SaveAndContinue() {
         
         const elem = $("//button[.='Save & Continue']");
         elem.waitForDisplayed({ timeout: 30000 });
         return elem
     }   

    async AddDoctorInfo(fname,mname,lname,address,suite,city,state,zip) {
        await this.DoctorFirstName.setValue(fname);
        await this.DoctorMiddleName.setValue(mname);
        await this.DoctorLastName.setValue(lname);
        await this.DoctorStreetAddress.setValue(address);
        await this.DoctorSuite.setValue(suite);
        await this.DoctorCity.setValue(city);
        await this.DoctorState.setValue(state);
        await this.DoctorZip.setValue(zip);
        await this.SaveDoctor.click();
    }


}

module.exports = new MedicalTreatmentObject();