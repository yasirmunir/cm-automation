class PatientObject {

    get Patient() {

        const elem = $("//a[.=' Patient ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get PatientList() {

        const elem = $("//a[.='Patient List']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get AddNew() {

        const elem = $("//a[.='Add New']")
        elem.waitForDisplayed({ timeout: 80000 });
        return elem
    }
    get FirstName() {
        const elem = $("//label[text()='First Name*']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get MiddleName() {
        const elem = $("//label[text()='Middle Name']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get LastName() {
        
        const elem = $("//label[text()='Last Name*']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Gender() {
        const elem = $("(//input[@type='radio']/following-sibling::span)[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get DateOfBirth() {
        const elem = $("//label[.='Date Of Birth* (mm/dd/yyyy)']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get SSN() {
        const elem = $("//label[.='SSN']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get HomePhone() {
        const elem = $("//label[normalize-space()='Home Phone']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get WorkPhone() {
        const elem = $("//label[.='Work Phone']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get CellPhone() {
        const elem = $("//label[.='Cell Phone*']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get Email() {
        
        const elem = $("//input[@id='email']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Address() {
        const elem = $("//label[.='Address*']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
       
    }
    get Suite() {
        const elem = $("//label[.='Suite / Floor']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get City() {
        const elem = $("//label[.='City']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get Zip() {
        
         const elem = $("//label[.='Zip']/following::input")
         elem.waitForDisplayed({ timeout: 30000 });
         return elem
         
    }
    get SaveAndContinue() {
        
        const elem = $("//button[.='Save & Continue']")
         elem.waitForDisplayed({ timeout: 30000 });
         return elem
    }
    get AddNewCase() {
        const elem = $("//a[.='Add New ']")
         elem.waitForDisplayed({ timeout: 80000 });
         return elem
        
    }
    get IamNewPatient() {
        const elem = $("//div[@class='row']/following::button/following::button/following::button/following::button/following::button")
        elem.waitForDisplayed({ timeout: 80000 });
        return elem
        
    }
    
    async MainMenu() {
        await this.Patient.click();
        await this.PatientList.click();
       // await this.AddNew.click();

    }
    async AddPatient(fname,mname,lname,dob,ssn,hp,wp,cp,email,address,suite,city) {

        await this.FirstName.setValue(fname);
        await this.MiddleName.setValue(mname);
        await this.LastName.setValue(lname);
        await this.Gender.click();
        await this.DateOfBirth.setValue(dob);
        await this.SSN.setValue(ssn);
        await this.HomePhone.setValue(hp);
        await this.WorkPhone.setValue(wp);
        await this.CellPhone.setValue(cp);
        await this.Email.setValue(email);
        await this.Address.setValue(address);
        await this.Suite.setValue(suite);
        await this.City.setValue(city);

        
    }
}

module.exports = new PatientObject();