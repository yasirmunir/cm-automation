class Appointment {
    get Scheduler() {
        const elem = $("//a[.=' Scheduler ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get ProviderCalender() {
        
        const elem = $("//a[.='Provider Calendar']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get PracticeLocation() {
       
        const elem = $("//div[.=' Practice-Location ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get LocationArea() {
        const elem = $$("//span[@tooltipclass='table-custom-tooltip']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get Provider() {

        const elem = $("//div[.=' Provider ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get ProviderList() {
        const elem = $$("//span[@tooltipclass='table-custom-tooltip']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get MonthView() {
        const elem = $("//button[.='Month']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get PreviousDate() {

        const elem = $("//div[@class='previous-date']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get CalenderDay() {
        const elem = $$("//span[.=contains(@class,'cal-day-number')]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get CaseNo() {

        const elem = $("(//input[@placeholder='Case No.'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Case() {
        
        const elem = $$("//a[@class='ng-star-inserted']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Billable() {
        const elem = $("//select[@formcontrolname='billable']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get Comments() {
        const elem = $("//textarea[@placeholder='No Comment Yet']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get SaveAndContinue() {
        const elem = $("//button[.=' Save & Continue']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    

    

    async MainMenu() {
        await this.Scheduler.click();
        await this.ProviderCalender.click();
        await this.PracticeLocation.click();
        
    }

}

module.exports = new Appointment();