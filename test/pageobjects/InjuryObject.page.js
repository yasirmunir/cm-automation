class InjuryObject {

    get Head() {
        
        const elem = $("//span[.='Head']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get PainScale() {
        
        const elem = $("//input[@type='range']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Prickling() {
        const elem = $("//span[.='Prickling']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
       
    }
    get SaveAndContinue() {
        
        const elem = $("//button[.='Save & Continue']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }

    async AddInjuryInfo() {
        await this.Head.click();
        await this.PainScale.click();
        await this.Prickling.click();
        await this.SaveAndContinue.click();
    }


}

module.exports = new InjuryObject();