class VisitPageObject {
    get Master() {
        const elem = $("//a[.=' Master ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Provider() {
        const elem = $("//a[.='Provider']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }

    get VisitType() {
        const elem = $("//a[text()='Visit Type']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get AddNewVisitTypeButton() {
        const elem = $("//button[text()=' Add New ']")
        elem.waitForDisplayed({ timeout: 60000 });
        return elem
       
    }
    get VisitName() {
       
        const elem = $("//input[@id='visit_name']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get description() {
        
        const elem = $("//textarea[@id='visit_description']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SaveAndContinue() {
        const elem = $("//button[text()='Save & Continue']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
     }
     get FilterVisitName() {
        const elem = $("//input[@id='name']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

     }
     get FilterComments() {
        
        const elem = $("//input[@id='description']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
     }
     get FIlter() {
        const elem = $("//button[.=' Filter ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
     }
     get Edit() {
        const elem = $("//i[@class='icon-pencil']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
         
     }
     get Update() {
        const elem = $("//button[.='Update']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
         
     }
      async MainPage() {
        await this.Master.click();
        await this.Provider.click();
        await this.VisitType.click();
     }

     async AddVisitType(name,desc) {
         
        await this.AddNewVisitTypeButton.click();
        await this.VisitName.setValue(name);
        await this.description.setValue(desc);
        await this.SaveAndContinue.click();
     }
     async FilterVisitType(name,comm) {
        await this.FilterVisitName.setValue(name);
        await this.FilterComments.setValue(comm);
        await this.FIlter.click();
     }
     async EditVisitType(name,desc) {
        await this.Edit.click();
        await this.VisitName.clearValue();
        await this.VisitName.setValue(name);
        await this.description.clearValue();
        await this.description.setValue(desc);
        await this.Update.click();
     }



}


module.exports = new VisitPageObject();