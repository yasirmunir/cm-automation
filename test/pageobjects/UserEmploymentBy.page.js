class UserEmploymentBy {
    get Master() {
        const elem = $("//a[.=' Master ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get User() {

        const elem = $("//a[.='User']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get EmploymentByButton() {
        const elem = $("//a[.='Employment By']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get AddNew() {

        const elem = $("//a[.=' Add New ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get EmploymentByName() {
        const elem = $("(//input[@formcontrolname='name'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Comments() {
        const elem = $("//textarea[@formcontrolname='comments']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Save() {
        const elem = $("//button[.='Save & Continue']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get FilterEmploymentByName() {

        const elem = $("//input[@id='EmploymentBy']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FilterComments() {
        const elem = $("//input[@id='comments']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Filter() {

        const elem = $("//button[.=' Filter ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Update() {

        const elem = $("//button[.='Update']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Edit() {
        const elem = $("//i[@class='icon-pencil']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }

    async MainMenu() {
        await this.Master.click();
        await this.User.click();
        await this.EmploymentByButton.click();
        await this.AddNew.click();

    }
    async AddEmploymentBy(name, cm) {
        await this.EmploymentByName.setValue(name);
        await this.Comments.setValue(cm);
        await this.Save.click();
    }
    async FilterEmploymentBy(name, cm) {
        await this.FilterEmploymentByName.setValue(name);
        await this.FilterComments.setValue(cm);
        await this.Filter.click();
    }
    async EditEmploymentBy(name, cm) {
        await this.Edit.click();
        await this.EmploymentByName.clearValue();
        await this.EmploymentByName.setValue(name);
        await this.Comments.clearValue();
        await this.Comments.setValue(cm);
        await this.Update.click();
    }


}

module.exports = new UserEmploymentBy();