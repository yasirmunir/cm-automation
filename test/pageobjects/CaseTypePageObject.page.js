class CaseTypePageObject {

    get Master() {
        const elem = $("//a[.=' Master ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Provider() {
        const elem = $("//a[.='Provider']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }

    get CaseTypeButton() {
        return $("//a[.='Case Type']")
    }

    get AddNewCase() {
        return $("//button[.=' Add New ']")
    }

    get CaseTypeName() {
        return $("//label[.='Case Type Name*']/following::input")
    }

    get NoOfDays() {
        return $("(//label[.='No. of Days Reminder'])[2]/following::input")
    }
    get description() {
        return $("//textarea[@formcontrolname='description']")
    }
    get comments() {
        return $("//textarea[@formcontrolname='comments']");
    }
    get Save() {
        return $("//button[normalize-space()='Save & Continue']")
    }
    get FilterCaseTypeName() {
        const elem =  $("//input[@id='name']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get FilterNoofDays() {
        const elem = $("//input[@id='remainder_days']");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem;
    }
    get FilterDescription() {
        const elem = $("//input[@id='description']");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem;
       
    }
    get FilterComments() {
        const elem = $("//input[@id='comments']");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem;
        
    }
    get Filter() {
        const elem = $("//button[.=' Filter ']");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem;
        
    }
    get Edit() {
        
        const elem = $("//i[@class='icon-pencil']");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem;
    }
    get Update() {
        const elem = $("//button[text()='Update']");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem;

    }

    async MainMenu() {
        await this.Master.click();
        await this.Provider.click();
        await this.CaseTypeButton.waitForDisplayed();
        await this.CaseTypeButton.click();
    }

    async AddCaseType(name, No, Des,comment) {
      
        await this.AddNewCase.waitForDisplayed();
        await this.AddNewCase.click();
        await this.CaseTypeName.waitForDisplayed();
        await this.CaseTypeName.setValue(name);
        await this.NoOfDays.waitForDisplayed();
        await this.NoOfDays.setValue(No);
        await this.description.waitForDisplayed();
        await this.description.setValue(Des)
        await this.comments.waitForDisplayed();
        await this.comments.setValue(comment);
        await this.Save.waitForDisplayed();
        await this.Save.click();
    }
    async FilterCaseType(name,no,des,com) {
        
        await this.FilterCaseTypeName.setValue(name);
        await this.FilterNoofDays.setValue(no);
        await this.FilterDescription.setValue(des);
        await this.FilterComments.setValue(com)
        await this.Filter.click();

    }

    async EditCaseType(name,no,des,com) {
        await this.Edit.click();
        await this.CaseTypeName.clearValue();
        await this.CaseTypeName.setValue(name);
        await this.NoOfDays.clearValue();
        await this.NoOfDays.setValue(no);
        await this.description.clearValue();
        await this.description.setValue(des);
        await this.comments.clearValue();
        await this.comments.setValue(com);
        await this.Update.click();
    }
}

module.exports = new CaseTypePageObject();