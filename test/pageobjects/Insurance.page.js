class InsurancePageObject {

    get Master() {
        const elem = $("//a[.=' Master ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }

    get Billing() {

        const elem = $("//a[.='Billing']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get InsuranceName() {
        const elem = $("(//input[@id='insurance_name'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get InsuranceCode() {
        const elem = $("//input[@id='insurance_code']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get AddLocation() {
        const elem = $("//button[.=' Add Location']")
        elem.waitForDisplayed({ timeout: 80000 });
        return elem

    }
    get Update() {
        const elem = $("//button[.='Update']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get AddNew() {
        const elem = $("//button[.='Add New']")
        elem.waitForDisplayed({ timeout: 80000 });
        return elem

    }
    get SetAsPrimary() {
        const elem = $("//label[@for='is_main_location']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get LocationName() {
        const elem = $("(//input[@formcontrolname='location_name'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get LocationCode() {
        const elem = $("//input[@formcontrolname='location_code']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get StreetAddress() {
        const elem = $("//input[@formcontrolname='street_address']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Suite() {
        const elem = $("//input[@formcontrolname='apartment_suite']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get City() {
        const elem = $("//input[@formcontrolname='city']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Zip() {
        const elem = $("//input[@formcontrolname='zip']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get PhoneNumber() {
        const elem = $("//input[@id='phone']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Extension() {
        const elem = $("//input[@id='Ext']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get CellNo() {

        const elem = $("//input[@id='Cell']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Fax() {
        const elem = $("//input[@id='fax']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Email() {

        const elem = $("//input[@id='email']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FirstName() {
        const elem = $("//input[@formcontrolname='contact_person_first_name']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get MiddleName() {
        const elem = $("//input[@formcontrolname='contact_person_middle_name']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get LastName() {
        const elem = $("//input[@formcontrolname='contact_person_last_name']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get CPPhoneNumber() {
        const elem = $("//input[@formcontrolname='contact_person_phone_no']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get CPExtension() {

        const elem = $("//input[@formcontrolname='contact_person_ext']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get CPCellNumber() {

        const elem = $("//input[@formcontrolname='contact_person_cell_number']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get CPFax() {

        const elem = $("//input[@formcontrolname='contact_person_fax']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get CPEmail() {

        const elem = $("//input[@formcontrolname='contact_person_email']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Comments() {

        const elem = $("//textarea[@formcontrolname='comments']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SaveAndContinue() {
        const elem = $("//button[.='Save & Continue']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get FilterInsuranceName() {

        const elem = $("//input[@id='insurance_name']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FilterCode() {
        const elem = $("//input[@id='Code']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get FilterLocation() {

        const elem = $("//input[@id='location']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FilterEmail() {
        const elem = $("//input[@id='Contact']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get FilterFax() {

        const elem = $("//input[@id='Fax']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FilterPhone() {

        const elem = $("//input[@id='cell']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Filter() {
        const elem = $("//button[.=' Filter ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get PlusIcon() {

        const elem = $("//span[@class='icon-plus']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Edit() {
        const elem = $("//button[text()=' Edit ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Update() {
        const elem = $("//button[.=' Update ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    async MainMenu() {
        await this.Master.click();
        await this.Billing.click();
    }

    async AddInsurance(name, code) {
        await this.InsuranceName.setValue(name);
        await this.InsuranceCode.setValue(code);

    }
    async AddInsuranceLocation(name, code,su) {
        await this.SetAsPrimary.click();
        await this.LocationName.setValue(name);
        await this.LocationCode.setValue(code);
        await this.Suite.setValue(su);
        
    }
    async AddLocationData( phone, ext, cell, fax, email) {
        await this.PhoneNumber.setValue(phone);
        await this.Extension.setValue(ext);
        await this.CellNo.setValue(cell);
        await this.Fax.setValue(fax);
        await this.Email.setValue(email);
    }
    async AddContactPersonData(fname, mname, lname, phone, ext, cell, fax, email, com) {
        await this.FirstName.setValue(fname);
        await this.MiddleName.setValue(mname);
        await this.LastName.setValue(lname);
        await this.CPPhoneNumber.setValue(phone);
        await this.CPExtension.setValue(ext);
        await this.CPCellNumber.setValue(cell);
        await this.CPFax.setValue(fax)
        await this.CPEmail.setValue(email)
        await this.Comments.setValue(com)
        await this.SaveAndContinue.click();
    }

    async FilterInsurance(name, code, loc) {
        await this.FilterInsuranceName.setValue(name);
        await this.FilterCode.setValue(code);
        await this.FilterLocation.setValue(loc);
        await this.PlusIcon.click();

    }
    async FilterInsuranceData(email, fax, phone) {
        await this.FilterEmail.setValue(email);
        await this.FilterFax.setValue(fax);
        await this.FilterPhone.setValue(phone);
        await this.Filter.click();

    }
    async EditInsurance(name, code) {
        await this.Edit.click();
        await this.InsuranceName.clearValue();
        await this.InsuranceName.setValue(name);
        await this.InsuranceCode.clearValue();
        await this.InsuranceCode.setValue(code);
        await this.Update.click();

    }
}



module.exports = new InsurancePageObject();