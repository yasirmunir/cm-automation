class VehicleObject {
    get AccidentReported() {
        const elem = $("//span[.='Was the accident reported? *']/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get ReportingDate() {
        const elem = $("//label[.='Reporting Date * (mm/dd/yyyy) ']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Precinct() {

        const elem = $("//label[.='Precinct']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }

    get VehicleWas() {
        const elem = $("//span[.='The vehicle was? ']/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get CarWas() {
        const elem = $("//span[.='Was this car? *']/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get HowManyVehicles() {
        const elem = $("//label[.='How many vehicles were involved in the accident?']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get OwnerOfVehicle() {
        const elem = $("//span[.='Were you the driver and the owner of the vehicle?']/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get VehicleButton() {
        const elem = $("//a[.='Add Vehicle']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Year() {

        const elem = $("//label[.='Year']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Make() {

        const elem = $("//label[.='Make']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Model() {

        const elem = $("//label[.='Model']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Color() {
        const elem = $("//label[.='Color']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get LisenceNo() {

        const elem = $("//label[.='License Plate No']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get PolicyNo() {
        const elem = $("//label[.='Policy Number']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }

    get EffectiveDate() {
        const elem = $("//label[.='Effective Date']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get ExpirationDate() {
        const elem = $("//label[.='Effective Date']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get DriverFirstName() {

        const elem = $("//label[.='Driver First Name']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get DriverMiddleName() {
        const elem = $("//label[.='Driver Middle Name']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get DriverLastName() {
        const elem = $("//label[.='Driver Last Name']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get DriverAddress() {
        const elem = $("//label[.='DriverAddress']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get DriverSuite() {
        const elem = $("//label[.='Suite / Floor']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get DriverCity() {
        const elem = $("//label[.='Driver City']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get DriverZip() {
        const elem = $("//label[.='Driver Zip']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get OwnerFirstName() {

        const elem = $("//label[.='Owner First Name']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get OwnerMiddleName() {
        const elem = $("//label[.='Owner Middle Name']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get OwnerLastName() {

        const elem = $("//label[.='Owner Last Name']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get OwnerAddress() {
        const elem = $("//label[.='OwnerAddress']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get OwnerSuite() {

        const elem = $("(//label[.='Suite / Floor'])[2]/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get OwnerCity() {
        const elem = $("//label[.='Owner City']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get OwnerZip() {
        const elem = $("//label[.='Owner Zip']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get SaveVehicle() {
        const elem = $("//button[.='Save']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get SaveAndContinue() {
        const elem = $("//button[.='Save & Continue']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }

    async AddDriverInfo(fname, mname, lname, address, suite, city) {
        await this.DriverFirstName.setValue(fname);
        await this.DriverMiddleName.setValue(mname);
        await this.DriverLastName.setValue(lname);
        await this.DriverAddress.setValue(address);
        await this.DriverSuite.setValue(suite);
        await this.DriverCity.setValue(city);
    }
    async AddOwnerInfo(fname,mname,lname,address,suite,city) {
        await this.OwnerFirstName.setValue(fname);
        await this.OwnerMiddleName.setValue(mname);
        await this.OwnerLastName.setValue(lname);
        await this.OwnerAddress.setValue(address);
        await this.OwnerSuite.setValue(suite);
        await this.OwnerCity.setValue(city);
    }

}
module.exports = new VehicleObject();