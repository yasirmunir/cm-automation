class ProviderAssignment {
    get Scheduler() {
        const elem = $("//a[.=' Scheduler ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Assignment() {
        const elem = $("//a[.='Assignment']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Provider() {

        const elem = $("(//a[.='Provider'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get PracticeLocation() {
        const elem = $$("//p[@tooltipclass='table-custom-tooltip']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Day() {
        const elem = $("//a[.='Day']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get ClickProviderUser() {
        const elem = $("//div[.=' Provider ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get ProviderList() {

        const elem = $$("//p[@container='body']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }

    get MonthView() {
        const elem = $("//a[.='Month']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get CalenderDaysList() {
        const elem = $$("//span[@class='cal-day-number']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem


    }
    get SaveAssignment() {
        const elem = $("//button[.='Save']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Recurrence() {


        const elem = $("//label[@for='recurrenceAfter']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get RangeOfRecurrence() {
        const elem = $("//label[@for='rangeRecurrence']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get EndAfter() {
        const elem = $("//span[.='End after']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get EndBy() {
        const elem = $("//span[.='End by']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get EndAfterNumber() {

        const elem = $("//input[@type='number']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Monday() {
        const elem = $("//input[@id='Mon']/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Tuesday() {
        const elem = $("//input[@id='Mon']/following::span/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Wednesday() {

        const elem = $("//input[@id='Mon']/following::span/following::span/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Thursday() {

        const elem = $("//input[@id='Mon']/following::span/following::span/following::span/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Friday() {

        const elem = $("//input[@id='Mon']/following::span/following::span/following::span/following::span/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Saturday() {

        const elem = $("//input[@id='Mon']/following::span/following::span/following::span/following::span/following::span/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get EndByDate() {

        const elem = $$("//td[@role='gridcell']/following::div")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get ClickEndByCalender() {
        const elem = $("(//span[@class='mat-button-wrapper'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Week() {
        const elem = $("//a[.='Week']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get WeekDay() {

        const elem = $("(//div[@class='cal-assign-doctor-week-view']/child::div/child::div/child::div/child::div)[6]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get DayView() {
        const elem = $("(//div[@class='row'])[9]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get DayButton() {

        const elem = $("//a[.='Day']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get ClickAssignment() {
        
        //const elem = $(".doc-label.col-md-12.text-truncate")
       const elem=$("//div[@class='doc-label col-md-12 text-truncate']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get ClickProviderWeekViewAssignment() {
        const elem=$("//div[@container='body']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get DeleteButton() {
        const elem = $("//a[@class='popover-icon-remove ng-star-inserted']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get DeleteAssignment() {
        const elem = $("//button[.='Delete']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get DeleteAllAssignments() {
        
        
        const elem = $("//label[@for='clinicName']//span[@class='fake-input position-absolute']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get EditAssignment() {
        const elem = $("(//span[@container='body']/following::span/following::a)[5]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get StartTime() {
        const elem = $("//label[.='Start Time*']/following::span/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get StartTimeInput() {
        const elem = $("//label[.='Start Time*']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get CancelTime() {
        const elem = $("//span[@aria-label='close']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
       
    }
    get EndTime() {
        
        const elem = $("//label[.='End Time* ']/following::span/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get EndTimeInput() {
        
        const elem = $("//label[.='End Time* ']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get UpdateAssignment() {
        
        const elem = $("//button[.=' Update ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Specialty() {
        const elem = $("//div[.=' Specialty ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get ChooseSpecialty() {
        
        const elem = $$("p[class$='clinic-name text-truncate mb-0']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SpecialtyMonthDate() {
        
        const elem = $$("span[class$='col-md-3 cal-day-number']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get DoNotAssign() {
        
        const elem = $("//label[@for='assignDoc']/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SaveSpecialtyAssignment() {
        const elem = $("//button[.='Save']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get SpecialtyLocation() {
        
        const elem = $$("//p[@tooltipclass='table-custom-tooltip']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SpecialtyPracticeLocationButton() {
        const elem = $("//div[.=' Practice-Location ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get SpecialtyClickAssignment() {
        const elem = $(".col-md-11.text-truncate")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get EditSpecialtyAssignment() {
        
        const elem = $("(//span[@container='body']/following::span/following::a)[4]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SpecialtyStartTime() {
        
        const elem = $("//label[.='Start Time*']/following::span/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SpecialtyStartTimeInput() {
        
        const elem = $("//label[.='Start Time*']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SpecialtyEndTime() {
        const elem = $("//label[.='End Time* ']/following::span/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get SpecialtyEndTimeInput() {
        const elem = $("//label[.='End Time* ']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get SpecialtyStartDate() {
        const elem = $("//label[.='Start Date*']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get UpdateSpecialtyAssignment() {
        const elem = $("//button[.='Update']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get SpecialtyDeleteButton() {
        
        const elem = $("(//span[@container='body']/following::span/following::a)[5]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get DeleteSpecialtyAssignment() {
        const elem = $("//button[.='Delete']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get SpecialtyRecurrence() {
        const elem = $("//span[.='Manual Assign']/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get SpecialtyRangeOfrecurrence() {
        
        const elem = $("//label[@for='range_recurrance']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SpecialtyEndAfter() {
        
        const elem = $("//label[@for='range_recurrance']/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get AllSubSequentAssignment() {
        
        const elem = $("//span[.='All Subsequent Assignment']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SpecialtyRepeatEvery() {
        
        const elem = $("//label[.=' Repeat every ']/following::select")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SpecialtySunday() {
       
        const elem = $("//label[.='on']/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SpecialtyMonday() {
        
        const elem = $("//label[.='on']/following::span/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SpecialtyTuesday() {
        
        const elem = $("//label[contains(text(),'Mon')]/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SpecialtyWednesday() {
        const elem = $("//label[contains(text(),'Tue')]/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get SpecialtyThursday() {
        const elem = $("//label[contains(text(),'Wed')]/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get SpecialtyFriday() {
        const elem = $("//label[contains(text(),'Thu')]/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get SpecialtySaturday() {
        
        const elem = $("//label[contains(text(),'Fri')]/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SpectaltyEndByDate() {
        const elem = $$("//div[@class='mat-calendar-body-cell-content']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get SpecialtyEndBy() {
        const elem = $("//span[.='End after']/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
  
    }
    get SpecialtyAutomaticAssign() {
        const elem = $("//span[.='Do not assign']/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }      
    get SpecialtyCalender() {
        
        const elem = $("//span[.='End by']/following::span/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get ManualAssign() {
        
        const elem = $("//span[.='Automatic Assign']/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SearchProvider() {
        
        const elem = $("//input[@placeholder='Search']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get doctorlist() {
        
        const elem = $$("//span[@class='col-md-12 doc-name']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SpecialtyWeekDay() {
        const elem = $("(//div[@class='cal-assign-doctor-week-view']/child::div/child::div/child::div/child::div)[4]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get ClickSpecialtyWeekAssignment() {
        const elem = $("(//div[@class='row'])[7]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        

    }
    get SpecEndTime() {
        const elem = $("//label[.='End Time* ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
       
    }

    get AssignProvider() {
        const elem = $("//label[.='Assigned Providers:']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get AutomaticSpecialtyUpdate() {
        const elem = $("//button[.='Update']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get ClickDayView() {
        
        const elem = $("(//div[contains(@class,'row')])[15]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get  ClickDayAssignment() {
        
        const elem = $("(//div[@class='row'])[9]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get NextMonth() {
       
        const elem = $("//button[@aria-label='Next month']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get ProviderAllSubSequentAssignment() {
       
        const elem = $("//span[.='All Subsequent Assignment']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get DeleteProviderAssignment() {
        const elem = $("//button[.='Delete']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }

    async MainMenu() {
        await this.Scheduler.click();
        await this.Assignment.click();
        await this.Provider.click();


    }
   
    async SpecialtyMainMenu() {
        await this.Scheduler.click();
        await this.Assignment.click();
        


    }

}

module.exports = new ProviderAssignment();