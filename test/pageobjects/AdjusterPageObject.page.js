class AdjusterPageObject {

    get Master() {
        const elem = $("//a[.=' Master ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }

    get Billing() {

        const elem = $("//a[.='Billing']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get AdjusterInformationButton() {

        const elem = $("//a[.='Adjuster Information']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get AddNew() {

        const elem = $("//button[.=' Add New ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SelectAdjuster() {

        const elem = $("//label[.='Insurance Name*']/following::ng-select")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FirstName() {

        const elem = $("//input[@formcontrolname='first_name']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get MiddleName() {

        const elem = $("//input[@formcontrolname='middle_name']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get LastName() {
        const elem = $("//input[@formcontrolname='last_name']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get StreetAddress() {

        const elem = $("//input[@formcontrolname='street_address']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Suite() {

        const elem = $("//input[@formcontrolname='apartment_suite']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get City() {

        const elem = $("//input[@formcontrolname='city']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Zip() {

        const elem = $("//input[@formcontrolname='zip']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get PhoneNumber() {

        const elem = $("(//input[@formcontrolname='phone_no'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Extension() {

        const elem = $("//input[@formcontrolname='ext']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get CellNo() {

        const elem = $("//input[@formcontrolname='cell_no']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Fax() {

        const elem = $("(//input[@formcontrolname='fax'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Email() {

        const elem = $("(//input[@formcontrolname='email'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Comments() {
        const elem = $("//textarea[@formcontrolname='comments']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get SaveAndContinue() {

        const elem = $("//button[.='Save & Continue']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Update() {

        const elem = $("//button[normalize-space()='Update']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FilterInsuranceName() {

        const elem = $("//input[@id='insurance_name']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FilterAdjusterName() {

        const elem = $("//input[@id='name']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FilterPhoneNo() {

        const elem = $("//input[@id='phone_no']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FilterFax() {
        const elem = $("//input[@id='fax']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get FilterEmail() {

        const elem = $("//input[@id='email']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Filter() {

        const elem = $("//button[.=' Filter ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Edit() {

        const elem = $("//i[@class='icon-pencil']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }

    async MainMenu() {
        await this.Master.click();
        await this.Billing.click();
        await this.AdjusterInformationButton.click();
        
    }
    async AddAdjusterInformation(fname, mname, lname, suite) {
        await this.FirstName.setValue(fname);
        await this.MiddleName.setValue(mname);
        await this.LastName.setValue(lname);
        await this.Suite.setValue(suite);
       
    }
    async AddAdjusterData(phone, ext, cell, fax, email, com) {
        
        await this.PhoneNumber.setValue(phone);
        await this.Extension.setValue(ext);
        await this.CellNo.setValue(cell);
        await this.Fax.setValue(fax);
        await this.Email.setValue(email);
        await this.Comments.setValue(com);
        await this.SaveAndContinue.click();

    }
    async FilterAdjuster(name, name2, phone, fax, email) {
        await this.FilterInsuranceName.setValue(name);
        await this.FilterAdjusterName.setValue(name2)
        await this.FilterPhoneNo.setValue(phone);
        await this.FilterFax.setValue(fax);
        await this.FilterEmail.setValue(email);
        await this.Filter.click();
    }
    async EditAdjusterInformation(fname, mname, lname, address, suite, city) {

        await this.FirstName.clearValue();
        await this.FirstName.setValue(fname);
        await this.MiddleName.clearValue();
        await this.MiddleName.setValue(mname);
        await this.LastName.clearValue();
        await this.LastName.setValue(lname);
        await this.StreetAddress.clearValue();
        await this.StreetAddress.setValue(address);
        await this.Suite.clearValue();
        await this.Suite.setValue(suite);
        await this.City.clearValue();
        await this.City.setValue(city);
    }

    async EditAdjusterData(zip, phone, ext, cell, fax, email, com) {
        await this.Zip.clearValue();
        await this.Zip.setValue(zip);
        await this.PhoneNumber.clearValue();
        await this.PhoneNumber.setValue(phone);
        await this.Extension.clearValue();
        await this.Extension.setValue(ext);
        await this.CellNo.clearValue();
        await this.CellNo.setValue(cell);
        await this.Fax.clearValue();
        await this.Fax.setValue(fax);
        await this.Email.clearValue();
        await this.Email.setValue(email);
        await this.Comments.clearValue();
        await this.Comments.setValue(com);
        await this.Update.click();

    }

}

module.exports = new AdjusterPageObject();