class FirmPageObject {
    get Master() {
        const elem = $("//a[.=' Master ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }

    get Billing() {

        const elem = $("//a[.='Billing']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get AttorneyButton() {

        const elem = $("//a[.='Attorney']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FirmTab() {

        const elem = $("//a[.='Firm ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get AddNew() {
        const elem = $("//button[.=' Add New ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get FirmName() {

        const elem = $("//input[@id='name']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get AddLocationButton() {

        const elem = $("//button[.=' Add Location ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SetAsPrimaryLocation() {

        const elem = $("//span[.='Set as Primary Location']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get LocationName() {
        const elem = $("//input[@formcontrolname='location_name']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get StreetAddress() {

        const elem = $("//input[@id='address']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Suite() {

        const elem = $("//input[@formcontrolname='apartment_suite']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get City() {
        const elem = $("(//input[@formcontrolname='city'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get State() {

        const elem = $("(//label[text()='State'])[2]/following::ng-select");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Zip() {
        const elem = $("(//input[@formcontrolname='zip'])[2]");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get PhoneNumber() {

        const elem = $("(//input[@formcontrolname='phone'])[2]");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Extension() {

        const elem = $("//input[@formcontrolname='ext']");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get CellNumber() {

        const elem = $("//input[@formcontrolname='cell']");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Fax() {
        const elem = $("(//input[@formcontrolname='fax'])[2]");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Email() {

        const elem = $("(//input[@formcontrolname='email'])[2]");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get FirstName() {

        const elem = $("//input[@formcontrolname='contact_person_first_name']");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get MiddleName() {

        const elem = $("//input[@formcontrolname='contact_person_middle_name']");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get LastName() {
        const elem = $("//input[@formcontrolname='contact_person_last_name']");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get CPPhoneNumber() {
        const elem = $("//input[@formcontrolname='contact_person_phone']");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem


    }
    get CPExtension() {

        const elem = $("//input[@formcontrolname='contact_person_ext']");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get CPCell() {

        const elem = $("//input[@formcontrolname='contact_person_cell']");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get CPFax() {

        const elem = $("//input[@formcontrolname='contact_person_fax']");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get CPEmail() {
        const elem = $("//input[@formcontrolname='contact_person_email']");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Comments() {

        const elem = $("//textarea[@formcontrolname='comments']");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SaveAndContinue() {

        const elem = $("//button[.='Save & Continue']");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FilterFirmName() {
        const elem = $("//input[@id='FirmName']");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get FilterLocationName() {
        const elem = $("//input[@id='location']");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get FilterStreetAddress() {
        const elem = $("//input[@id='street_address']");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get FilterCity() {
        const elem = $("//input[@id='city']");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get FilterState() {

        const elem = $("//input[@id='state']");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FilterZip() {
        const elem = $("//input[@id='zip']");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get FilterPhoneNumber() {
        const elem = $("//input[@id='phone']");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get FilterFax() {

        const elem = $("//input[@id='fax']");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FilterEmail() {

        const elem = $("//input[@id='email']");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Filter() {

        const elem = $("//button[.=' Filter ']");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get PlusIcon() {

        const elem = $("//span[@class='icon-plus']");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Update() {
        const elem = $("//button[.=' Update ']");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Edit() {

        const elem = $("//span[@class='icon-pencil']");
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }

    async MainMenu() {
        await this.Master.click();
        await this.Billing.click();
        await this.AttorneyButton.click();
        await this.FirmTab.click();
        

    }
    async AddFirm(name) {
        await this.FirmName.setValue(name);
        await this.AddLocationButton.click();


    }
    async AddLocation(loc,suite) {
        await this.SetAsPrimaryLocation.click();
        await this.LocationName.setValue(loc);
        await this.Suite.setValue(suite);
       

    }
    async AddLocationData(phone, ext, cell, fax, email) {
        
        await this.PhoneNumber.setValue(phone);
        await this.Extension.setValue(ext);
        await this.CellNumber.setValue(cell);
        await this.Fax.setValue(fax);
        await this.Email.setValue(email);
    }
    async AddContactPersonData(fname, mname, lname, phone, ext, cell, fax, email, com) {
        await this.FirstName.setValue(fname);
        await this.MiddleName.setValue(mname);
        await this.LastName.setValue(lname);
        await this.CPPhoneNumber.setValue(phone);
        await this.CPExtension.setValue(ext);
        await this.CPCell.setValue(cell);
        await this.CPFax.setValue(fax);
        await this.CPEmail.setValue(email);
        await this.Comments.setValue(com);
        await this.SaveAndContinue.click();

    }
    async FilterFirm(name, loc, address, city) {
        await this.FilterFirmName.setValue(name);
        await this.FilterLocationName.setValue(loc);
        await this.FilterStreetAddress.setValue(address);
        await this.FilterCity.setValue(city);



    }
    async FilterData(state, zip, phone, fax, email) {
        await this.FilterState.setValue(state);
        await this.FilterZip.setValue(zip);
        await this.FilterPhoneNumber.setValue(phone);
        await this.FilterFax.setValue(fax);
        await this.FilterEmail.setValue(email);
        await this.Filter.click();

    }

    async EditFirm(name) {
        await this.Edit.click();
        await this.FirmName.clearValue();
        await this.FirmName.setValue(name);
        await this.Update.click();

    }




}

module.exports = new FirmPageObject();