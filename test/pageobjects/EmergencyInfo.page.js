class EmergencyInfo {
    get FirstName() {

        const elem = $("//label[.='First Name *']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get MiddleName() {

        const elem = $("//label[text()='Middle Name']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get LastName() {

        const elem = $("//label[text()='Last Name *']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get DOB() {
        const elem = $("//label[.='Date of Birth (mm/dd/yyyy)']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get HomePhone() {

        const elem = $("//label[.='Home Phone']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get CellPhone() {

        const elem = $("//label[.='Cell Phone *']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Email() {

        const elem = $("//label[.='Email']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Fax() {

        const elem = $("//label[.='Fax']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get StreetAddress() {

        const elem = $("//label[.='Street Address']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Suite() {
        const elem = $("//label[.='Suite / Floor']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get City() {

        const elem = $("//label[.='City']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get State() {

        const elem = $("//label[.='State']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Zip() {

        const elem = $("//label[.='Zip']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SaveAndContinue() {
        const elem = $("//button[.='Save & Continue']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    async AddEmergencyContactInformation(fname, lname, mname, dob, home, cell, email, fax, address, suite, city, state, zip) {
        await this.FirstName.setValue(fname);
        await this.MiddleName.setValue(mname);
        await this.LastName.setValue(lname);
        await this.DOB.setValue(dob);
        await this.HomePhone.setValue(home);
        await this.CellPhone.setValue(cell);
        await this.Email.setValue(email);
        await this.Fax.setValue(fax);
        await this.StreetAddress.setValue(address);
        await this.Suite.setValue(suite);
        await this.City.setValue(city);
        await this.State.setValue(state);
        await this.Zip.setValue(zip);



    }
    async EditEmergencyContactInformation(fname, lname, mname, dob, home, cell, email, fax, address, suite, city, state, zip) {
        await this.FirstName.clearValue();
        await this.FirstName.setValue(fname);
        await this.MiddleName.clearValue();
        await this.MiddleName.setValue(mname);
        await this.LastName.clearValue();
        await this.LastName.setValue(lname);
        //await this.DOB.setValue(dob);
        await this.HomePhone.clearValue();
        await this.HomePhone.setValue(home);
        await this.CellPhone.clearValue();
        await this.CellPhone.setValue(cell);
        await this.Email.clearValue();
        await this.Email.setValue(email);
        await this.Fax.clearValue();
        await this.Fax.setValue(fax);
        await this.StreetAddress.clearValue();
        await this.StreetAddress.setValue(address);
        await this.Suite.clearValue();
        await this.Suite.setValue(suite);
        await this.City.clearValue();
        await this.City.setValue(city);
        await this.State.clearValue();
        await this.State.setValue(state);
        await this.Zip.clearValue();
        await this.Zip.setValue(zip);



    }



}

module.exports = new EmergencyInfo();