class AttorneyPageObject {
    get Master() {
        const elem = $("//a[.=' Master ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }

    get Billing() {

        const elem = $("//a[.='Billing']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get AttorneyButton() {

        const elem = $("//a[.='Attorney']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SubAttorneyButton() {
        const elem = $("(//a[.='Attorney'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get AddNew() {

        const elem = $("//button[.=' Add New ']")
        elem.waitForDisplayed({ timeout: 80000 });
        return elem
    }
    get SelectFirmName() {

        const elem = $("//label[.='Firm Name*']/following::ng-select")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get FirstName() {

        const elem = $("//input[@formcontrolname='first_name']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get MiddleName() {

        const elem = $("//input[@formcontrolname='middle_name']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get LastName() {

        const elem = $("//input[@formcontrolname='last_name']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get StreetAddress() {

        const elem = $("//input[@id='address']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Suite() {

        const elem = $("//input[@formcontrolname='suit_floor']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get City() {
        const elem = $("//input[@formcontrolname='city']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Zip() {
        const elem = $("//input[@formcontrolname='zip']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get PhoneNumber() {

        const elem = $("(//input[@formcontrolname='phone_no'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Extension() {

        const elem = $("//input[@formcontrolname='ext']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get CellNo() {
        const elem = $("//input[@formcontrolname='cell_no']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Fax() {

        const elem = $("(//input[@formcontrolname='fax'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Email() {

        const elem = $("(//input[@formcontrolname='email'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get CPFirstName() {
        const elem = $("(//input[@formcontrolname='first_name'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get CPMiddleName() {
        const elem = $("(//input[@formcontrolname='middle_name'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get CPLastName() {
        const elem = $("(//input[@formcontrolname='last_name'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get CPPhoneNumber() {
        const elem = $("(//input[@formcontrolname='phone_no'])[3]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get CPExtension() {
        const elem = $("//input[@formcontrolname='extension']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get CPCellNo() {

        const elem = $("(//input[@formcontrolname='cell_no'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get CPFax() {
        const elem = $("(//input[@formcontrolname='fax'])[3]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get CPEmail() {
        const elem = $("(//input[@formcontrolname='email'])[3]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Comments() {
        const elem = $("//textarea[@formcontrolname='comments']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get SaveAndContinue() {
        const elem = $("//button[.='Save & Continue']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get FilterFirmName() {

        const elem = $("//input[@id='AttorneyFirmName']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FilterAttorneyName() {
        const elem = $("//input[@id='name']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FilterPhoneNumber() {
        const elem = $("//input[@id='phone_no']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get FilterEmail() {
        const elem = $("//input[@id='email']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get FilterFax() {

        const elem = $("//input[@id='fax']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Filter() {
        const elem = $("//button[.='Filter']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem


    }
    get Edit() {

        const elem = $("//i[@class='icon-pencil']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Update() {
        
        const elem = $("//button[.='Update']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }




    async MainMenu() {
        await this.Master.click();
        await this.Billing.click();
        await this.AttorneyButton.click();
        await this.SubAttorneyButton.click();
       

    }
    async AddAttorney(fname, mname, lname,su) {
        await this.FirstName.setValue(fname);
        await this.MiddleName.setValue(mname);
        await this.LastName.setValue(lname);
        await this.Suite.setValue(su);
        
    }
    async AddAttorneyData(phone, ext, cell, fax, email) {
      
        await this.PhoneNumber.setValue(phone);
        await this.Extension.setValue(ext);
        await this.CellNo.setValue(cell);
        await this.Fax.setValue(fax);
        await this.Email.setValue(email);

    }
    async AddContactPersonData(fname, mname, lname, phone, ext, cell, fax, email, com) {
        await this.CPFirstName.setValue(fname);
        await this.CPMiddleName.setValue(mname);
        await this.CPLastName.setValue(lname);
        await this.CPPhoneNumber.setValue(phone);
        await this.CPExtension.setValue(ext);
        await this.CPCellNo.setValue(cell);
        await this.CPFax.setValue(fax);
        await this.CPEmail.setValue(email);
        await this.Comments.setValue(com);
        await this.SaveAndContinue.click();
    }
    async FilterAttorney(name, name2, phone, email, fax) {
        await this.FilterFirmName.setValue(name);
        await this.FilterAttorneyName.setValue(name2);
        await this.FilterPhoneNumber.setValue(phone);
        await this.FilterEmail.setValue(email)
        await this.FilterFax.setValue(fax);
        await this.Filter.click();

    }
    async EditAttorney(fname, mname, lname, address, suite, city) {

        await this.FirstName.clearValue();
        await this.FirstName.setValue(fname);
        await this.MiddleName.clearValue();
        await this.MiddleName.setValue(mname);
        await this.LastName.clearValue();
        await this.LastName.setValue(lname);
        await this.StreetAddress.clearValue();
        await this.StreetAddress.setValue(address);
        await this.Suite.clearValue();
        await this.Suite.setValue(suite);
        await this.City.clearValue();
        await this.City.setValue(city);
    }

    async EditAttorneyData(zip, phone, ext, cell, fax, email) {
        await this.Zip.clearValue();
        await this.Zip.setValue(zip);
        await this.PhoneNumber.clearValue();
        await this.PhoneNumber.setValue(phone);
        await this.Extension.clearValue();
        await this.Extension.setValue(ext);
        await this.CellNo.clearValue();
        await this.CellNo.setValue(cell);
        await this.Fax.clearValue();
        await this.Fax.setValue(fax);
        await this.Email.clearValue();
        await this.Email.setValue(email);

    }
    async EditContactPersonData(fname, mname, lname, phone, ext, cell, fax, email, com) {
        await this.CPFirstName.clearValue();
        await this.CPFirstName.setValue(fname);
        await this.CPMiddleName.clearValue();
        await this.CPMiddleName.setValue(mname);
        await this.CPLastName.clearValue();
        await this.CPLastName.setValue(lname);
        await this.CPPhoneNumber.clearValue();
        await this.CPPhoneNumber.setValue(phone);
        await this.CPExtension.clearValue();
        await this.CPExtension.setValue(ext);
        await this.CPCellNo.clearValue();
        await this.CPCellNo.setValue(cell);
        await this.CPFax.clearValue();
        await this.CPFax.setValue(fax);
        await this.CPEmail.clearValue();
        await this.CPEmail.setValue(email);
        await this.Comments.clearValue();
        await this.Comments.setValue(com);
        await this.Update.click();
    }



}

module.exports = new AttorneyPageObject();