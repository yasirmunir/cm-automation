class AccidentObject {
    get OccupationalDisease() {
        const elem = $("//span[.='Was this an occupational disease?*']/following::span/following::span/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get DOA() {
        const elem = $("//label[.='DOA * (mm/dd/yyyy)']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get InjuryHappened() {
        
        const elem = $("//label[.='Where did the injury/illness happen?']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get StreetAddress() {

        
        const elem = $("//label[.='Street Address']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Suite() {
        const elem = $("//label[.='Suite / Floor']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get City() {
        const elem = $("//label[.='City']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get State() {
        const elem = $("//label[.='State*']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get Zip() {
        
        const elem = $("//label[.='Zip']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get  BecameIll() {
        const elem = $("//label[.='What were you doing when you were injured or became ill? (e.g, unloading a truck, typing a report) *']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        

    }
    get illnessHappened() {
        const elem = $("//label[.='How did the injury/illness happen? (e.g., I tripped over a pipe and fell on the floor) *']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get DescribeInjury() {
       
        const elem = $("//label[.='Describe your injury?']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get NatureOfInjury() {
        const elem = $("//label[.='Explain fully the nature of your injury / illness(e.g., twisted left ankle and cut to forehead) *']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
       
      }
      get ObjectInvolved() {
        const elem = $("//span[.='Was an object involved in an injury/illness? *']/following::span/following::span/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
          
      }
      get WhoseVehicle() {
          
          const elem = $("//span[.='Whose vehicle was this?']/following::span")
          elem.waitForDisplayed({ timeout: 30000 });
          return elem
      }
      get LisenceMotor() {
        const elem = $("//span[.='Was the injury the result of the use or operation of a licensed motor vehicle? *']/following::span/following::span/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        }
        get WitnessOfAccident() {
            const elem = $("//span[.='Do you have any witness to the accident']/following::span//following::span//following::span");
            elem.waitForDisplayed({ timeout: 30000 });
            return elem
            
        }
        get FirstName() {
            
            const elem = $("//label[.='First Name *']/following::input")
            elem.waitForDisplayed({ timeout: 30000 });
            return elem
        }
        get MiddleName() {
            
            const elem = $("//label[.='Middle Name']/following::input")
            elem.waitForDisplayed({ timeout: 30000 });
            return elem
        }
        get LastName() {
            const elem = $("//label[.='Last Name *']/following::input")
            elem.waitForDisplayed({ timeout: 30000 });
            return elem
            
        }
        get WitnessStreetAddress() {
            const elem = $("(//label[.='Street Address'])[2]/following::input")
            elem.waitForDisplayed({ timeout: 30000 });
            return elem
            
        }
        get  WitnessSuite() {
            
            const elem = $("(//label[.='Suite / Floor'])[2]/following::input")
            elem.waitForDisplayed({ timeout: 30000 });
            return elem
        }
        get WitnessCity() {
            const elem = $("(//label[.='City'])[2]/following::input")
            elem.waitForDisplayed({ timeout: 30000 });
            return elem

            
        }
        get WitnessState() {
            
            const elem = $("//label[.='State']/following::input")
            elem.waitForDisplayed({ timeout: 30000 });
            return elem
        }
        get WitnessZip() {
            
            const elem = $("(//label[.='Zip'])[2]/following::input")
            elem.waitForDisplayed({ timeout: 30000 });
            return elem
        }
        get CellPhoneNo() {
            const elem = $("//label[.='Cell Phone No']/following::input")
            elem.waitForDisplayed({ timeout: 30000 });
            return elem
           
        }
        get SaveWitness() {
            
            const elem = $("//button[.='Save']")
            elem.waitForDisplayed({ timeout: 30000 });
            return elem
        }
        get SaveAndContinue() {
            
            const elem = $("//button[.=' Save & Continue']")
            elem.waitForDisplayed({ timeout: 30000 });
            return elem
        }
        get LisenceMotorVehicle() {
            const elem = $("//span[.='Was the injury the result of the use or operation of a licensed motor vehicle? *']/following::span/following::span/following::span")
            elem.waitForDisplayed({ timeout: 30000 });
            return elem
            
        }
        get TimeOfAccident() {
            const elem = $("//span[normalize-space()='At the time of accident I was? *']/following::span/following::span/following::span")
            elem.waitForDisplayed({ timeout: 30000 });
            return elem
            
        }
        get WorkPlace() {
            const elem = $("//span[.='Were you injured at your workplace? *']/following::span")
            elem.waitForDisplayed({ timeout: 30000 });
            return elem
            
        }
        get InjuryIllnessHappened() {
            const elem = $("//label[.='How did the injury/illness happen? (e.g., I tripped over a pipe and fell on the floor) *']/following::input")
            elem.waitForDisplayed({ timeout: 30000 });
            return elem
            
        }


    async AddAccidentInfo(inj,address,suite,city,state,zip,bi,ih,di,ni) {
       // await this.DOA.setValue(doa);
        await this.InjuryHappened.setValue(inj);
        await this.StreetAddress.setValue(address);
        await this.Suite.setValue(suite);
        await this.City.setValue(city);
        await this.State.setValue(state);
        await this.Zip.setValue(zip);
        await this.BecameIll.setValue(bi);
        await this.illnessHappened.setValue(ih),
        await this.DescribeInjury.setValue(di);
        await this.NatureOfInjury.setValue(ni);
        await this.ObjectInvolved.click();
        await this.WhoseVehicle.click();
       
       
        
    }
    async AddWCAccidentInfo(inj,address,suite,city,state,zip,bi,ih,di,ni) {
        // await this.DOA.setValue(doa);
         await this.InjuryHappened.setValue(inj);
         await this.StreetAddress.setValue(address);
         await this.Suite.setValue(suite);
         await this.City.setValue(city);
         await this.State.setValue(state);
         await this.Zip.setValue(zip);
         await this.BecameIll.setValue(bi);
         await this.illnessHappened.setValue(ih),
         await this.DescribeInjury.setValue(di);
         await this.NatureOfInjury.setValue(ni);
         await this.ObjectInvolved.click();
         
        
        
         
     }
    async AddWitness(fname,mname,lname,address,suite,city,state,zip,phone) {
        await this.FirstName.setValue(fname);
        await this.MiddleName.setValue(mname);
        await this.LastName.setValue(lname);
        await this.WitnessStreetAddress.setValue(address);
        await this.WitnessSuite.setValue(suite);
        await this.WitnessCity.setValue(city);
        await this.WitnessState.setValue(state);
        await this.WitnessZip.setValue(zip);
        await this.CellPhoneNo.setValue(phone);
        await this.SaveWitness.click();
    }
    async AddLienAccidentInfo(ilh) {
        await this.TimeOfAccident.click();
        await this.WorkPlace.click();
        await this.InjuryIllnessHappened.setValue(ilh);
        await this.SaveAndContinue.click();

    }

}
module.exports = new AccidentObject();