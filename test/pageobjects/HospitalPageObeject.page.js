class HospitalPageObject {

    get Master() {
        const elem = $("//a[.=' Master ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Practice() {
        const elem = $("//a[text()='Practice']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get HospitalButton() {

        const elem = $("//a[contains(text(),'Hospital')]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get AddNew() {
        const elem = $(".btn.btn-default.round-btn.float-right")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get HospitalName() {

        const elem = $("(//input[@formcontrolname='name'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get StreetAddress() {

        const elem = $("(//input[@formcontrolname='street_address'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Suite() {

        const elem = $("//input[@formcontrolname='apartment']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get City() {
        const elem = $("//input[@formcontrolname='city']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Zip() {
        const elem = $("//input[@formcontrolname='zip']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get PhoneNumber() {

        const elem = $("(//input[@formcontrolname='work_phone'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Extension() {
        const elem = $("//input[@id='ext']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Fax() {
        const elem = $("(//input[@id='fax'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Email() {
        const elem = $("(//input[@formcontrolname='email'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get FirstName() {

        const elem = $("//input[@formcontrolname='first_name']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get MiddleName() {
        const elem = $("//input[@formcontrolname='middle_name']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get LastName() {
        const elem = $("//input[@formcontrolname='last_name']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get CPPhoneNumber() {
        const elem = $("//input[@formcontrolname='phone_no']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get CPExtension() {
        const elem = $("//input[@formcontrolname='extension']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get CPCellNo() {
        const elem = $("//input[@formcontrolname='cell_no']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get CPFax() {

        const elem = $("(//input[@formcontrolname='fax'])[3]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get CPEmail() {

        const elem = $("(//input[@formcontrolname='email'])[3]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SaveAndContinue() {
        const elem = $("//button[normalize-space()='Save & Continue']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get FilterHospitalName() {
        const elem = $("//input[@id='HospitalName']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get FilterPhoneNumber() {

        const elem = $("//input[@id='work_phone']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FilterStreetAddress() {
        const elem = $("//input[@id='street_address']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem


    }
    get FilterEmail() {
        const elem = $("//input[@id='email']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get FilterFax() {
        const elem = $("//input[@id='fax']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get FilterContactPerson() {
        const elem = $("//input[@id='contact_person_name']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get PlusIcon() {
        const elem = $("//span[@class='icon-plus']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Filter() {
        const elem = $("//button[normalize-space()='Filter']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Update() {
        const elem = $("//button[.='Update']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get Edit() {
        const elem = $("//i[@class='icon-pencil']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }

   async MainMenu() {
       await this.Master.click();
       await this.Practice.click();
       await this.HospitalButton.click();
       await this.AddNew.click();
    }
   async AddHospital(name, address, suite, city) {
    await this.HospitalName.setValue(name);
    await this.StreetAddress.setValue(address);
    await this.Suite.setValue(suite);
    await this.City.setValue(city);
    }
    async AddHospitalData(zip, phone, ext, fax, email) {
        await this.Zip.setValue(zip);
        await this.PhoneNumber.setValue(phone);
        await this.Extension.setValue(ext);
        await this.Fax.setValue(fax);
        await this.Email.setValue(email);

    }

   async AddContactPersonData(fname, mname, lname, phone, ext, cell, fax, email) {
    await this.FirstName.setValue(fname);
    await this.MiddleName.setValue(mname);
    await this.LastName.setValue(lname);
    await this.CPPhoneNumber.setValue(phone);
    await this.CPExtension.setValue(ext);
    await this.CPCellNo.setValue(cell);
    await this.CPFax.setValue(fax);
    await this.CPEmail.setValue(email);
    await this.SaveAndContinue.click();

    }
    async FilterHospital(name,phone,street,email) {
        await this.FilterHospitalName.setValue(name);
        await this.FilterPhoneNumber.setValue(phone);
        await this.FilterStreetAddress.setValue(street);
        await this.FilterEmail.setValue(email);
       
    }
   async  FilterData(fax) {
    await this.FilterFax.setValue(fax);
    await this.Filter.click();
    }
   async EditHospital(name, address, suite, city) {
    await this.Edit.click();
    await this.HospitalName.clearValue();
    await this.HospitalName.setValue(name);
    await this.StreetAddress.clearValue();
    await  this.StreetAddress.setValue(address);
    await  this.Suite.clearValue();
    await this.Suite.setValue(suite);
    await this.City.clearValue();
    await  this.City.setValue(city);
    }
    async EditHospitalData(zip, phone, ext, fax, email) {
        await  this.Zip.clearValue();
        await this.Zip.setValue(zip);
        await this.PhoneNumber.clearValue();
        await this.PhoneNumber.setValue(phone);
        await this.Extension.clearValue();
        await this.Extension.setValue(ext);
        await this.Fax.clearValue();
        await this.Fax.setValue(fax);
        await this.Email.clearValue();
        await this.Email.setValue(email);

    }
    async EditContactPersonData(fname, mname, lname, phone, ext, cell, fax, email) {
        await this.FirstName.clearValue();
        await this.FirstName.setValue(fname);
        await this.MiddleName.clearValue();
        await this.MiddleName.setValue(mname);
        await this.LastName.clearValue();
        await this.LastName.setValue(lname);
        await this.CPPhoneNumber.clearValue();
        await this.CPPhoneNumber.setValue(phone);
        await this.CPExtension.clearValue();
        await this.CPExtension.setValue(ext);
        await this.CPCellNo.clearValue();
        await this.CPCellNo.setValue(cell);
        await this.CPFax.clearValue();
        await this.CPFax.setValue(fax);
        await this.CPEmail.clearValue();
        await this.CPEmail.setValue(email);
        await this.Update.click();

    }

}

module.exports = new HospitalPageObject();