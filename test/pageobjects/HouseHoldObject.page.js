class HouseHoldObject {
    get LiveWithYou() {

        const elem = $("//span[.='Did anyone live with you at the time of accident? *']/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get FirstName() {
        const elem = $("//label[.='First Name*']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem


    }
    get MiddleName() {
        const elem = $("//label[.='Middle Name']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get LastName() {

        const elem = $("//label[.='Last Name*']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SSN() {
        const elem = $("//label[.='SSN']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get DOB() {

        const elem = $("//label[.='Date of birth (mm/dd/yyyy)']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get CellNumber() {

        const elem = $("//label[.='Cell Phone Number']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get PolicyNumber() {
        const elem = $("//label[.='Policy No']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem


    }
    get EffectiveDate() {

        const elem = $("//label[.='Effective Date (mm/dd/yyyy)']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get ExpirationDate() {
        const elem = $("//label[.='Expiration Date (mm/dd/yyyy)']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem


    }
    get OwnMotorVehicle() {

        const elem = $("//span[.='Does this person own a motor vehicle? ']/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SaveHouse() {
        const elem = $("//button[.='Save']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get SaveAndContinue() {

        const elem = $("//button[.='Save & Continue']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }

    async AddHouseHoldInfo(fname, mname, lname, ssn, dob, cell) {
        await this.FirstName.setValue(fname);
        await this.MiddleName.setValue(mname);
        await this.LastName.setValue(lname);
        await this.SSN.setValue(ssn);
        await this.DOB.setValue(dob);
        await this.CellNumber.setValue(cell);
    }

}

module.exports = new HouseHoldObject();