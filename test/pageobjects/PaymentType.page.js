class PaymentTypePageObject {
    get Master() {
        const elem = $("//a[.=' Master ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }

    get Billing() {

        const elem = $("//a[.='Billing']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SubBilling() {

        const elem = $("(//a[.='Billing'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }

    get AddNew() {

        const elem = $("//a[.=' Add ']")
        elem.waitForDisplayed({ timeout: 80000 });
        return elem
    }
    get PaymentTypeName() {
        

        const elem = $("(//input[@formcontrolname='name'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Description() {

        const elem = $("//textarea[@formcontrolname='description']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Comments() {

        const elem = $("//textarea[@formcontrolname='comments']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SaveAndContinue() {

        const elem = $("//button[.='Save & Continue']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get FilterPaymentTypeName() {
        const elem = $("//input[@id='PaymentType']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get FilterDescription() {
        const elem = $("//input[@id='description']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get FilterComments() {

        const elem = $("//input[@id='comments']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Filter() {
        const elem = $("//button[.=' Filter ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Edit() {

        const elem = $("//i[@class='icon-pencil']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Update() {
        const elem = $("//button[.='Update']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }

    async MainMenu() {
        await this.Master.click();
        await this.Billing.click();
        await this.SubBilling.click();
        await this.AddNew.click();
    }
    async AddPaymentType(name, des, com) {
        await this.PaymentTypeName.setValue(name);
        await this.Description.setValue(des);
        await this.Comments.setValue(com);
        await this.SaveAndContinue.click();
    }
    async FilterPaymentType(name, des, com) {
        await this.FilterPaymentTypeName.setValue(name);
        await this.FilterDescription.setValue(des);
        await this.FilterComments.setValue(com);
        await this.Filter.click();

    }
    async EditPaymentType(name, des, com) {
        await this.Edit.click();
        await this.PaymentTypeName.clearValue();
        await this.PaymentTypeName.setValue(name);
        await this.Description.clearValue();
        await this.Description.setValue(des);
        await this.Comments.clearValue();
        await this.Comments.setValue(com);
        await this.Update.click();
    }



}

module.exports = new PaymentTypePageObject();