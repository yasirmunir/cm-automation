class BasicInformationObject {

    get HomePhone() {

        const elem = $("//label[text()='Home Phone']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get CellPhone() {

        const elem = $("//label[text()='Cell Phone *']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get WorkPhone() {
        const elem = $("//label[text()='Work Phone']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }


    get Extension() {
        const elem = $("//label[text()='Extention']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get StreetAddress() {

        const elem = $("//label[.='Street Address *']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Suite() {
        const elem = $("//label[text()='Suite / Floor']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Email() {

        const elem = $("//label[text()='Email']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Fax() {

        const elem = $("//label[text()='Fax']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get City() {

        const elem = $("//label[text()='City']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get State() {
        const elem = $("//label[text()='State']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Zip() {
        const elem = $("//label[text()='Zip']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get ResidentialAddress() {
        const elem = $("//span[.='Is your residential address same as mailing address? *']/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get EnterInfoSelf() {
        const elem = $("//span[contains(text(),'Is the patient entering this information themselve')]/following::span/following::span/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem


    }
    get SaveAndContinue() {
        const elem = $("//button[.='Save & Continue']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }

    async AddBasicContactInformation(hp, cp, wp, ex, ad, suite, city, state, zip, email, fax) {
        await this.HomePhone.clearValue();
        await this.HomePhone.setValue(hp);
        await this.CellPhone.clearValue();
        await this.CellPhone.setValue(cp);
        await this.WorkPhone.clearValue();
        await this.WorkPhone.setValue(wp);
        await this.Extension.clearValue();
        await this.Extension.setValue(ex);
        await this.StreetAddress.clearValue();
        await this.StreetAddress.setValue(ad);
        await this.Suite.clearValue();
        await this.Suite.setValue(suite);
        await this.City.clearValue();
        await this.City.setValue(city);
        await this.State.clearValue();
        await this.State.setValue(state);
        await this.Zip.clearValue();
        await this.Zip.setValue(zip);
        await this.Email.clearValue();
        await this.Email.setValue(email);
        await this.Fax.clearValue();
        await this.Fax.setValue(fax);
        await this.ResidentialAddress.click();
        await this.EnterInfoSelf.click();
        await this.SaveAndContinue.click();



    }
    async EditBasicContactInformation(hp, cp, wp, ex, ad, suite, city, state, zip, email, fax) {
        await this.HomePhone.clearValue();
        await this.HomePhone.setValue(hp);
        await this.CellPhone.clearValue();
        await this.CellPhone.setValue(cp);
        await this.WorkPhone.clearValue();
        await this.WorkPhone.setValue(wp);
        await this.Extension.clearValue();
        await this.Extension.setValue(ex);
        await this.StreetAddress.clearValue();
        await this.StreetAddress.setValue(ad);
        await this.Suite.clearValue();
        await this.Suite.setValue(suite);
        await this.City.clearValue();
        await this.City.setValue(city);
        await this.State.clearValue();
        await this.State.setValue(state);
        await this.Zip.clearValue();
        await this.Zip.setValue(zip);
        await this.Email.clearValue();
        await this.Email.setValue(email);
        await this.Fax.clearValue();
        await this.Fax.setValue(fax);
        await this.SaveAndContinue.click();



    }
}


module.exports = new BasicInformationObject();