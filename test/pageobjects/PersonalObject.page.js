class PersonalObject {

    get Pessenger() {

        const elem = $("//span[.='Passenger']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Pedestrain() {
        const elem = $("//span[.='Pedastrian']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get SSN() {
        const elem = $("//label[.='SSN']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Weight() {
        const elem = $("//label[.='Weight in lbs.']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get Height() {
        const elem = $("//label[.='Height in Ft.']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get Heightinches() {
        
        const elem = $("//label[.='Height in Inches']/following::input")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get MartialStatus(){
        const elem = $("//span[.='Married']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get Translator() {
        const elem = $("//span[.='Will you need a translator? *']/following::span")
        
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get NoTranslator() {
        const elem = $("//span[.='Will you need a translator? *']/following::span/following::span/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get EnforcementAgent() {
        
        const elem = $("//span[.='Are you a law enforcement agent? *']/following::span/following::span/following::span")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get SaveAndContinue() {
        const elem = $("//button[.='Save & Continue']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
       
    }

    

    async AddPersonalInfo(ssn,wt,he,inches) {
        await this.Pedestrain.click();
        await this.SSN.setValue(ssn);
        await this.Weight.setValue(wt);
        await this.Height.setValue(he);
        await this.Heightinches.setValue(inches);
        await this.MartialStatus.click();
        await this.NoTranslator.click();
        
       
    }
    async AddWCPersonalInfo(ssn,wt,he,inches) {
        
        await this.SSN.setValue(ssn);
        await this.Weight.setValue(wt);
        await this.Height.setValue(he);
        await this.Heightinches.setValue(inches);
        await this.MartialStatus.click();
        await this.NoTranslator.click();
        
       
    }
    async EditWCPersonalInfo(ssn,wt,he,inches) {
        
        await this.SSN.setValue(ssn);
        await this.SSN.clearValue();
        await this.Weight.clearValue();
        await this.Weight.setValue(wt);
        await this.Height.clearValue();
        await this.Height.setValue(he);
        await this.Heightinches.clearValue();
        await this.Heightinches.setValue(inches);
       
        
       
    }



}



module.exports = new PersonalObject();