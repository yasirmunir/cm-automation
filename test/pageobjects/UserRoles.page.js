class UserRoles {
    get Master() {
        const elem = $("//a[.=' Master ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get User() {

        const elem = $("//a[.='User']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get UserRole() {
        const elem = $("//a[.='User Roles']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get AddNew() {

        const elem = $("//a[.=' Add New ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get UserRoleName() {

        const elem = $("(//input[@formcontrolname='name'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Qualifier() {

        const elem = $("(//input[@formcontrolname='qualifier'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Comments() {
        const elem = $("//textarea[@formcontrolname='comment']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get MedicalIdentifier() {
        const elem = $("//mat-checkbox[@id='amateur']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Save() {
        const elem = $("//button[.='Save & Continue']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get FilterUserRoleName() {
        const elem = $("//input[@id='Name']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get FilterQualifier() {
        const elem = $("//input[@id='qualifier']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get FilterComments() {
        const elem = $("//input[@id='comments']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get Filter() {

        const elem = $("//button[.='Filter']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Update() {

        const elem = $("//button[.='Update']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Edit() {
        const elem = $("//i[@class='icon-pencil']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }

    async MainMenu() {
        await this.Master.click();
        await this.User.click();
        await this.UserRole.click();
        await this.AddNew.click();


    }
    async AddUserRole(name, qf, cm) {
        await this.UserRoleName.setValue(name);
        await this.Qualifier.setValue(qf);
        await this.Comments.setValue(cm);
        await this.MedicalIdentifier.click();
        await this.Save.click();
    }
    async FilterUserRoles(name, qf, cm) {
        await this.FilterUserRoleName.setValue(name);
        await this.FilterQualifier.setValue(qf);
        await this.FilterComments.setValue(cm);
        await this.Filter.click();

    }

    async EditUserRole(name, qf, cm) {
        await this.Edit.click();
        await this.UserRoleName.clearValue();
        await this.UserRoleName.setValue(name);
        await this.Qualifier.clearValue();
        await this.Qualifier.setValue(qf);
        await this.Comments.clearValue();
        await this.Comments.setValue(cm);
        await this.Update.click();
    }




}

module.exports = new UserRoles();