class BillingObject {

    get Billing() {
        const elem = $("//a[.=' Billing ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem

    }
    get UnBilledVisits() {
        const elem = $("//a[.='Unbilled Visit']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get PlusIcon() {
        const elem = $("//span[@class='icon-plus']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get Status() {
        const elem = $("//label[.='Status']/following::ng-select")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Billable() {
        const elem = $("//label[.='Billable']/following::ng-select")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get Filter() {
        
        const elem = $("//button[.=' Filter ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get CopyRight() {
        
        const elem = $("//p[.='Copyrights@ovada.com']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Actions() {
        
        const elem = $("//div[.=' Actions ']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
    }
    get Row() {
        const elem = $("(//div[@class='row'])[4]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get Checkbox() {
        
        const elem = $("//label[@for='mat-checkbox-2-input']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
       
    }
    get CreateBill() {
        
        const elem = $("//button[contains(.,'Create Bill [1]')]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
       
    }
    get ICDCode() {
        const elem = $("//input[@aria-autocomplete='list']")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }
    get CancelIcon() {
        const elem = $("(//mat-icon[.='cancel'])[2]")
        elem.waitForDisplayed({ timeout: 30000 });
        return elem
        
    }

    async MainMenu() {
        await this.Billing.click();
        await this.UnBilledVisits.click();
        await this.PlusIcon.click();

    }

}

module.exports = new BillingObject();