const user = require("../pageobjects/UserRoles.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['UserRoles'];
let json = utils.sheet_to_json(worksheet);
let UserRoleName;
let Qualifier;
let Comments;
let EditUserRoleName;
let EditQualifier;
let EditComments;
describe('UserRoles Page', () => {
    it('User should login with valid credentials', async () => {
        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await user.MainMenu();


    });

    for (let i = 0; i < json.length; i++) {

        UserRoleName = json[i]['UserRoleName'] ? json[i]['UserRoleName'] : '';
        Qualifier = json[i]['Qualifier'] ? json[i]['Qualifier'] : '';
        Comments = json[i]['Comments'] ? json[i]['Comments'] : '';
        EditUserRoleName = json[i]['EditUserRoleName'] ? json[i]['EditUserRoleName'] : '';
        EditQualifier = json[i]['EditQualifier'] ? json[i]['EditQualifier'] : '';
        EditComments = json[i]['EditComments'] ? json[i]['EditComments'] : '';


        it('Add New User Role', async () => {

            await user.AddUserRole(UserRoleName, Qualifier, Comments);
            await browser.pause(4000);



        });
        it('Filter User Roles', async () => {
            await user.FilterUserRoles(UserRoleName, Qualifier, Comments);
            await browser.pause(4000);

        });

        it('Edit User Role', async () => {
            await user.EditUserRole(EditUserRoleName, EditQualifier, EditComments);
            await browser.pause(4000);

        });
    }

});
