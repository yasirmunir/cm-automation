const assignment = require("../pageobjects/ProviderAssignment.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['SpecialtyAssignment'];
let json = utils.sheet_to_json(worksheet);
let PracticeLocationName;
let SpecialtyName;
let CalenderDate;


describe('Specialty Day View  Assignment', () => {

    it('User should login with valid credentials', async () => {
        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await assignment.SpecialtyMainMenu();
        await browser.pause(6000);


    });
    for (let i = 0; i < json.length; i++) {

        PracticeLocationName = json[i]['PracticeLocationName'] ? json[i]['PracticeLocationName'] : '';
        SpecialtyName = json[i]['SpecialtyName'] ? json[i]['SpecialtyName'] : '';
        CalenderDate = json[i]['CalenderDate'] ? json[i]['CalenderDate'] : '';

        it('Add Specialty Day View Automatic Assignment', async () => {

            await assignment.SpecialtyPracticeLocationButton.click();
            await browser.pause(10000);
            let Location = await assignment.SpecialtyLocation
            for (let i = 0; i < Location.length; i++) {
                const element = await Location[i];
                if (await element.getText() === PracticeLocationName) {
                    await element.click();
                    break;
                }

            }
            await assignment.Specialty.click();
            await browser.pause(5000);

            let SpecialtyUser = await assignment.ChooseSpecialty
            for (let i = 0; i < SpecialtyUser.length; i++) {
                const element = await SpecialtyUser[i];
                if (await element.getText() === SpecialtyName) {
                    await element.click();
                    break;
                }

            }

            await browser.pause(4000);
            await assignment.DayButton.click();
            await assignment.ClickDayView.doubleClick();
            await browser.pause(10000);
            await assignment.SpecialtyAutomaticAssign.click();
            await assignment.SaveSpecialtyAssignment.click();
            await browser.pause(6000);

            //--------------------------------------------Update Specialty Assignment--------------------------------------------//

            await assignment.ClickDayAssignment.click();
            await browser.pause(5000);
            await assignment.EditSpecialtyAssignment.click();
            await assignment.SpecialtyStartTime.click();
            await assignment.SpecialtyStartTimeInput.setValue("06.00 PM");
            await assignment.SpecialtyEndTime.click();
            await assignment.SpecialtyEndTimeInput.setValue("06.30 PM")
            await browser.pause(1000);
            await assignment.SpecEndTime.click();
            await assignment.UpdateSpecialtyAssignment.click();
            await browser.pause(5000);

            //------------------------------------------Delete Assignment--------------------------------------------------------//

            await assignment.ClickDayAssignment.click();
            await assignment.SpecialtyDeleteButton.click();
            await browser.pause(5000);
            await assignment.DeleteSpecialtyAssignment.click();



            await browser.pause(10000);

        });
    }



});


