const planName = require("../pageobjects/PlanNameObject.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx";
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['PlanName'];
let json = utils.sheet_to_json(worksheet);
let PlanName;
let Comments;
let EditPlanName;
let EditComments;
describe('Plan Name Page', () => {
    it('Should Login with valid Credentials', async () => {


        await browser.url(configData.baseURl);
        await console.log("Url Launched");
        await browser.maximizeWindow();
        await console.log("Window Maximized");
        await loginPage.Login(configData.username, configData.password);
        await console.log("User Logged In Successfully");
       await  planName.MainMenu();
       await console.log("User will add Plan Name");

    });
    for (let i = 0; i < json.length; i++) {

        PlanName = json[i]['PlanName'] ? json[i]['PlanName'] : '';
        Comments = json[i]['Comments'] ? json[i]['Comments'] : '';
        EditPlanName = json[i]['EditPlanName'] ? json[i]['EditPlanName'] : '';
        EditComments = json[i]['EditComments'] ? json[i]['EditComments'] : '';

        it('Add Plan Name', async () => {
           await planName.AddPlanName(PlanName, Comments);
           await console.log("Plan Name Added Successfully");
            await browser.pause(3000);


        });
    }
    it('Filter Plan Name', async () => {
        await planName.FilterPlanName(PlanName, Comments);
        await console.log("Plan Name Filtered Successfully");
       await  browser.pause(5000);

    });

    it('Edit Plan Name', async () => {
       await  planName.EditPlanName(EditPlanName, EditComments);
       await console.log("Plan Name Editeed Successfully");
        await browser.pause(5000);

    });


})

