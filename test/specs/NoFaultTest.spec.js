const patient = require("../pageobjects/PatientObject.page");
const personal = require("../pageobjects/PersonalObject.page");
const BasicInfo = require("../pageobjects/BasicInformationObject.page");
const FormFiller = require("../pageobjects/FormFillerObject.object");
const Emergency = require("../pageobjects/EmergencyInfo.page");
const Referral = require("../pageobjects/ReferralsObject.page");
const Insurance = require("../pageobjects/InsuranceObject.page");
const Employer = require("../pageobjects/EmployerObject.page");
const Accident = require("../pageobjects/AccidentObject.page");
const Vehicle = require("../pageobjects/VehicleObject.page");
const HouseHold = require("../pageobjects/HouseHoldObject.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const Medical = require("../pageobjects/MedicalTreatmentObject.page");
const Injury = require("../pageobjects/InjuryObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['NoFaultCase']
let json = utils.sheet_to_json(worksheet);
//---------------------------------------Patient Test Data-------------------------------------//
/*
let FirstName;
let MiddleName;
let LastName;
let SSN;
let HomePhone;
let WorkPhone;
let CellPhone;
let Email;
let Address;
let Suite;
let City;
let State;
let Zip;
//------------------------------Case Info Test Data-------------------------------------//
let PracticeLocation;
let Category;
let PurposeOfVisit;
let CaseType;
let DOA;
//--------------------------Personal Test Data------------------------------------------//
let PersonalSSN;
let Weight;
let Height;
let Inches;
//-----------------------------------Basic Contact Information Test Data-------------------------------//
let BHomePhone;
let BCellPhone;
let BWorkPhone;
let BExtension;
let BStreetAddress;
let BSuite;
let BCity;
let BState;
let BZip;
let BEmail;
let BFax;
//-------------------------------------Form Filler Test Data------------------------------------------//
let FFirstName;
let FMiddleName;
let FLastName;
let FCellPhone;
let FEmail;
let FFax;
let FStreetAddress;
let FSuite;
let FCity;
let FState;
let FZip;
let FRelation;
//------------------------------------Emergency Contact Information Test Data----------------------------------------//
let EFirstName;
let EMiddleName;
let ELastName;
let EDOB;
let EHomePhone;
let ECellPhone;
let EEmail;
let EFax;
let EStreetAddress;
let ESuite;
let ECity;
let EState;
let EZip;
let ERelation;
//----------------------------------- Refferals Test Data------------------------------------------------------------//
let RPFirstName;
let RPMiddleName;
let RPLastName;
let RPClinicName;
let RPEmail;
let RPhoneNo;
let RPExtension;
let RPAddress;
let RPSuite;
let RPCity;
let RPState;
let RPZip;
let PPFirstName;
let PPMiddleName;
let PPLastName;
let PPClinicName;
let PPEmail;
let PPPhoneNo;
let PPExtension;
let PPAddress;
let PPSuite;
let PPCity;
let PPState;
let PPZip;
// ------------------------------------------------------Insurance Test Data---------------------------------------//
let PInsuranceName;
let PMemberID;
let PGroupNo;
let PPriorAuthNo;
let NFInsuranceName;
let NFPHFirstName;
let NFPHMiddleName;
let NFPHLastName;
let NFPHClaimNo;
let NFPHPolicyNo;
let NFPHWcbNo;
let NFPHPriorAuth;
let SInsuranceName;
let SPHFirstName;
let SPHMiddleName;
let SPHLastName;
let SPHClaimNo;
let SPHPolicyNo;
let SPHWcbNo;
let SPHPriorAuth;
let TInsuranceName;
let TPHFirstName;
let TPHMiddleName;
let TPHLastName;
let TPHClaimNo;
let TPHPolicyNo;
let TPHWcbNo;
let TPHPriorAuth;
//---------------------------------------------Attorney Test Data------------------------------------------------------//
let FirmName;
let AttorneyName;
//--------------------------------------------Employer Test Data------------------------------------------------//

let PEmployerName;
let PPatientOccupation;
let PHiringDate;
let PFirstName;
let PMiddleName;
let PLastName;
let SEmployerName;
let SPatientOccupation;
let SHiringDate;
let YearlyEmployerName;
let YearlyEmployerOccupation;
let YearlyHiringDate;
let YearlyEndDate;
let JobTitle;
let TypeOfActivity;
let GrossPay;
let WeeklyPay;
let Days;
let Hours;
let WhatDate;
let ReturnWork;
let NFirstName;
let NMiddleName;
let NLastName;
let NoticeDate;
//------------------------------------------------Accident Test Data-----------------------------------------------------//

let InjuryHappened;
let AStreetAddress;
let ASuite;
let ACity;
let AState;
let AZip;
let BecameIll;
let IllnessHappened;
let DescribeInjury;
let NatureInjury;
//---------------------------------------------Accident Test Data-------------------------------------------------//
let ReportDate;
let Precincit;
let VState;
let VCity;
let VCounty;
let ManyVehicles;
let VYear;
let VMake;
let VModel;
let VColor;
let VLisenceNo;
let VehState;
let VInsuranceName;
let VPolicyNo;
let DFirstName;
let DMiddleName;
let DLastName;
let DAddress;
let DSuite;
let DCity;
let DState;
let DZip;
let OFirstName;
let OMiddleName;
let OLastName;
let OAddress;
let OSuite;
let OCity;
let OState;
let OZip;
//---------------------------------------------HouseHold Information Test Data---------------------------------------------//
let HHFirstName;
let HHMiddleName;
let HHLastName;
let HHSSN;
let HHDOB;
let HHCellNumber;
let HHRelation;
let HHInsuranceName;
let HHPolicyNo;
let HHEffectiveDate;
let HHExpirationDate;
//--------------------------------------------Medical Treatment-----------------------------------------------------//
let MFirstTreatmentDate;
let MDateOfTreatment;
let MedicalHospitalName;
let DocFirstName;
let DocMiddleName;
let DocLastName;
let DocAddress;
let DocSuite;
let DocCity;
let DocState;
let DocZip;
let Amount;
*/


describe('NO Fault Case Page', () => {
    it('Should Login with valid credentials', async () => {
        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await patient.MainMenu();


    });
    for (let i = 0; i < json.length; i++) {
        //----------------------Patient Info---------------------//
        /*

        FirstName = json[i]['FirstName'] ? json[i]['FirstName'] : '';
        MiddleName = json[i]['MiddleName'] ? json[i]['MiddleName'] : '';
        LastName = json[i]['LastName'] ? json[i]['LastName'] : '';
        SSN = json[i]['SSN'] ? json[i]['SSN'] : '';
        HomePhone = json[i]['HomePhone'] ? json[i]['HomePhone'] : '';
        WorkPhone = json[i]['WorkPhone'] ? json[i]['WorkPhone'] : '';
        CellPhone = json[i]['CellPhone'] ? json[i]['CellPhone'] : '';
        Email = json[i]['Email'] ? json[i]['Email'] : '';
        Address = json[i]['Address'] ? json[i]['Address'] : '';
        Suite = json[i]['Suite'] ? json[i]['Suite'] : '';
        City = json[i]['City'] ? json[i]['City'] : '';
        State = json[i]['State'] ? json[i]['State'] : '';
        Zip = json[i]['Zip'] ? json[i]['Zip'] : '';
        //---------------Case Info data--------------------------------//
        PracticeLocation = json[i]['PracticeLocation'] ? json[i]['PracticeLocation'] : '';
        Category = json[i]['Category'] ? json[i]['Category'] : '';
        PurposeOfVisit = json[i]['PurposeOfVisit'] ? json[i]['PurposeOfVisit'] : '';
        CaseType = json[i]['CaseType'] ? json[i]['CaseType'] : '';
        DOA = json[i]['DOA'] ? json[i]['DOA'] : '';
        //--------------Personal Data-----------------------------------//
        PersonalSSN = json[i]['PersonalSSN'] ? json[i]['PersonalSSN'] : '';
        Weight = json[i]['Weight'] ? json[i]['Weight'] : '';
        Height = json[i]['Height'] ? json[i]['Height'] : '';
        Inches = json[i]['Inches'] ? json[i]['Inches'] : '';
        //-------------Basic Contact Information  Data-----------------------//
        BHomePhone = json[i]['BHomePhone'] ? json[i]['BHomePhone'] : '';
        BCellPhone = json[i]['BCellPhone'] ? json[i]['BCellPhone'] : '';
        BWorkPhone = json[i]['BWorkPhone'] ? json[i]['BWorkPhone'] : '';
        BExtension = json[i]['BExtension'] ? json[i]['BExtension'] : '';
        BStreetAddress = json[i]['BStreetAddress'] ? json[i]['BStreetAddress'] : '';
        BSuite = json[i]['BSuite'] ? json[i]['BSuite'] : '';
        BCity = json[i]['BCity'] ? json[i]['BCity'] : '';
        BState = json[i]['BState'] ? json[i]['BState'] : '';
        BZip = json[i]['BZip'] ? json[i]['BZip'] : '';
        BEmail = json[i]['BEmail'] ? json[i]['BEmail'] : '';
        BFax = json[i]['BFax'] ? json[i]['BFax'] : '';
        //-------------------------------Form Filler Information Data------------------------------------------//
        FFirstName = json[i]['FFirstName'] ? json[i]['FFirstName'] : '';
        FMiddleName = json[i]['FMiddleName'] ? json[i]['FMiddleName'] : '';
        FLastName = json[i]['FLastName'] ? json[i]['FLastName'] : '';
        FCellPhone = json[i]['FCellPhone'] ? json[i]['FCellPhone'] : '';
        FEmail = json[i]['FEmail'] ? json[i]['FEmail'] : '';
        FFax = json[i]['FFax'] ? json[i]['FFax'] : '';
        FStreetAddress = json[i]['FStreetAddress'] ? json[i]['FStreetAddress'] : '';
        FSuite = json[i]['FSuite'] ? json[i]['FSuite'] : '';
        FCity = json[i]['FCity'] ? json[i]['FCity'] : '';
        FState = json[i]['FState'] ? json[i]['FState'] : '';
        FZip = json[i]['FZip'] ? json[i]['FZip'] : '';
        FRelation = json[i]['FRelation'] ? json[i]['FRelation'] : '';

        //---------------------------------Emergency Conatact Person Data------------------------------------------------//
        EFirstName = json[i]['EFirstName'] ? json[i]['EFirstName'] : '';
        EMiddleName = json[i]['EMiddleName'] ? json[i]['EMiddleName'] : '';
        ELastName = json[i]['ELastName'] ? json[i]['ELastName'] : '';
        EDOB = json[i]['EDOB'] ? json[i]['EDOB'] : '';
        EHomePhone = json[i]['EHomePhone'] ? json[i]['EHomePhone'] : '';
        ECellPhone = json[i]['ECellPhone'] ? json[i]['ECellPhone'] : '';
        EEmail = json[i]['EEmail'] ? json[i]['EEmail'] : '';
        EFax = json[i]['EFax'] ? json[i]['EFax'] : '';
        EStreetAddress = json[i]['EStreetAddress'] ? json[i]['EStreetAddress'] : '';
        ESuite = json[i]['ESuite'] ? json[i]['ESuite'] : '';
        ECity = json[i]['ECity'] ? json[i]['ECity'] : '';
        EState = json[i]['EState'] ? json[i]['EState'] : '';
        EZip = json[i]['EZip'] ? json[i]['EZip'] : '';
        ERelation = json[i]['ERelation'] ? json[i]['ERelation'] : '';
        //------------------------------------Reffrals Data----------------------------------------//
        RPFirstName = json[i]['RPFirstName'] ? json[i]['RPFirstName'] : '';
        RPMiddleName = json[i]['RPMiddleName'] ? json[i]['RPMiddleName'] : '';
        RPLastName = json[i]['RPLastName'] ? json[i]['RPLastName'] : '';
        RPClinicName = json[i]['RPClinicName'] ? json[i]['RPClinicName'] : '';
        RPEmail = json[i]['RPEmail'] ? json[i]['RPEmail'] : '';
        RPhoneNo = json[i]['RPhoneNo'] ? json[i]['RPhoneNo'] : '';
        RPExtension = json[i]['RPExtension'] ? json[i]['RPExtension'] : '';
        RPAddress = json[i]['RPAddress'] ? json[i]['RPAddress'] : '';
        RPSuite = json[i]['RPSuite'] ? json[i]['RPSuite'] : '';
        RPCity = json[i]['RPCity'] ? json[i]['RPCity'] : '';
        RPState = json[i]['RPState'] ? json[i]['RPState'] : '';
        RPZip = json[i]['RPZip'] ? json[i]['RPZip'] : '';
        PPFirstName = json[i]['PPFirstName'] ? json[i]['PPFirstName'] : '';
        PPMiddleName = json[i]['PPMiddleName'] ? json[i]['PPMiddleName'] : '';
        PPLastName = json[i]['PPLastName'] ? json[i]['PPLastName'] : '';
        PPClinicName = json[i]['PPClinicName'] ? json[i]['PPClinicName'] : '';
        PPEmail = json[i]['PPEmail'] ? json[i]['PPEmail'] : '';
        PPPhoneNo = json[i]['PPPhoneNo'] ? json[i]['PPPhoneNo'] : '';
        PPExtension = json[i]['PPExtension'] ? json[i]['PPExtension'] : '';
        PPAddress = json[i]['PPAddress'] ? json[i]['PPAddress'] : '';
        PPSuite = json[i]['PPSuite'] ? json[i]['PPSuite'] : '';
        PPCity = json[i]['PPCity'] ? json[i]['PPCity'] : '';
        PPState = json[i]['PPState'] ? json[i]['PPState'] : '';
        PPZip = json[i]['PPZip'] ? json[i]['PPZip'] : '';
        //----------------------------------Insurance Data--------------------------------------------//
        PInsuranceName = json[i]['PInsuranceName'] ? json[i]['PInsuranceName'] : '';
        PMemberID = json[i]['PMemberID'] ? json[i]['PMemberID'] : '';
        PGroupNo = json[i]['PGroupNo'] ? json[i]['PGroupNo'] : '';
        PPriorAuthNo = json[i]['PPriorAuthNo'] ? json[i]['PPriorAuthNo'] : '';
        NFInsuranceName = json[i]['NFInsuranceName'] ? json[i]['NFInsuranceName'] : '';
        NFPHFirstName = json[i]['NFPHFirstName'] ? json[i]['NFPHFirstName'] : '';
        NFPHMiddleName = json[i]['NFPHMiddleName'] ? json[i]['NFPHMiddleName'] : '';
        NFPHLastName = json[i]['NFPHLastName'] ? json[i]['NFPHLastName'] : '';
        NFPHClaimNo = json[i]['NFPHClaimNo'] ? json[i]['NFPHClaimNo'] : '';
        NFPHPolicyNo = json[i]['NFPHPolicyNo'] ? json[i]['NFPHPolicyNo'] : '';
        NFPHWcbNo = json[i]['NFPHWcbNo'] ? json[i]['NFPHWcbNo'] : '';
        NFPHPriorAuth = json[i]['NFPHPriorAuth'] ? json[i]['NFPHPriorAuth'] : '';
        SInsuranceName = json[i]['SInsuranceName'] ? json[i]['SInsuranceName'] : '';
        SPHFirstName = json[i]['SPHFirstName'] ? json[i]['SPHFirstName'] : '';
        SPHMiddleName = json[i]['SPHMiddleName'] ? json[i]['SPHMiddleName'] : '';
        SPHLastName = json[i]['SPHLastName'] ? json[i]['SPHLastName'] : '';
        SPHClaimNo = json[i]['SPHClaimNo'] ? json[i]['SPHClaimNo'] : '';
        SPHPolicyNo = json[i]['SPHPolicyNo'] ? json[i]['SPHPolicyNo'] : '';
        SPHWcbNo = json[i]['SPHWcbNo'] ? json[i]['SPHWcbNo'] : '';
        SPHPriorAuth = json[i]['SPHPriorAuth'] ? json[i]['SPHPriorAuth'] : '';
        TInsuranceName = json[i]['TInsuranceName'] ? json[i]['TInsuranceName'] : '';
        TPHFirstName = json[i]['TPHFirstName'] ? json[i]['TPHFirstName'] : '';
        TPHMiddleName = json[i]['TPHMiddleName'] ? json[i]['TPHMiddleName'] : '';
        TPHLastName = json[i]['TPHLastName'] ? json[i]['TPHLastName'] : '';
        TPHClaimNo = json[i]['TPHClaimNo'] ? json[i]['TPHClaimNo'] : '';
        TPHPolicyNo = json[i]['TPHPolicyNo'] ? json[i]['TPHPolicyNo'] : '';
        TPHWcbNo = json[i]['TPHWcbNo'] ? json[i]['TPHWcbNo'] : '';
        TPHPriorAuth = json[i]['TPHPriorAuth'] ? json[i]['TPHPriorAuth'] : '';
        //-------------------------------Attorney Data---------------------------------------//
        FirmName = json[i]['FirmName'] ? json[i]['FirmName'] : '';
        AttorneyName = json[i]['AttorneyName'] ? json[i]['AttorneyName'] : '';
        //-----------------------------------Employer Test Data--------------------------------//
        PEmployerName = json[i]['PEmployerName'] ? json[i]['PEmployerName'] : '';
        PPatientOccupation = json[i]['PPatientOccupation'] ? json[i]['PPatientOccupation'] : '';
        PFirstName = json[i]['PFirstName'] ? json[i]['PFirstName'] : '';
        PMiddleName = json[i]['PMiddleName'] ? json[i]['PMiddleName'] : '';
        PLastName = json[i]['PLastName'] ? json[i]['PLastName'] : '';
        SEmployerName = json[i]['SEmployerName'] ? json[i]['SEmployerName'] : '';
        SPatientOccupation = json[i]['SPatientOccupation'] ? json[i]['SPatientOccupation'] : '';
        SHiringDate = json[i]['SHiringDate'] ? json[i]['SHiringDate'] : '';
        YearlyEmployerName = json[i]['YearlyEmployerName'] ? json[i]['YearlyEmployerName'] : '';
        YearlyEmployerOccupation = json[i]['YearlyEmployerOccupation'] ? json[i]['YearlyEmployerOccupation'] : '';
        YearlyHiringDate = json[i]['YearlyHiringDate'] ? json[i]['YearlyHiringDate'] : '';
        JobTitle = json[i]['JobTitle'] ? json[i]['JobTitle'] : '';
        TypeOfActivity = json[i]['TypeOfActivity'] ? json[i]['TypeOfActivity'] : '';
        GrossPay = json[i]['GrossPay'] ? json[i]['GrossPay'] : '';
        WeeklyPay = json[i]['WeeklyPay'] ? json[i]['WeeklyPay'] : '';
        Days = json[i]['Days'] ? json[i]['Days'] : '';
        Hours = json[i]['Hours'] ? json[i]['Hours'] : '';
        WhatDate = json[i]['WhatDate'] ? json[i]['WhatDate'] : '';
        ReturnWork = json[i]['ReturnWork'] ? json[i]['ReturnWork'] : '';
        NFirstName = json[i]['NFirstName'] ? json[i]['NFirstName'] : '';
        NMiddleName = json[i]['NMiddleName'] ? json[i]['NMiddleName'] : '';
        NLastName = json[i]['NLastName'] ? json[i]['NLastName'] : '';
        NoticeDate = json[i]['NoticeDate'] ? json[i]['NoticeDate'] : '';
        //--------------------------------------Accident Data-------------------------------------------------------------//
        InjuryHappened = json[i]['InjuryHappened'] ? json[i]['InjuryHappened'] : '';
        AStreetAddress = json[i]['AStreetAddress'] ? json[i]['AStreetAddress'] : '';
        ASuite = json[i]['ASuite'] ? json[i]['ASuite'] : '';
        ACity = json[i]['ACity'] ? json[i]['ACity'] : '';
        AState = json[i]['AState'] ? json[i]['AState'] : '';
        AZip = json[i]['AZip'] ? json[i]['AZip'] : '';
        BecameIll = json[i]['BecameIll'] ? json[i]['BecameIll'] : '';
        IllnessHappened = json[i]['IllnessHappened'] ? json[i]['IllnessHappened'] : '';
        DescribeInjury = json[i]['DescribeInjury'] ? json[i]['DescribeInjury'] : '';
        NatureInjury = json[i]['NatureInjury'] ? json[i]['NatureInjury'] : '';
        //--------------------------------------------Vehicle Data-----------------------------------------------------//
        ReportDate = json[i]['ReportDate'] ? json[i]['ReportDate'] : '';
        Precincit = json[i]['Precincit'] ? json[i]['Precincit'] : '';
        VState = json[i]['VState'] ? json[i]['VState'] : '';
        VCity = json[i]['VCity'] ? json[i]['VCity'] : '';
        VCounty = json[i]['VCounty'] ? json[i]['VCounty'] : '';
        ManyVehicles = json[i]['ManyVehicles'] ? json[i]['ManyVehicles'] : '';
        VYear = json[i]['VYear'] ? json[i]['VYear'] : '';
        VMake = json[i]['VMake'] ? json[i]['VMake'] : '';
        VModel = json[i]['VModel'] ? json[i]['VModel'] : '';
        VColor = json[i]['VColor'] ? json[i]['VColor'] : '';
        VLisenceNo = json[i]['VLisenceNo'] ? json[i]['VLisenceNo'] : '';
        VehState = json[i]['VehState'] ? json[i]['VehState'] : '';
        VInsuranceName = json[i]['VInsuranceName'] ? json[i]['VInsuranceName'] : '';
        VPolicyNo = json[i]['VPolicyNo'] ? json[i]['VPolicyNo'] : '';
        DFirstName = json[i]['DFirstName'] ? json[i]['DFirstName'] : '';
        DMiddleName = json[i]['DMiddleName'] ? json[i]['DMiddleName'] : '';
        DLastName = json[i]['DLastName'] ? json[i]['DLastName'] : '';
        DAddress = json[i]['DAddress'] ? json[i]['DAddress'] : '';
        DSuite = json[i]['DSuite'] ? json[i]['DSuite'] : '';
        DCity = json[i]['DCity'] ? json[i]['DCity'] : '';
        DState = json[i]['DState'] ? json[i]['DState'] : '';
        DZip = json[i]['DZip'] ? json[i]['DZip'] : '';
        OFirstName = json[i]['OFirstName'] ? json[i]['OFirstName'] : '';
        OMiddleName = json[i]['OMiddleName'] ? json[i]['OMiddleName'] : '';
        OLastName = json[i]['OLastName'] ? json[i]['OLastName'] : '';
        OAddress = json[i]['OAddress'] ? json[i]['OAddress'] : '';
        OSuite = json[i]['OSuite'] ? json[i]['OSuite'] : '';
        OCity = json[i]['OCity'] ? json[i]['OCity'] : '';
        OState = json[i]['OState'] ? json[i]['OState'] : '';
        OZip = json[i]['OZip'] ? json[i]['OZip'] : '';
        //-------------------------------------HouseHold Information Data-------------------------------------------------//
        HHFirstName = json[i]['HHFirstName'] ? json[i]['HHFirstName'] : '';
        HHMiddleName = json[i]['HHMiddleName'] ? json[i]['HHMiddleName'] : '';
        HHLastName = json[i]['HHLastName'] ? json[i]['HHLastName'] : '';
        HHSSN = json[i]['HHSSN'] ? json[i]['HHSSN'] : '';
        HHDOB = json[i]['HHDOB'] ? json[i]['HHDOB'] : '';
        HHCellNumber = json[i]['HHCellNumber'] ? json[i]['HHCellNumber'] : '';
        HHRelation = json[i]['HHRelation'] ? json[i]['HHRelation'] : '';
        HHInsuranceName = json[i]['HHInsuranceName'] ? json[i]['HHInsuranceName'] : '';
        HHPolicyNo = json[i]['HHPolicyNo'] ? json[i]['HHPolicyNo'] : '';
        HHEffectiveDate = json[i]['HHEffectiveDate'] ? json[i]['HHEffectiveDate'] : '';
        HHExpirationDate = json[i]['HHExpirationDate'] ? json[i]['HHExpirationDate'] : '';
        //-----------------------------------Medical Treatment Data--------------------------------------------------//
        MFirstTreatmentDate = json[i]['MFirstTreatmentDate'] ? json[i]['MFirstTreatmentDate'] : '';
        MDateOfTreatment = json[i]['MDateOfTreatment'] ? json[i]['MDateOfTreatment'] : '';
        MedicalHospitalName = json[i]['MedicalHospitalName'] ? json[i]['MedicalHospitalName'] : '';
        DocFirstName = json[i]['DocFirstName'] ? json[i]['DocFirstName'] : '';
        DocMiddleName = json[i]['DocMiddleName'] ? json[i]['DocMiddleName'] : '';
        DocLastName = json[i]['DocLastName'] ? json[i]['DocLastName'] : '';
        DocAddress = json[i]['DocAddress'] ? json[i]['DocAddress'] : '';
        DocSuite = json[i]['DocSuite'] ? json[i]['DocSuite'] : '';
        DocCity = json[i]['DocCity'] ? json[i]['DocCity'] : '';
        DocState = json[i]['DocState'] ? json[i]['DocState'] : '';
        DocZip = json[i]['DocZip'] ? json[i]['DocZip'] : '';
        Amount = json[i]['Amount'] ? json[i]['Amount'] : '';
        */


        // ------------------------------ADD PATIENT------------------------------//
        it('Add Patient', async () => {
            await browser.pause(5000);
            await patient.AddNew.click();
            await patient.AddPatient(json[i]['FirstName'], json[i]['MiddleName'], json[i]['LastName'], "10031985", json[i]['SSN'], json[i]['HomePhone'], json[i]['WorkPhone'],json[i]['CellPhone'], json[i]['Email'], json[i]['Address'], json[i]['Suite'], json[i]['City']);
            const selectState = await $("//select[@container='body']");
            await selectState.selectByVisibleText(json[i]['State']);
            await patient.Zip.setValue(json[i]['Zip']);
            await patient.SaveAndContinue.click();
            await browser.pause(3000);
            await patient.IamNewPatient.click();
            await browser.pause(9000);
            await $("//a[.='Add Case ']").click();

        });


        //------------------------CASE INFO----------------------------------------//

        it('Case Info Page', async () => {
            //--------select Practice Location-------------//


            await $('//label[.="Practice*"]/following::ng-select').click();
            await browser.pause(8000);
            let list1 = await $$("//div[@role='option']")
            for (let i = 0; i < list1.length; i++) {
                const element = await list1[i];
                if (await element.getText() === json[i]['PracticeLocation']) {
                    await element.click();
                    break;
                }

            }
            await $("//label[.='Practice*']").click();
            //-----------------select category---------------------------------//
            const selectCategory = await $("//select[@tooltipclass='table-custom-tooltip']");
            await selectCategory.selectByVisibleText(json[i]['Category']);
            await browser.pause(6000);
            //---------------Select Purpose of Visit-------------------------//
            const selectPurposeOfVisit = await $("(//select[@container='body'])[2]");
            await selectPurposeOfVisit.selectByVisibleText(json[i]['PurposeOfVisit']);
            await browser.pause(6000);

            //---------------------select Case Type-------------------------//
            const selectCaseType = await $("(//select[@container='body'])[3]");
            await selectCaseType.selectByVisibleText(json[i]['CaseType']);
            await browser.pause(4000);
            await $("//label[.='DOA* (mm/dd/yyyy)']/following::input").setValue(json[i]['DOA']);
            await $("//button[.='Save & Continue']").click();
            await browser.pause(4000);
            //await $("//button[.='Create a New Case']").click();
        });

        //------------------------------ Personal Information Page--------------------------------------//

        it('Personal Information Page', async () => {
            await browser.pause(5000);
            await personal.AddPersonalInfo(json[i]['PersonalSSN'], json[i]['Weight'], json[i]['Height'], json[i]['Inches']);
            /*
           await $('//label[.="If yes, What Language?"]/following::ng-select').click();
           await browser.pause(6000);
           let list2 = await $$("//div[@role='option']")
           for (let i = 0; i < list2.length; i++) {
               const element = await list2[i];
               if (await element.getText() === 'Afar') {
                   await element.click();
                   break;
               }
        
           }
           */

            await browser.pause(3000);
            await personal.SaveAndContinue.click();


        });

        //----------------------------- Basic Information Page---------------------------------------------------//
        it('Basic Information Page', async () => {

            await BasicInfo.AddBasicContactInformation(json[i]['BHomePhone'], json[i]['BCellPhone'], json[i]['BWorkPhone'], json[i]['BExtension'], json[i]['BStreetAddress'], json[i]['BSuite'], json[i]['BCity'], json[i]['BState'], json[i]['BZip'], json[i]['BEmail'], json[i]['BFax']);



        });

        //---------------------------------Form Filler Page------------------------------------------------------//
        it('Form Filler Page', async () => {
            await FormFiller.AddFormFillerInfo(json[i]['FFirstName'], json[i]['FMiddleName'], json[i]['FLastName'], json[i]['FCellPhone'], json[i]['FEmail'], json[i]['FFax'], json[i]['FStreetAddress'], json[i]['FSuite'], json[i]['FCity'], json[i]['FState'], json[i]['FZip']);
            const selectRelation = await $("//select[@container='body']");
            await selectRelation.selectByVisibleText(json[i]['FRelation']);
            await FormFiller.EmergencyContactPerson.click();
            await FormFiller.SaveAndContinue.click();
            await browser.pause(6000);


        });


        //-----------------------------Emergency Contact Person Information----------------------------------------------------------//

        it('Emergency Contact Information', async () => {
            await Emergency.AddEmergencyContactInformation(json[i]['EFirstName'], json[i]['EMiddleName'], json[i]['ELastName'], json[i]['EDOB'], json[i]['EHomePhone'], json[i]['ECellPhone'], json[i]['EEmail'], json[i]['EFax'], json[i]['EStreetAddress'], json[i]['ESuite'], json[i]['ECity'], json[i]['EState'], json[i]['EZip']);
            const selectRel = await $("//select[@container='body']");
            await selectRel.selectByVisibleText(json[i]['ERelation']);
            await Emergency.SaveAndContinue.click();
            await browser.pause(6000);

        });
        //----------------------------Referrals Page---------------------------------------------------------------//

        it('Referrals Page', async () => {
            await Referral.AddRefferedPhysician(json[i]['RPFirstName'], json[i]['RPMiddleName'], json[i]['RPLastName'], json[i]['RPClinicName'], json[i]['RPEmail'], json[i]['RPhoneNo'], json[i]['RPExtension'], json[i]['RPAddress'],json[i]['RPSuite'], json[i]['RPCity']);
            const selectState = await $("//select[@container='body']");
            await selectState.selectByVisibleText(json[i]['RPState']);
            await Referral.Zip.setValue(json[i]['RPZip']);
            await Referral.PrimaryPhysician.click();
            await Referral.AddPrimaryPhysician(json[i]['PPFirstName'], json[i]['PPMiddleName'], json[i]['PPLastName'], json[i]['PPClinicName'], json[i]['PPEmail'], json[i]['PPPhoneNo'], json[i]['PPExtension'], json[i]['PPAddress'], json[i]['PPSuite'], json[i]['PPCity']);
            const PrimaryselectState = await $("(//select[@container='body'])[2]");
            await PrimaryselectState.selectByVisibleText(json[i]['PPState']);
            await Referral.PZip.setValue(json[i]['PPZip']);
            await Referral.SaveAndContinue.click();
            await browser.pause(6000);

        });



        // ------------------------------------Select Insurance-----------------------------------------------------------//

        it('Insurance Page', async () => {

            //-----------------------Private Health Insurance-----------------------------------------------------------//

            await Insurance.AddPrivateHealthInsurance();
            
            
            await $("//label[.='Insurance Name']/following::ng-select").click();
           
            await browser.pause(4000);
            let PrivateInsuranceName = await $$("//div[@role='option']")
            for (let i = 0; i < PrivateInsuranceName.length; i++) {
                const element = await PrivateInsuranceName[i];
                if (await element.getText() === json[i]['PInsuranceName']) {
                    await element.click();
                    break;
                }

            }
            await browser.pause(4000);
            await $("//label[.='Location Name*']/following::ng-select").click();
            await browser.pause(4000);
            let PrivateLocationName = await $$("//div[@role='option']")
            for (let i = 0; i < PrivateLocationName.length; i++) {
                const element = await PrivateLocationName[i];
                if (await element.getText() === 'new') {
                    await element.click();
                    break;
                }

            }

            await Insurance.AddPrivateHealthInsuranceData(json[i]['PMemberID'], json[i]['PGroupNo'], json[i]['PPriorAuthNo']);
            //----------------------------------------------------No Fault------------------------------------------------------//
           // await $("//h6[.=' No-Fault']").scrollIntoView();
            await browser.pause(3000);
            await Insurance.NoFaultAccident.click();
            await $("(//span[@class='ng-arrow-wrapper'])[3]").click();
            await browser.pause(4000);

            let NoFaultInsuranceName = await $$("//div[@role='option']")
            for (let i = 0; i < NoFaultInsuranceName.length; i++) {
                const element = await NoFaultInsuranceName[i];
                if (await element.getText() === json[i]['NFInsuranceName']) {
                    await element.click();
                    break;
                }

            }
            await browser.pause(2000);
            
            await $("(//span[@class='ng-arrow-wrapper'])[4]").click();
            await browser.pause(4000);

            let NoFaultInsuranceLocation = await $$("//div[@role='option']")
            for (let i = 0; i < NoFaultInsuranceLocation.length; i++) {
                const element = await NoFaultInsuranceLocation[i];
                if (await element.getText() === 'new') {
                    await element.click();
                    break;
                }

            }
            //await $("(//span[@class='ng-arrow-wrapper'])[5]").click();
            await Insurance.AddNoFaultPolicyHolder(json[i]['NFPHFirstName'],json[i]['NFPHMiddleName'], json[i]['NFPHLastName'], json[i]['NFPHClaimNo'], json[i]['NFPHPolicyNo'], json[i]['NFPHWcbNo'], json[i]['NFPHPriorAuth']);
            //--------------------------------------Select No Fault Adjuster--------------------------------------------//
            await $("(//span[@class='ng-arrow-wrapper'])[6]").click();
            await browser.pause(5000);

            let NoFaultAdjuster = await $$("//div[@role='option']")
            

            for (let i = 0; i < NoFaultAdjuster.length; i++) {
                const element = await NoFaultAdjuster[i];
                if (await element.getText() === 'Rajendra  Vakil') {
                    await element.click();
                    break;
                }

            }
            //-----------------------------------------Secondry Insurance--------------------------------------------------------//
            await $("//h6[.=' Secondary Insurance Information']").scrollIntoView();
            await $("//span[.='Do you have secondary insurance?*']/following::span").click();
            await $("(//span[@class='ng-arrow-wrapper'])[8]").click();
            await browser.pause(6000);
            let SecondryInsuranceName = await $$("//div[@role='option']")
            for (let i = 0; i < SecondryInsuranceName.length; i++) {
                const element = await SecondryInsuranceName[i];
                if (await element.getText() === json[i]['SInsuranceName']) {
                    await element.click();
                    break;
                }

            }
            await browser.pause(3000);
            await $("(//span[@class='ng-arrow-wrapper'])[9]").click();
            await browser.pause(4000);
            let SecondryLocationName = await $$("//div[@role='option']")
            for (let i = 0; i < SecondryLocationName.length; i++) {
                const element = await SecondryLocationName[i];
                if (await element.getText() === 'new') {
                    await element.click();
                    break;
                }

            }
            await $("(//label[.='First Name'])[2]").scrollIntoView();
            await Insurance.AddSecondryPolicyHolderInformation(json[i]['SPHFirstName'], json[i]['SPHMiddleName'], json[i]['SPHLastName'], json[i]['SPHClaimNo'], json[i]['SPHPolicyNo'], json[i]['SPHWcbNo'], json[i]['SPHPriorAuth']);
            //-----------------------------------------Select Secondry Adjuster--------------------------------------------------------//
            await $("(//span[@class='ng-arrow-wrapper'])[11]").click();
            await browser.pause(5000);

            let SecondryAdjuster = await $$("//div[@role='option']")

            for (let i = 0; i < SecondryAdjuster.length; i++) {
                const element = await SecondryAdjuster[i];
                if (await element.getText() === 'Rajendra  Vakil') {
                    await element.click();
                    break;
                }

            }

            //----------------------------------------Tertiary Insurance Information---------------------------------------------------//

            await Insurance.TertiaryInsurance.click();
            await $("(//span[@class='ng-arrow-wrapper'])[12]").click();
            await browser.pause(6000);
            let TertiaryInsuranceName = await $$("//div[@role='option']")
            for (let i = 0; i < TertiaryInsuranceName.length; i++) {
                const element = await TertiaryInsuranceName[i];
                if (await element.getText() === json[i]['TInsuranceName']) {
                    await element.click();
                    break;
                }

            }
            await browser.pause(3000);
            await $("(//span[@class='ng-arrow-wrapper'])[13]").click();
            await browser.pause(4000);
            let TertiaryLocationName = await $$("//div[@role='option']")
            for (let i = 0; i < TertiaryLocationName.length; i++) {
                const element = await TertiaryLocationName[i];
                if (await element.getText() === 'new') {
                    await element.click();
                    break;
                }

            }
            await browser.pause(6000);
            await $('(//label[.="First Name"])[3]').scrollIntoView();
            await Insurance.AddTertiaryPolicyHolderInformation(json[i]['TPHFirstName'], json[i]['TPHMiddleName'], json[i]['TPHLastName'], json[i]['TPHClaimNo'], json[i]['TPHPolicyNo'], json[i]['TPHWcbNo'], json[i]['TPHPriorAuth']);

            //-----------------------------------------Select Tertiary Adjuster-------------------------------------------//
            await $("(//span[@class='ng-arrow-wrapper'])[15]").click();
            await browser.pause(5000);

            let TertiaryAdjuster = await $$("//div[@role='option']")

            for (let i = 0; i < TertiaryAdjuster.length; i++) {
                const element = await TertiaryAdjuster[i];
                if (await element.getText() === 'Rajendra  Vakil') {
                    await element.click();
                    break;
                }

            }

            await Insurance.SaveAndContinue.click();




        });
        //-----------------------------------------------Attorney Page--------------------------------------------------//

        it('Attorney Page', async () => {
            //---------------------Select Firm Information----------------------------------------//
            await browser.pause(4000);
            await $("//ng-select[@role='listbox']").click();
            await browser.pause(7000);
            let Firm = await $$("//div[@role='option']")
            for (let i = 0; i < Firm.length; i++) {
                const element = await Firm[i];
                if (await element.getText() === json[i]['FirmName']) {
                    await element.click();
                    break;
                }

            }
            await browser.pause(5000);

            //-------------------------------Select Attorney Information----------------------------------//
            await $("(//ng-select[@role='listbox'])[3]").click();
            await browser.pause(7000);
            let Attorney = await $$("//div[@role='option']")
            for (let i = 0; i < Attorney.length; i++) {
                const element = await Attorney[i];
                if (await element.getText() === json[i]['AttorneyName']) {
                    await element.click();
                    break;
                }

            }
            await $("//button[.='Save & Continue']").click();
            await browser.pause(5000);
            // await $("//a[.='Accident']").click();
        });



        //---------------------------------------------------Employer Information---------------------------------------------------------//

        it('Employer Page', async () => {
            await Employer.CourseofEmployment.click();

            //---------------------------Primary Employer Information-----------------------------------------------//

            await $("(//ng-select[@role='listbox'])[2]").click();
            await browser.pause(5000);


            let PrimaryEmployer = await $$("//div[@role='option']")


            for (let i = 0; i < PrimaryEmployer.length; i++) {

                const element = await PrimaryEmployer[i];
                if (await element.getText() === 'Aamir 100') {

                    await element.click();
                    break;
                }

            }

            await Employer.PatientOccupation.setValue(json[i]['PPatientOccupation']);
            await Employer.HiringDate.setValue(json[i]['PHiringDate']);
            await Employer.LooseEmployment.click();
            await Employer.FirstName.setValue(json[i]['PFirstName']);
            await Employer.MiddleName.setValue(json[i]['PMiddleName']);
            await Employer.LastName.setValue(json[i]['PLastName']);
            await Employer.SaveAndContinue.click();
            await browser.pause(10000);


            //-------------------------------------------Secondry Employer----------------------------------------------------------//

            await Employer.SecondryEmployer.click();

            await $("(//span[@class='ng-arrow-wrapper'])[2]").click();
            await browser.pause(5000);


            let SecondryEmployer = await $$("//div[@role='option']")

            for (let i = 0; i < SecondryEmployer.length; i++) {

                const element = await SecondryEmployer[i];
                if (await element.getText() === 'Aamir 100') {
                    await element.click();
                    break;
                }

            }
            await Employer.SecondryPatientOccupation.setValue(json[i]['SPatientOccupation']);
            await Employer.SecondryHiringDate.setValue(json[i]['SHiringDate']);

            await Employer.SecondrySaveAndContinue.click();
            await browser.pause(5000);
            //--------------------------------Yearly Employer-----------------------------------------------------------------//

            await Employer.YearlyEmployer.click();
            await $("(//span[@class='ng-arrow-wrapper'])[2]").click();
            await browser.pause(5000);


            let YearlyEmployer = await $$("//div[@role='option']")

            for (let i = 0; i < YearlyEmployer.length; i++) {

                const element = await YearlyEmployer[i];
                if (await element.getText() === 'Aamir 100') {
                    await element.click();
                    break;
                }

            }
            await Employer.YearlyPatientOccupation.setValue(json[i]['YearlyEmployerOccupation']);
            await Employer.YearlyHiringDate.setValue(json[i]['YearlyHiringDate']);
            await Employer.YearlyEndDate.setValue(json[i]['YearlyEndDate']);
            await Employer.SaveAndContinue.click();
            //-----------------------------Employment Information----------------------------------------------------------//
            await browser.pause(4000);
            await Employer.JobDescription.setValue(json[i]['JobTitle']);
            await Employer.TypeOfActivities.setValue(json[i]['TypeOfActivity']);
            await Employer.YourJob.click();
            const PaidType = await $("//select[@container='body']");
            await PaidType.selectByVisibleText('Hourly');
            await Employer.GrossPay.setValue(json[i]['GrossPay']);
            await Employer.WeeklyEarning.setValue(json[i]['WeeklyPay']);
            await Employer.WeekDays.setValue(json[i]['Days']);
            await Employer.HoursDay.setValue(json[i]['Hours']);
            await Employer.LoadingTips.click();
            //---------------------------------------Return to Work Information-----------------------------------------------------//

            await Employer.StopWork.click();
            await Employer.onWhatDate.setValue(json[i]['WhatDate']);
            await Employer.ReturnOfWork.click();
            await Employer.DateOfReturnOfWork.setValue(json[i]['ReturnWork']);
            await Employer.SameEmployer.click();
            await Employer.TypeOfAssignment.click();
            await Employer.NoticeOfInjury.click();
            //---------------------------------------Notice Given To-----------------------------------------------------------//

            await Employer.NoticeFirstName.setValue(json[i]['NFirstName']);
            await Employer.NoticeMiddleName.setValue(json[i]['NMiddleName']);
            await Employer.NoticeLastName.setValue(json[i]['NLastName']);
            await Employer.NoticeGiven.click();
            await Employer.DateOfNotice.setValue(json[i]['NoticeDate']);
            await Employer.EmployerSaveAndContinue.click();
            await browser.pause(5000);

        });


        //--------------------------------------------Accident Page------------------------------------------//



        it('Accident Page', async () => {
            await browser.pause(4000);

            await Accident.AddAccidentInfo(json[i]['InjuryHappened'], json[i]['AStreetAddress'], json[i]['ASuite'], json[i]['ACity'], json[i]['AState'], json[i]['AZip'], json[i]['BecameIll'], json[i]['IllnessHappened'], json[i]['DescribeInjury'], json[i]['NatureInjury']);
            await browser.pause(3000);
            // await Accident.AddWitness("Yasir","Munir","Munir","Phase 8","3rd","New York","New York","54000","3211468377");
            await $("//h6[.='Witness to the Accident ']").scrollIntoView();
            await Accident.WitnessOfAccident.click();

            await $("//button[.=' Save & Continue']").click();
            await browser.pause(5000);

        });

        //-------------------------------------------Vehicle Page----------------------------------------------------------//


        it('Vehicle Page', async () => {
            await Vehicle.AccidentReported.click();
            await Vehicle.ReportingDate.setValue(json[i]['ReportDate']);
            await Vehicle.Precinct.setValue(json[i]['Precincit']);
            await $("//label[.='State *']/following::ng-select").click();
            let VehicleState = await $$("//div[@role='option']")
            for (let i = 0; i < VehicleState.length; i++) {
                const element = await VehicleState[i];
                if (await element.getText() === json[i]['VState']) {
                    await element.click();
                    break;
                }

            }
            await $("//label[.='City *']/following::ng-select").click();
            await browser.pause(5000);
            let VehicleCity = await $$("//div[@role='option']")

            for (let i = 0; i < VehicleCity.length; i++) {
                const element = await VehicleCity[i];
                if (await element.getText() === json[i]['VCity']) {
                    await element.click();
                    break;
                }

            }

            await $("//label[.='County']/following::ng-select").click();
            await browser.pause(5000);
            let VehicleCounty = await $$("//div[@role='option']");

            for (let i = 0; i < VehicleCounty.length; i++) {
                const element = await VehicleCounty[i];
                if (await element.getText() === json[i]['VCounty']) {
                    await element.click();
                    break;
                }

            }
            await Vehicle.VehicleWas.click();
            await Vehicle.CarWas.click();
            await Vehicle.HowManyVehicles.setValue(json[i]['ManyVehicles']);
            await Vehicle.OwnerOfVehicle.click();
            await browser.pause(3000);
            const selectVehicle = await $("//select[@container='body']");
            await selectVehicle.selectByVisibleText('vehicle 1');
            await Vehicle.VehicleButton.click();
            await browser.pause(7000);
            const selectedVehicle = await $("(//select[@container='body'])[2]");
            await selectedVehicle.selectByVisibleText('vehicle 1');
            await Vehicle.Year.setValue(json[i]['VYear']);
            await Vehicle.Make.setValue(json[i]['VMake']);
            await Vehicle.Model.setValue(json[i]['VModel']);
            await Vehicle.Color.setValue(json[i]['VColor']);
            await Vehicle.LisenceNo.setValue(json[i]['VLisenceNo']);
            const selectVehicleState = await $("(//select[@container='body'])[3]");
            await selectVehicleState.selectByVisibleText(json[i]['VehState']);
            await $("//label[.='Insurance Name']/following::ng-select").click();
            await browser.pause(5000);
            let VehicleInsurance = await $$("//div[@role='option']");

            for (let i = 0; i < VehicleInsurance.length; i++) {
                const element = await VehicleInsurance[i];
                if (await element.getText() === json[i]['VInsuranceName']) {
                    await element.click();
                    break;
                }

            }

            await Vehicle.PolicyNo.setValue(json[i]['VPolicyNo']);
            //await Vehicle.EffectiveDate.setValue("9/2/2021");
            //await Vehicle.ExpirationDate.setValue("10/03/2022");
            await Vehicle.AddDriverInfo(json[i]['DFirstName'],json[i]['DMiddleName'],json[i]['DLastName'], json[i]['DAddress'], json[i]['DSuite'], json[i]['DCity']);
            const DriverState = await $("(//select[@container='body'])[4]");
            await DriverState.selectByVisibleText(json[i]['DState']);
            await Vehicle.DriverZip.setValue(json[i]['DZip']);
            await Vehicle.AddOwnerInfo(json[i]['OFirstName'], json[i]['OMiddleName'],json[i]['OLastName'], json[i]['OAddress'], json[i]['OSuite'], json[i]['OCity']);
            const OwnerState = await $("(//select[@container='body'])[5]");
            await OwnerState.selectByVisibleText(json[i]['OState']);
            await Vehicle.OwnerZip.setValue(json[i]['OZip']);
            await Vehicle.SaveVehicle.click();
            await Vehicle.SaveAndContinue.click();
            await browser.pause(5000);
        });

        //----------------------------HouseHold Information---------------------------------------------------------------------//

        it('HouseHold Information Page', async () => {

            await HouseHold.LiveWithYou.click();
            await browser.pause(5000);
            await HouseHold.AddHouseHoldInfo(json[i]['HHFirstName'], json[i]['HHMiddleName'], json[i]['HHLastName'], json[i]['HHSSN'], json[i]['HHDOB'], json[i]['HHCellNumber']);
            const HouseHoldRelation = await $("//select[@container='body']");
            await HouseHoldRelation.selectByVisibleText(json[i]['HHRelation']);
            await $("//label[.='Insurance Name']/following::ng-select").click();
            await browser.pause(5000);
            let HouseInsurance = await $$("//div[@role='option']");

            for (let i = 0; i < HouseInsurance.length; i++) {
                const element = await HouseInsurance[i];
                if (await element.getText() === json[i]['HHInsuranceName']) {
                    await element.click();
                    break;
                }

            }
            await HouseHold.PolicyNumber.setValue(json[i]['HHPolicyNo']);
            await HouseHold.EffectiveDate.setValue(json[i]['HHEffectiveDate']);
            await HouseHold.ExpirationDate.setValue(json[i]['HHExpirationDate']);
            await HouseHold.OwnMotorVehicle.click();
            await HouseHold.SaveHouse.click();
            await browser.pause(8000);
            await HouseHold.SaveAndContinue.click();
            

        });
    
    //----------------------------------------------------Medical Treatment---------------------------------------------------//

    it('Medical Treatment', async () => {
        await browser.pause(15000);
        await Medical.FirstTreatment.click();
        await Medical.FirstTreatmentDate.setValue(json[i]['MFirstTreatmentDate']);
        await Medical.TreatedOnSite.click();
        await Medical.DateOfTreatment.setValue(json[i]['MDateOfTreatment']);
        await Medical.OffSiteTreatment.click();
        await Medical.TreatmentForInjury.click();

        await $("//label[.='Hospital Name']/following::ng-select").click();
        await browser.pause(5000);
        let Hospital = await $$("//div[@role='option']");

        for (let i = 0; i < Hospital.length; i++) {
            const element = await Hospital[i];
            if (await element.getText() === json[i]['MedicalHospitalName']) {
                await element.click();
                break;
            }

        }
        await Medical.TreatedForThisInjury.click();
        await browser.pause(3000);
        await Medical.AddDoctorInfo(json[i]['DocFirstName'], json[i]['DocMiddleName'], json[i]['DocLastName'], json[i]['DocAddress'], json[i]['DocSuite'],json[i]['DocCity'], json[i]['DocState'], json[i]['DocZip']);
        await Medical.SameBodyPart.click();
        await Medical.TreatedByDoctor.click();
        await Medical.InjuryWorkRelated.click();
        await $("//h6[.='Genaral Details']").scrollIntoView();
        await browser.pause(3000);
        await Medical.SameEmployer.click();
        await Medical.IMEExamination.click();

        // await Medical.NewYorkStateDisability.click();
        await Medical.AmountOfHealth.setValue(json[i]['Amount']);
        await Medical.AnyOtherExpanses.click();
        await Medical.HealthTreatment.click();
        await Medical.SaveAndContinue.click();
        await browser.pause(8000);
    });

    //--------------------------------------------- Injury---------------------------------------------------------//

    it('Injury Page', async () => {
        await browser.pause(5000);
        await Injury.AddInjuryInfo();
        await browser.pause(8000);
        await $("//a[.=' Patient ']").click();
        await browser.pause(9000);
        await $("//a[.='Patient List']").click();
        await browser.pause(6000);

    });
}
    //---------------------------------------Marketting-------------------------------------------------------------------//
    /*
    it('Marketting Page', async () => {
        await browser.pause(5000);
        const RefferedBy = await $("//select[@container='body']");
        await RefferedBy.selectByVisibleText('Patient');
        await $("//label[.='Enter Name']/following::input").setValue("Yasir");
        const Advertisement = await $("(//select[@container='body'])[2]");
        await Advertisement.selectByVisibleText('News');
        await $("//button[.='Save & Continue']").click();
        await browser.pause(3000);
        await $("(//div[.='Yes'])[2]").click();
        await browser.pause(20000);
        await $("(//span[.='×'])[2]").click();
        await browser.pause(15000);
        

    });
*/

});


