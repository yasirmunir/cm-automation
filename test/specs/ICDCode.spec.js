const icdCode = require("../pageobjects/ICDCode.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['ICD10Code'];
let json = utils.sheet_to_json(worksheet);
let ICDCodeName;
let Description;
let Comments;
let EditICDCodeName;
let EditDescription;
let EditComments;
describe('ICD10Code Page', () => {
    it('Should Login with valid Credentials', async () => {

        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await icdCode.MainMenu();
        await browser.pause(3000);

    }); 
    for (let i = 0; i < json.length; i++) {

        ICDCodeName = json[i]['ICDCodeName'] ? json[i]['ICDCodeName'] : '';
        Description = json[i]['Description'] ? json[i]['Description'] : '';
        Comments = json[i]['Comments'] ? json[i]['Comments'] : '';
        EditICDCodeName= json[i]['EditICDCodeName'] ? json[i]['EditICDCodeName'] : '';
        EditDescription= json[i]['EditDescription'] ? json[i]['EditDescription'] : '';
        EditComments = json[i]['EditComments'] ? json[i]['EditComments'] : '';
        

    it('Add ICDCode', async () => {
        await icdCode.AddICDCode(ICDCodeName,Description, Comments);
       

        
    });
}
    it('Filter ICDCode', async () => {
        await icdCode.FilterICDCode(ICDCodeName,Description, Comments)
        
        
    });
    it('Edit ICDCode', async () => {
        await icdCode.EditICDCode(EditICDCodeName,EditDescription, EditComments);
        await browser.pause(5000);

       

        
    });
    
})


