const Hbot = require("../pageobjects/HbotAppiontment.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['HbotAppiontment'];
let json = utils.sheet_to_json(worksheet);
let Location;
let Doctor;





describe('Hbot Unavailability', () => {


    for (let i = 0; i < json.length; i++) {

        Location = json[i]['Location'] ? json[i]['Location'] : '';
        Doctor = json[i]['Doctor'] ? json[i]['Doctor'] : '';


        it('Should Login with valid Credentials', async () => {

            await browser.url(configData.baseURl);
            await browser.maximizeWindow();
            await loginPage.Login(configData.username, configData.password);
            await Hbot.MainMenu();

            await browser.pause(5000);

            //-------------------------------------select Practice Location-----------------------------------------//
            let PracticeLocationArea = await Hbot.LocationArea


            for (let i = 0; i < PracticeLocationArea.length; i++) {
                const element = await PracticeLocationArea[i];
                if (await element.getText() == Location) {
                    await element.click();
                    break;
                }

            }

            //----------------------------------------Select Provider----------------------------------------------//
            await Hbot.Provider.click();
            await browser.pause(5000);
            let SelectProvider = await Hbot.ProviderList
            for (let i = 0; i < SelectProvider.length; i++) {
                const element = await SelectProvider[i];
                if (await element.getText() === Doctor) {
                    await element.click();
                    break;
                }

            }

            //---------------------------------------Select Day from  Month---------------------------------------//

            await Hbot.MonthView.click();
            await Hbot.PreviousDate.click();
            await Hbot.PreviousDate.click();
            await Hbot.PreviousDate.click();
            await Hbot.PreviousDate.click();
            await Hbot.PreviousDate.click();
            await Hbot.Unavailability.click();
            await Hbot.Subject.setValue("Vacation");
            await Hbot.Description.setValue("I will not be available for next 10 days");
            await Hbot.AddUnavailabilty.click();
            
            
            await browser.pause(6000);

        });
    }
});
