const icdCode = require("../pageobjects/PaymentActionType.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/Data.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['PaymentActionType']
let json = utils.sheet_to_json(worksheet);
let PaymentActionTypeNameData;
let Description;
let Comments;
let EditPaymentActionTypeNameData;
let EditDescription;
let EditComments;

describe('Payment Action  Type Page', () => {
    it('Should Login with valid Credentials', async () => {

        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await icdCode.MainMenu();
        await browser.pause(3000);
        await console.log(json);

    });
    
    for (let i = 0; i < json.length; i++) {

        PaymentActionTypeNameData= json[i]['PaymentActionTypeNameData'] ? json[i]['PaymentActionTypeNameData'] : '';
        Description = json[i]['Description'] ? json[i]['Description'] : '';
        Comments = json[i]['Comments'] ? json[i]['Comments'] : '';
        EditPaymentActionTypeNameData= json[i]['EditPaymentActionTypeNameData'] ? json[i]['EditPaymentActionTypeNameData'] : '';
        EditDescription = json[i]['EditDescription'] ? json[i]['EditDescription'] : '';
        EditComments = json[i]['EditComments'] ? json[i]['EditComments'] : '';


        it('Add Payment Action Type', async () => {
            await icdCode.AddPaymentActionType(PaymentActionTypeNameData,Description,Comments);



        });
    }
    
    it('Filter Payment Action Type', async () => {
        await icdCode.FilterPaymentActionType(PaymentActionTypeNameData, Description, Comments)


    });
    it('Edit Payment Action Type', async () => {
        await icdCode.EditPaymentActionType(EditPaymentActionTypeNameData, EditDescription, EditComments);
        await browser.pause(5000);




    });
    

})


