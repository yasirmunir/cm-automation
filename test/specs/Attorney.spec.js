const attorney = require("../pageobjects/Attorney.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['Attorney'];
let json = utils.sheet_to_json(worksheet);
/*
let FirstName;
let MiddleName;
let LastName;
let StreetAddress;
let Suite;
let City;
let Zip;
let PhoneNumber;
let Extension;
let CellNumber;
let Fax;
let Email;
let CPFirstName;
let CPMiddleName;
let CPLastName;
let CPPhoneNo;
let CPExtension;
let CPCellNo;
let CPFax;
let CPEmail;
let Comments;
let EditFirstName;
let EditMiddleName;
let EditLastName;
let EditStreetAddress;
let EditSuite;
let EditCity;
let EditZip;
let EditPhoneNumber;
let EditExtension;
let EditCellNumber;
let EditFax;
let EditEmail;
let EditCPFirstName;
let EditCPMiddleName;
let EditCPLastName;
let EditCPPhoneNumber;
let EditCPExtension;
let EditCPCellNo;
let EditCPFax;
let EditCPEmail;
let EditComments;
*/
describe('Attorney Page', () => {

    it('Should Login with valid Credentials', async () => {

        await browser.url(configData.baseURl);
       await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await attorney.MainMenu();
        await browser.pause(3000);


    });

    for (let i = 0; i < json.length; i++) {
        /*

        FirstName = json[i]['FirstName'] ? json[i]['FirstName'] : '';
        MiddleName = json[i]['MiddleName'] ? json[i]['MiddleName'] : '';
        LastName = json[i]['LastName'] ? json[i]['LastName'] : '';
        StreetAddress = json[i]['StreetAddress'] ? json[i]['StreetAddress'] : '';
        Suite = json[i]['Suite'] ? json[i]['Suite'] : '';
        City = json[i]['City'] ? json[i]['City'] : '';
        Zip = json[i]['Zip'] ? json[i]['Zip'] : '';
        PhoneNumber = json[i]['PhoneNumber'] ? json[i]['PhoneNumber'] : '';
        Extension = json[i]['Extension'] ? json[i]['Extension'] : '';
        CellNumber = json[i]['CellNumber'] ? json[i]['CellNumber'] : '';
        Fax = json[i]['Fax'] ? json[i]['Fax'] : '';
        Email= json[i]['Email'] ? json[i]['Email'] : '';
        CPFirstName = json[i]['CPFirstName'] ? json[i]['CPFirstName'] : '';
        CPMiddleName = json[i]['CPMiddleName'] ? json[i]['CPMiddleName'] : '';
        CPLastName = json[i]['CPLastName'] ? json[i]['CPLastName'] : '';
        CPPhoneNo = json[i]['CPPhoneNo'] ? json[i]['CPPhoneNo'] : '';
        CPExtension = json[i]['CPExtension'] ? json[i]['CPExtension'] : '';
        CPCellNo= json[i]['CPCellNo'] ? json[i]['CPCellNo'] : '';
        CPFax = json[i]['CPFax'] ? json[i]['CPFax'] : '';
        CPEmail = json[i]['CPEmail'] ? json[i]['CPEmail'] : '';
        Comments= json[i]['Comments'] ? json[i]['Comments'] : '';
        EditFirstName = json[i]['EditFirstName'] ? json[i]['EditFirstName'] : '';
        EditMiddleName = json[i]['EditMiddleName'] ? json[i]['EditMiddleName'] : '';
        EditLastName = json[i]['EditLastName'] ? json[i]['EditLastName'] : '';
        EditStreetAddress = json[i]['EditStreetAddress'] ? json[i]['EditStreetAddress'] : '';
        EditSuite = json[i]['EditSuite'] ? json[i]['EditSuite'] : '';
        EditCity = json[i]['EditCity'] ? json[i]['EditCity'] : '';
        EditZip = json[i]['EditZip'] ? json[i]['EditZip'] : '';
        EditPhoneNumber = json[i]['EditPhoneNumber'] ? json[i]['EditPhoneNumber'] : '';
        EditExtension = json[i]['EditExtension'] ? json[i]['EditExtension'] : '';
        EditCellNumber = json[i]['EditCellNumber'] ? json[i]['EditCellNumber'] : '';
        EditFax = json[i]['EditFax'] ? json[i]['EditFax'] : '';
        EditEmail= json[i]['EditEmail'] ? json[i]['EditEmail'] : '';
        EditCPFirstName = json[i]['EditCPFirstName'] ? json[i]['EditCPFirstName'] : '';
        EditCPMiddleName = json[i]['EditCPMiddleName'] ? json[i]['EditCPMiddleName'] : '';
        EditCPLastName = json[i]['EditCPLastName'] ? json[i]['EditCPLastName'] : '';
        EditCPPhoneNumber = json[i]['EditCPPhoneNumber'] ? json[i]['EditCPPhoneNumber'] : '';
        EditCPExtension = json[i]['EditCPExtension'] ? json[i]['EditCPExtension'] : '';
        EditCPCellNo= json[i]['EditCPCellNo'] ? json[i]['EditCPCellNo'] : '';
        EditCPFax = json[i]['EditCPFax'] ? json[i]['EditCPFax'] : '';
        EditCPEmail = json[i]['EditCPEmail'] ? json[i]['EditCPEmail'] : '';
        EditComments= json[i]['EditComments'] ? json[i]['EditComments'] : '';
      */

    it('Add Attorney', async () => {
        await browser.pause(4000);
        await attorney.AddNew.click();
        await attorney.SelectFirmName.click();
        list = await $$("//div[@role='option']");
        for (let i = 0; i < list.length; i++) {
            const element = await list[i];

            if (await element.getText() === 'Law Office of Gerard A. Nisivoccia, LLC') {
                await element.click();
                break;
            }

        }
        
        await attorney.AddAttorney(json[i]['FirstName'] , json[i]['MiddleName'], json[i]['LastName'],json[i]['Suite'] );
        /*
        await $("//label[.='State']/following::ng-select").click();

        list = await $$("//div[@role='option']");
        for (let i = 0; i < list.length; i++) {
            const element = await list[i];

            if (await element.getText() === 'CA') {
                await element.click();
                break;
            }

        }*/
        await attorney.StreetAddress.setValue(json[i]['StreetAddress']);
        await browser.pause(3000);
        await $("span.pac-icon.pac-icon-marker").click();
        await attorney.AddAttorneyData(json[i]['PhoneNumber'] ,json[i]['Extension'],json[i]['CellNumber'],json[i]['Fax'],json[i]['Email']);
        await attorney.AddContactPersonData(json[i]['CPFirstName'] ,json[i]['CPMiddleName'],json[i]['CPLastName'] , json[i]['CPPhoneNumber'] ,json[i]['CPExtension'] ,json[i]['CPCellNo'],json[i]['CPFax'] ,json[i]['CPEmail'],json[i]['Comments']);
        await browser.pause(5000);

    });
}
/*
    it('Filter Attorney', async () => {
        await attorney.FilterAttorney("Law Office of Gerard A. Nisivoccia, LLC","Yasir Munir Munir",PhoneNumber,Email,Fax);
        await browser.pause(5000);
        
    });

    it("Edit Attorney", async () => {
        await attorney.Edit.click();
      
        await attorney.EditAttorney(EditFirstName , EditMiddleName, EditLastName , EditStreetAddress, EditSuite, EditCity );
        await $("//label[.='State']/following::ng-select").click();

        list = await $$("//div[@role='option']");
        for (let i = 0; i < list.length; i++) {
            const element = await list[i];

            if (await element.getText() === 'CA') {
                await element.click();
                break;
            }


        }
        await attorney.EditAttorneyData(EditZip ,EditPhoneNumber , EditExtension,EditCPCellNo,EditFax,EditEmail);
        await attorney.EditContactPersonData(EditCPFirstName , EditCPMiddleName,EditCPLastName,EditCPPhoneNumber,EditCPExtension,EditCPCellNo,EditCPFax,EditCPEmail ,EditComments);
        await browser.pause(5000);
       


    })
    */
})

