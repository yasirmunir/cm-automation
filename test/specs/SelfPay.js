const patient = require("../pageobjects/PatientObject.page");
const personal = require("../pageobjects/PersonalObject.page");
const BasicInfo = require("../pageobjects/BasicInformationObject.page");
const FormFiller = require("../pageobjects/FormFillerObject.object");
const Emergency = require("../pageobjects/EmergencyInfo.page");
const Referral = require("../pageobjects/ReferralsObject.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['SelfPay']
let json = utils.sheet_to_json(worksheet);
//---------------------------------------Patient Test Data-------------------------------------//
let FirstName;
let MiddleName;
let LastName;
let SSN;
let HomePhone;
let WorkPhone;
let CellPhone;
let Email;
let Address;
let Suite;
let City;
let State;
let Zip;
//------------------------------Case Info Test Data-------------------------------------//
let PracticeLocation;
let Category;
let PurposeOfVisit;
let CaseType;
let DOA;
//--------------------------Personal Test Data------------------------------------------//
let PersonalSSN;
let Weight;
let Height;
let Inches;
//-----------------------------------Basic Contact Information Test Data-------------------------------//
let BHomePhone;
let BCellPhone;
let BWorkPhone;
let BExtension;
let BStreetAddress;
let BSuite;
let BCity;
let BState;
let BZip;
let BEmail;
let BFax;
//-------------------------------------Form Filler Test Data------------------------------------------//
let FFirstName;
let FMiddleName;
let FLastName;
let FCellPhone;
let FEmail;
let FFax;
let FStreetAddress;
let FSuite;
let FCity;
let FState;
let FZip;
let FRelation;
//------------------------------------Emergency Contact Information Test Data----------------------------------------//
let EFirstName;
let EMiddleName;
let ELastName;
let EDOB;
let EHomePhone;
let ECellPhone;
let EEmail;
let EFax;
let EStreetAddress;
let ESuite;
let ECity;
let EState;
let EZip;
let ERelation;
//----------------------------------- Refferals Test Data------------------------------------------------------------//
let RPFirstName;
let RPMiddleName;
let RPLastName;
let RPClinicName;
let RPEmail;
let RPhoneNo;
let RPExtension;
let RPAddress;
let RPSuite;
let RPCity;
let RPState;
let RPZip;
let PPFirstName;
let PPMiddleName;
let PPLastName;
let PPClinicName;
let PPEmail;
let PPPhoneNo;
let PPExtension;
let PPAddress;
let PPSuite;
let PPCity;
let PPState;
let PPZip;


describe('Self Pay Case Page', () => {
    it('Should Login with valid credentials', async () => {
        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await patient.MainMenu();


    });

    for (let i = 0; i < json.length; i++) {
        //----------------------Patient Info Data---------------------//

        FirstName = json[i]['FirstName'] ? json[i]['FirstName'] : '';
        MiddleName = json[i]['MiddleName'] ? json[i]['MiddleName'] : '';
        LastName = json[i]['LastName'] ? json[i]['LastName'] : '';
        SSN = json[i]['SSN'] ? json[i]['SSN'] : '';
        HomePhone = json[i]['HomePhone'] ? json[i]['HomePhone'] : '';
        WorkPhone = json[i]['WorkPhone'] ? json[i]['WorkPhone'] : '';
        CellPhone = json[i]['CellPhone'] ? json[i]['CellPhone'] : '';
        Email = json[i]['Email'] ? json[i]['Email'] : '';
        Address = json[i]['Address'] ? json[i]['Address'] : '';
        Suite = json[i]['Suite'] ? json[i]['Suite'] : '';
        City = json[i]['City'] ? json[i]['City'] : '';
        State = json[i]['State'] ? json[i]['State'] : '';
        Zip = json[i]['Zip'] ? json[i]['Zip'] : '';
        //---------------Case Info data--------------------------------//
        PracticeLocation = json[i]['PracticeLocation'] ? json[i]['PracticeLocation'] : '';
        Category = json[i]['Category'] ? json[i]['Category'] : '';
        PurposeOfVisit = json[i]['PurposeOfVisit'] ? json[i]['PurposeOfVisit'] : '';
        CaseType = json[i]['CaseType'] ? json[i]['CaseType'] : '';
        DOA = json[i]['DOA'] ? json[i]['DOA'] : '';
        //--------------Personal Data-----------------------------------//
        PersonalSSN = json[i]['PersonalSSN'] ? json[i]['PersonalSSN'] : '';
        Weight = json[i]['Weight'] ? json[i]['Weight'] : '';
        Height = json[i]['Height'] ? json[i]['Height'] : '';
        Inches = json[i]['Inches'] ? json[i]['Inches'] : '';
        //-------------Basic Contact Information  Data-----------------------//
        BHomePhone = json[i]['BHomePhone'] ? json[i]['BHomePhone'] : '';
        BCellPhone = json[i]['BCellPhone'] ? json[i]['BCellPhone'] : '';
        BWorkPhone = json[i]['BWorkPhone'] ? json[i]['BWorkPhone'] : '';
        BExtension = json[i]['BExtension'] ? json[i]['BExtension'] : '';
        BStreetAddress = json[i]['BStreetAddress'] ? json[i]['BStreetAddress'] : '';
        BSuite = json[i]['BSuite'] ? json[i]['BSuite'] : '';
        BCity = json[i]['BCity'] ? json[i]['BCity'] : '';
        BState = json[i]['BState'] ? json[i]['BState'] : '';
        BZip = json[i]['BZip'] ? json[i]['BZip'] : '';
        BEmail = json[i]['BEmail'] ? json[i]['BEmail'] : '';
        BFax = json[i]['BFax'] ? json[i]['BFax'] : '';
        //-------------------------------Form Filler Information Data------------------------------------------//
        FFirstName = json[i]['FFirstName'] ? json[i]['FFirstName'] : '';
        FMiddleName = json[i]['FMiddleName'] ? json[i]['FMiddleName'] : '';
        FLastName = json[i]['FLastName'] ? json[i]['FLastName'] : '';
        FCellPhone = json[i]['FCellPhone'] ? json[i]['FCellPhone'] : '';
        FEmail = json[i]['FEmail'] ? json[i]['FEmail'] : '';
        FFax = json[i]['FFax'] ? json[i]['FFax'] : '';
        FStreetAddress = json[i]['FStreetAddress'] ? json[i]['FStreetAddress'] : '';
        FSuite = json[i]['FSuite'] ? json[i]['FSuite'] : '';
        FCity = json[i]['FCity'] ? json[i]['FCity'] : '';
        FState = json[i]['FState'] ? json[i]['FState'] : '';
        FZip = json[i]['FZip'] ? json[i]['FZip'] : '';
        FRelation = json[i]['FRelation'] ? json[i]['FRelation'] : '';

        //---------------------------------Emergency Conatact Person Data------------------------------------------------//
        EFirstName = json[i]['EFirstName'] ? json[i]['EFirstName'] : '';
        EMiddleName = json[i]['EMiddleName'] ? json[i]['EMiddleName'] : '';
        ELastName = json[i]['ELastName'] ? json[i]['ELastName'] : '';
        EDOB = json[i]['EDOB'] ? json[i]['EDOB'] : '';
        EHomePhone = json[i]['EHomePhone'] ? json[i]['EHomePhone'] : '';
        ECellPhone = json[i]['ECellPhone'] ? json[i]['ECellPhone'] : '';
        EEmail = json[i]['EEmail'] ? json[i]['EEmail'] : '';
        EFax = json[i]['EFax'] ? json[i]['EFax'] : '';
        EStreetAddress = json[i]['EStreetAddress'] ? json[i]['EStreetAddress'] : '';
        ESuite = json[i]['ESuite'] ? json[i]['ESuite'] : '';
        ECity = json[i]['ECity'] ? json[i]['ECity'] : '';
        EState = json[i]['EState'] ? json[i]['EState'] : '';
        EZip = json[i]['EZip'] ? json[i]['EZip'] : '';
        ERelation = json[i]['ERelation'] ? json[i]['ERelation'] : '';
        //------------------------------------Reffrals Data----------------------------------------//
        RPFirstName = json[i]['RPFirstName'] ? json[i]['RPFirstName'] : '';
        RPMiddleName = json[i]['RPMiddleName'] ? json[i]['RPMiddleName'] : '';
        RPLastName = json[i]['RPLastName'] ? json[i]['RPLastName'] : '';
        RPClinicName = json[i]['RPClinicName'] ? json[i]['RPClinicName'] : '';
        RPEmail = json[i]['RPEmail'] ? json[i]['RPEmail'] : '';
        RPhoneNo = json[i]['RPhoneNo'] ? json[i]['RPhoneNo'] : '';
        RPExtension = json[i]['RPExtension'] ? json[i]['RPExtension'] : '';
        RPAddress = json[i]['RPAddress'] ? json[i]['RPAddress'] : '';
        RPSuite = json[i]['RPSuite'] ? json[i]['RPSuite'] : '';
        RPCity = json[i]['RPCity'] ? json[i]['RPCity'] : '';
        RPState = json[i]['RPState'] ? json[i]['RPState'] : '';
        RPZip = json[i]['RPZip'] ? json[i]['RPZip'] : '';
        PPFirstName = json[i]['PPFirstName'] ? json[i]['PPFirstName'] : '';
        PPMiddleName = json[i]['PPMiddleName'] ? json[i]['PPMiddleName'] : '';
        PPLastName = json[i]['PPLastName'] ? json[i]['PPLastName'] : '';
        PPClinicName = json[i]['PPClinicName'] ? json[i]['PPClinicName'] : '';
        PPEmail = json[i]['PPEmail'] ? json[i]['PPEmail'] : '';
        PPPhoneNo = json[i]['PPPhoneNo'] ? json[i]['PPPhoneNo'] : '';
        PPExtension = json[i]['PPExtension'] ? json[i]['PPExtension'] : '';
        PPAddress = json[i]['PPAddress'] ? json[i]['PPAddress'] : '';
        PPSuite = json[i]['PPSuite'] ? json[i]['PPSuite'] : '';
        PPCity = json[i]['PPCity'] ? json[i]['PPCity'] : '';
        PPState = json[i]['PPState'] ? json[i]['PPState'] : '';
        PPZip = json[i]['PPZip'] ? json[i]['PPZip'] : '';




        // ------------------------------ADD PATIENT------------------------------//
        it('Add Patient', async () => {
            await patient.AddPatient(FirstName, MiddleName, LastName, "10031988", SSN, HomePhone, WorkPhone, CellPhone, Email, Address, Suite, City);
            const selectState = await $("//select[@container='body']");
            await selectState.selectByVisibleText(State);
            await patient.Zip.setValue(Zip);
            await patient.SaveAndContinue.click();
            await browser.pause(3000);
            await patient.IamNewPatient.click();
            await browser.pause(3000);
            await patient.AddNewCase.click();

        });



        //------------------------CASE INFO----------------------------------------//

        it('Case Info Page', async () => {
            //--------select Practice Location-------------//


            await $('//label[.="Practice*"]/following::ng-select').click();
            await browser.pause(4000);
            let list1 = await $$("//div[@role='option']")
            for (let i = 0; i < list1.length; i++) {
                const element = await list1[i];
                if (await element.getText() === PracticeLocation) {
                    await element.click();
                    break;
                }

            }
            await $("//label[.='Practice*']").click();
            //-----------------select category---------------------------------//
            const selectCategory = await $("//select[@tooltipclass='table-custom-tooltip']");
            await selectCategory.selectByVisibleText(Category);
            await browser.pause(4000);
            //---------------Select Purpose of Visit-------------------------//
            const selectPurposeOfVisit = await $("(//select[@container='body'])[2]");
            await selectPurposeOfVisit.selectByVisibleText(PurposeOfVisit);
            await browser.pause(4000);

            //---------------------select Case Type-------------------------//

            const selectCaseType = await $("(//select[@container='body'])[3]");
            await selectCaseType.selectByVisibleText(CaseType);

            await $("//label[.='DOA * (mm/dd/yyyy)']/following::input").setValue(DOA);
            await $("//button[.='Save & Continue']").click();
            await browser.pause(3000);
            await $("//button[.='Create a New Case']").click();
        });
        //------------------------------ Personal Information Page--------------------------------------//


        it('Personal Information Page', async () => {
            await browser.pause(5000);
            await personal.AddWCPersonalInfo(PersonalSSN, Weight, Height, Inches);

            /*
           await $('//label[.="If yes, What Language?"]/following::ng-select').click();
           await browser.pause(6000);
           let list2 = await $$("//div[@role='option']")
           for (let i = 0; i < list2.length; i++) {
               const element = await list2[i];
               if (await element.getText() === 'Afar') {
                   await element.click();
                   break;
               }
        
           }
           */

            await browser.pause(3000);
            await personal.SaveAndContinue.click();


        });


        //----------------------------- Basic Information Page---------------------------------------------------//
        it('Basic Information Page', async () => {

            await BasicInfo.AddBasicContactInformation(BHomePhone, BCellPhone, BWorkPhone, BExtension, BStreetAddress, BSuite, BCity, BState, BZip, BEmail, BFax);



        });

        //---------------------------------Form Filler Page------------------------------------------------------//
        it('Form Filler Page', async () => {
            await FormFiller.AddFormFillerInfo(FFirstName, FMiddleName, FLastName, FCellPhone, FEmail, FFax, FStreetAddress, FSuite, FCity, FState, FZip);
            const selectRelation = await $("//select[@container='body']");
            await selectRelation.selectByVisibleText(FRelation);
            await FormFiller.EmergencyContactPerson.click();
            await FormFiller.SaveAndContinue.click();
            await browser.pause(6000);


        });


        //-----------------------------Emergency Contact Person Information----------------------------------------------------------//

        it('Emergency Contact Information', async () => {
            await Emergency.AddEmergencyContactInformation(EFirstName, EMiddleName, ELastName, EDOB, EHomePhone, ECellPhone, EEmail, EFax, EStreetAddress, ESuite, ECity, EState, EZip);
            const selectRel = await $("//select[@container='body']");
            await selectRel.selectByVisibleText(ERelation);
            await Emergency.SaveAndContinue.click();
            await browser.pause(6000);

        });
        //----------------------------Referrals Page---------------------------------------------------------------//

        it('Referrals Page', async () => {
            await Referral.AddRefferedPhysician(RPFirstName, RPMiddleName, RPLastName, RPClinicName, RPEmail, RPhoneNo, RPExtension, RPAddress, RPSuite, RPCity);
            const selectState = await $("//select[@container='body']");
            await selectState.selectByVisibleText(RPState);
            await Referral.Zip.setValue(RPZip);
            await Referral.PrimaryPhysician.click();
            await Referral.AddPrimaryPhysician(PPFirstName, PPMiddleName, PPLastName, PPClinicName, PPEmail, PPPhoneNo, PPExtension, PPAddress, PPSuite, PPCity);
            const PrimaryselectState = await $("(//select[@container='body'])[2]");
            await PrimaryselectState.selectByVisibleText(PPState);
            await Referral.PZip.setValue(PPZip);
            await Referral.SaveAndContinue.click();
            await browser.pause(6000);

        });



    }
    //---------------------------------------Marketting-------------------------------------------------------------------//
    it('Marketting Page', async () => {
        await browser.pause(5000);
        const RefferedBy = await $("//select[@container='body']");
        await browser.pause(3000);
        await RefferedBy.selectByVisibleText('Patient');
        await $("//label[.='Enter Name']/following::input").setValue("Yasir");
        const Advertisement = await $("(//select[@container='body'])[2]");
        await Advertisement.selectByVisibleText('News');
        await $("//button[.='Save & Continue']").click();
        await browser.pause(19000);
        await $("//app-case-edit[@class='ng-star-inserted']//div[@class='ng-star-inserted'][normalize-space()='Yes']").click();
        await browser.pause(15000);
        await $("(//span[.='×'])[2]").click();
        await browser.pause(5000);

    });


});


