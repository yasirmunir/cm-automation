const icdCode = require("../pageobjects/EorStatus.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/Data.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['EorStatus']
let json = utils.sheet_to_json(worksheet);
let EorStausNameData;
let Description;
let Comments;
let EditEorStausNameData;
let EditDescription;
let EditComments;
describe('Eor Status Page', () => {
    it('Should Login with valid Credentials', async () => {

        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await icdCode.MainMenu();
        await browser.pause(3000);
        await console.log(json);

    });
    for (let i = 0; i < json.length; i++) {

        EorStausNameData = json[i]['EorStausNameData'] ? json[i]['EorStausNameData'] : '';
        Description = json[i]['Description'] ? json[i]['Description'] : '';
        Comments = json[i]['Comments'] ? json[i]['Comments'] : '';
        EditEorStausNameData = json[i]['EditEorStausNameData'] ? json[i]['EditEorStausNameData'] : '';
        EditDescription = json[i]['EditDescription'] ? json[i]['EditDescription'] : '';
        EditComments = json[i]['EditComments'] ? json[i]['EditComments'] : '';


        it('Add Eor Status', async () => {
            await icdCode.AddEorStatus(EorStausNameData, Description, Comments);



        });
    }
    it('Filter Eor Status', async () => {
        await icdCode.FilterEorStatus(EorStausNameData, Description, Comments)


    });
    it('Edit  Eor Status', async () => {
        await icdCode.EditEorStatus(EditEorStausNameData, EditDescription, EditComments);
        await browser.pause(5000);




    });

})


