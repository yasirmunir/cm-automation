const icdCode = require("../pageObjects/EorType.page");
const loginPage = require("../pageObjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['EorType']
let json = utils.sheet_to_json(worksheet);
let EorTypeName;
let Description;
let Comments;
let EditEorTypeName;
let EditDescription;
let EditComments;
describe('Eor Type Page', () => {
    it('Should Login with valid Credentials', async () => {

        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await icdCode.MainMenu();
        await browser.pause(3000);

    });
    for (let i = 0; i < json.length; i++) {

        EorTypeName = json[i]['EorTypeName'] ? json[i]['EorTypeName'] : '';
        Description = json[i]['Description'] ? json[i]['Description'] : '';
        Comments = json[i]['Comments'] ? json[i]['Comments'] : '';
        EditEorTypeName = json[i]['EditEorTypeName'] ? json[i]['EditEorTypeName'] : '';
        EditDescription = json[i]['EditDescription'] ? json[i]['EditDescription'] : '';
        EditComments = json[i]['EditComments'] ? json[i]['EditComments'] : '';


        it('Add Eor Type', async () => {
            await icdCode.AddEorType(EorTypeName, Description, Comments);



        });
    }
    it('Filter Eor Type', async () => {
        await icdCode.FilterEorType(EorTypeName, Description, Comments)


    });
    it('Edit  Eor Type', async () => {
        await icdCode.EditEorType(EditEorTypeName, EditDescription, EditComments);
        await browser.pause(5000);




    });

})


