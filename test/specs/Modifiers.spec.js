const icdCode = require("../pageobjects/Modifier.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['Modifiers']
let json = utils.sheet_to_json(worksheet);
let ModifierName;
let Description;
let Comments;
let EditModifierName;
let EditDescription;
let EditComments;
describe('Modifiers Page', () => {
    it('Should Login with valid Credentials', async () => {

        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await icdCode.MainMenu();
        await browser.pause(3000);

    });
    for (let i = 0; i < json.length; i++) {

        ModifierName = json[i]['ModifierName'] ? json[i]['ModifierName'] : '';
        Description = json[i]['Description'] ? json[i]['Description'] : '';
        Comments = json[i]['Comments'] ? json[i]['Comments'] : '';
        EditModifierName = json[i]['EditModifierName'] ? json[i]['EditModifierName'] : '';
        EditDescription = json[i]['EditDescription'] ? json[i]['EditDescription'] : '';
        EditComments = json[i]['EditComments'] ? json[i]['EditComments'] : '';


        it('Add Modifiers', async () => {
            await icdCode.AddModifiers(ModifierName, Description, Comments);



        });
    }
    it('Filter Modifiers', async () => {
        await icdCode.FilterModifiers(ModifierName, Description, Comments)


    });
    it('Edit  Modifiers', async () => {
        await icdCode.EditModifiers(EditModifierName, EditDescription, EditComments);
        await browser.pause(5000);




    });

});


