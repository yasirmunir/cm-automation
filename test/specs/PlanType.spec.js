const planType = require("../pageobjects/PlanTypePageObject.page")
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['PlanType'];
let json = utils.sheet_to_json(worksheet);
let PlanTypeName;
let Comments;
let EditPlanTypeName;
let EditComments;

describe('Plan Type Page', () => {
    it('Should Login with valid Credentials', async () => {

        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await planType.MainMenu();


    });
    for (let i = 0; i < json.length; i++) {

        PlanTypeName = json[i]['PlanTypeName'] ? json[i]['PlanTypeName'] : '';
        Comments = json[i]['Comments'] ? json[i]['Comments'] : '';
        EditPlanTypeName = json[i]['EditPlanTypeName'] ? json[i]['EditPlanTypeName'] : '';
        EditComments = json[i]['EditComments'] ? json[i]['EditComments'] : '';
        it('Add Plan Type', async () => {
            await planType.AddPlanType(PlanTypeName, Comments);
            await browser.pause(4000);


        });
    }
    it('Filter Plan Type', async () => {

        await planType.FilterPlanType(PlanTypeName, Comments);
        await browser.pause(4000);

    });
    it('Edit Plan Type', async () => {
        await planType.EditPlanType(EditPlanTypeName, EditComments);
        await browser.pause(4000);

    });


})
