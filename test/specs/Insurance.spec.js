const insurance = require("../pageobjects/Insurance.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/Data.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['Insurance'];
let json = utils.sheet_to_json(worksheet);
/*
let InsuranceName;
let InsuranceCode;
let LocationName;
let LocationCode;
let StreetAddress;
let Suite;
let City;
let Zip;
let PhoneNumber;
let Extension;
let CellNo;
let Fax;
let Email;
let FirstName;
let MiddleName;
let LastName;
let CPPhoneNumber;
let CPExtension;
let CPCellNo;
let CPFax;
let CPEmail;
let Comments;
let EditInsuranceName;
let EditInsuranceCode;
*/


describe('Insurance Page', () => {
    it('Login with valid Credentials', async () => {
        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await insurance.MainMenu();
        await browser.pause(3000);
        

    });

    for (let i = 0; i < json.length; i++) {
        /*

        InsuranceName = json[i]['InsuranceName'] ? json[i]['InsuranceName'] : '';
        InsuranceCode = json[i]['InsuranceCode'] ? json[i]['InsuranceCode'] : '';
        LocationName = json[i]['LocationName'] ? json[i]['LocationName'] : '';
        LocationCode = json[i]['LocationCode'] ? json[i]['LocationCode'] : '';
        StreetAddress = json[i]['StreetAddress'] ? json[i]['StreetAddress'] : '';
        Suite = json[i]['Suite'] ? json[i]['Suite'] : '';
        City = json[i]['City'] ? json[i]['City'] : '';
        Zip = json[i]['Zip'] ? json[i]['Zip'] : '';
        PhoneNumber = json[i]['PhoneNumber'] ? json[i]['PhoneNumber'] : '';
        Extension = json[i]['Extension'] ? json[i]['Extension'] : '';
        CellNo = json[i]['CellNo'] ? json[i]['CellNo'] : '';
        Fax = json[i]['Fax'] ? json[i]['Fax'] : '';
        Email = json[i]['Email'] ? json[i]['Email'] : '';
        FirstName = json[i]['FirstName'] ? json[i]['FirstName'] : '';
        MiddleName = json[i]['MiddleName'] ? json[i]['MiddleName'] : '';
        LastName = json[i]['LastName'] ? json[i]['LastName'] : '';
        CPPhoneNumber = json[i]['CPPhoneNumber'] ? json[i]['CPPhoneNumber'] : '';
        CPExtension = json[i]['CPExtension'] ? json[i]['CPExtension'] : '';
        CPCellNo = json[i]['CPCellNo'] ? json[i]['CPCellNo'] : '';
        CPFax = json[i]['CPFax'] ? json[i]['CPFax'] : '';
        CPEmail = json[i]['CPEmail'] ? json[i]['CPEmail'] : '';
        Comments = json[i]['Comments'] ? json[i]['Comments'] : '';
        EditInsuranceName = json[i]['EditInsuranceName'] ? json[i]['EditInsuranceName'] : '';
        EditInsuranceCode = json[i]['EditInsuranceCode'] ? json[i]['EditInsuranceCode'] : '';
        */
        it('Add Insurance', async () => {
            await insurance.AddNew.click();
            await insurance.AddInsurance(json[i]['InsuranceName'], json[i]['InsuranceCode']);
            await browser.pause(2000);
            await insurance.AddLocation.click();
            await insurance.AddInsuranceLocation(json[i]['LocationName'], json[i]['LocationCode'],json[i]['Suite']);
            await insurance.StreetAddress.setValue(json[i]['StreetAddress']);
            await browser.pause(4000);
            await $("span.pac-icon.pac-icon-marker").click();
            /*
            await $("//label[.='State']/following::ng-select").click();

            list = await $$("//div[@role='option']")
            for (let i = 0; i < list.length; i++) {
                const element = await list[i];
                if (await element.getText() === 'CA') {
                    await element.click();
                    break;
                }

            }*/
            await insurance.AddLocationData( json[i]['PhoneNumber'],json[i]['Extension'],json[i]['CellNo'], json[i]['Fax'], json[i]['Email']);
            await insurance.AddContactPersonData(json[i]['FirstName'], json[i]['MiddleName'], json[i]['LastName'], json[i]['CPPhoneNumber'], json[i]['CPExtension'], json[i]['CPCellNo'], json[i]['CPFax'], json[i]['CPEmail'], json[i]['Comments']);
            await browser.pause(4000);
            


        });
    }
    /*
    it('Filter Insurance', async () => {
        await insurance.FilterInsurance(InsuranceName, InsuranceCode, LocationName);
        await browser.pause(2000);
        await insurance.FilterInsuranceData(Email, Fax, PhoneNumber);
        await browser.pause(5000);

    });
    it('Edit Insurance', async () => {
        await insurance.EditInsurance(EditInsuranceName, EditInsuranceCode);
       

        await browser.pause(15000);

    });
    */

})

