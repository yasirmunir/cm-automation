const region = require("../pageobjects/Region.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/Data.xlsx";
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['Region'];
let json = utils.sheet_to_json(worksheet);
let RegionNameData;
let Description
let Comments;
let EditDescription;
let EditComments;

describe('Region Page Test', () => {
    it('Should Login with valid Credentials', async () => {

        await browser.url(configData.baseURl);
        await browser.maximizeWindow();;
        await loginPage.Login(configData.username, configData.password);
        await  region.MainMenu();
        

    });

    for (let i = 0; i < json.length; i++) {

        //RegionNameData = json[i]['RegionNameData'] ? json[i]['RegionNameData'] : '';
        Description = json[i]['Description'] ? json[i]['Description'] : '';
        Comments = json[i]['Comments'] ? json[i]['Comments'] : '';
        EditDescription = json[i]['EditDescription'] ? json[i]['EditDescription'] : '';
        EditComments = json[i]['EditComments'] ? json[i]['EditComments'] : '';

        it('Add Region', async () => {
           await region.AddRegion("Hello122221111", Description,Comments);
            await browser.pause(3000);


        });
        it('Filter Region', async () => {
            await region.FilterRegion("Hello122221111",Description,Comments);
            
        });
        it('Edit Region', async () => {
            await region.EditRegion("Hello221aaa1",EditDescription,EditComments);
            
        });
    }
    
})
