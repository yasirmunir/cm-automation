const icdCode = require("../pageobjects/CaseStatus.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['CaseStatus']
let json = utils.sheet_to_json(worksheet);
let CaseStatusName;
let Description;
let Comments;
let EditCaseStatusName;
let EditDescription;
let EditComments;
describe('Case Status Page', () => {
    it('Should Login with valid Credentials', async () => {

        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await icdCode.MainMenu();
        await browser.pause(3000);

    });
    for (let i = 0; i < json.length; i++) {

        CaseStatusName= json[i]['CaseStatusName'] ? json[i]['CaseStatusName'] : '';
        Description = json[i]['Description'] ? json[i]['Description'] : '';
        Comments = json[i]['Comments'] ? json[i]['Comments'] : '';
        EditCaseStatusName = json[i]['EditCaseStatusName'] ? json[i]['EditCaseStatusName'] : '';
        EditDescription = json[i]['EditDescription'] ? json[i]['EditDescription'] : '';
        EditComments = json[i]['EditComments'] ? json[i]['EditComments'] : '';


        it('Add Case Status ', async () => {
            await icdCode.AddCaseStatus(CaseStatusName, Description, Comments);



        });
    }
    it('Filter Case Status', async () => {
        await icdCode.FilterCaseStatus(CaseStatusName, Description, Comments)


    });
    it('Edit Case Status', async () => {
        await icdCode.EditCaseStatus(EditCaseStatusName, EditDescription, EditComments);
        await browser.pause(5000);




    });

})


