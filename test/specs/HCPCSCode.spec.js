const icdCode = require("../pageobjects/HCPCSCode.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['HCPCSCODE'];
let json = utils.sheet_to_json(worksheet);
let HCPCSCodeName;
let Description;
let Comments;
let EditHCPCSCodeName;
let EditDescription;
let EditComments;
describe('HCPCSCode Page', () => {
    it('Should Login with valid Credentials', async () => {

        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await icdCode.MainMenu();
        await browser.pause(3000);

    }); 
    for (let i = 0; i < json.length; i++) {

        HCPCSCodeName = json[i]['HCPCSCodeName'] ? json[i]['HCPCSCodeName'] : '';
        Description = json[i]['Description'] ? json[i]['Description'] : '';
        Comments = json[i]['Comments'] ? json[i]['Comments'] : '';
        EditHCPCSCodeName= json[i]['EditHCPCSCodeName'] ? json[i]['EditHCPCSCodeName'] : '';
        EditDescription= json[i]['EditDescription'] ? json[i]['EditDescription'] : '';
        EditComments = json[i]['EditComments'] ? json[i]['EditComments'] : '';
        

    it('Add HCPCSCode', async () => {
        await icdCode.AddHCPCSCode(HCPCSCodeName,Description, Comments);
       

        
    });
}
    it('Filter HCPCSCode', async () => {
        await icdCode.FilterHCPCSCode(HCPCSCodeName,Description, Comments)
        
        
    });
    it('Edit HCPCSCode', async () => {
        await icdCode.EditHCPCSCode(EditHCPCSCodeName,EditDescription, EditComments);
        await browser.pause(5000);

       

        
    });
    
})


