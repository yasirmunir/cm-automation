const hospital = require("../pageobjects/HospitalPageObeject.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['Hospital'];
let json = utils.sheet_to_json(worksheet);
let HospitalName;
let StreetAddress;
let Apartment;
let City;
let ZipCode;
let PhoneNumber;
let Extension;
let Fax;
let Email;
let FirstName;
let MiddleName;
let LastName;
let CPPhoneNo;
let CPExtension;
let CPCellNo;
let CPFax;
let CPEmail;
let EditHospitalName;
let EditStreetAddress;
let EditApartment;
let EditCity;
let EditZipCode;
let EditPhoneNumber;
let EditExtension;
let EditFax;
let EditEmail;
let EditFirstName;
let EditMiddleName;
let EditLastName;
let EditCPPhoneNo;
let EditCPExt;
let EditCPCellNumber;
let EditCpFax;
let EditCPEmail;
let list4;


describe('Hospital Page', () => {

    it('Login with valid Credentials', async () => {
        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await hospital.MainMenu();

    });
    for (let i = 0; i < json.length; i++) {

        HospitalName = json[i]['HospitalName'] ? json[i]['HospitalName'] : '';
        StreetAddress = json[i]['StreetAddress'] ? json[i]['StreetAddress'] : '';
        Apartment = json[i]['Apartment'] ? json[i]['Apartment'] : '';
        City = json[i]['City'] ? json[i]['City'] : '';
        ZipCode = json[i]['ZipCode'] ? json[i]['ZipCode'] : '';
        PhoneNumber = json[i]['PhoneNumber'] ? json[i]['PhoneNumber'] : '';
        Extension = json[i]['Extension'] ? json[i]['Extension'] : '';
        Fax = json[i]['Fax'] ? json[i]['Fax'] : '';
        Email = json[i]['Email'] ? json[i]['Email'] : '';
        FirstName = json[i]['FirstName'] ? json[i]['FirstName'] : '';
        MiddleName = json[i]['MiddleName'] ? json[i]['MiddleName'] : '';
        LastName = json[i]['LastName'] ? json[i]['LastName'] : '';
        CPPhoneNo = json[i]['CPPhoneNo'] ? json[i]['CPPhoneNo'] : '';
        CPExtension = json[i]['CPExtension'] ? json[i]['CPExtension'] : '';
        CPCellNo = json[i]['CPCellNo'] ? json[i]['CPCellNo'] : '';
        CPFax = json[i]['CPFax'] ? json[i]['CPFax'] : '';
        CPEmail = json[i]['CPEmail'] ? json[i]['CPEmail'] : '';
        EditHospitalName = json[i]['EditHospitalName'] ? json[i]['EditHospitalName'] : '';
        EditStreetAddress = json[i]['EditStreetAddress'] ? json[i]['EditStreetAddress'] : '';
        EditApartment = json[i]['EditApartment'] ? json[i]['EditApartment'] : '';
        EditCity = json[i]['EditCity'] ? json[i]['EditCity'] : '';
        EditZipCode = json[i]['EditZipCode'] ? json[i]['EditZipCode'] : '';
        EditPhoneNumber = json[i]['EditPhoneNumber'] ? json[i]['EditPhoneNumber'] : '';
        EditExtension = json[i]['EditExtension'] ? json[i]['EditExtension'] : '';
        EditFax = json[i]['EditFax'] ? json[i]['EditFax'] : '';
        EditEmail = json[i]['EditEmail'] ? json[i]['EditEmail'] : '';
        EditFirstName = json[i]['EditFirstName'] ? json[i]['EditFirstName'] : '';
        EditMiddleName = json[i]['EditMiddleName'] ? json[i]['EditMiddleName'] : '';
        EditLastName = json[i]['EditLastName'] ? json[i]['EditLastName'] : '';
        EditCPPhoneNo = json[i]['EditCPPhoneNo'] ? json[i]['EditCPPhoneNo'] : '';
        EditCPExt = json[i]['EditCPExt'] ? json[i]['EditCPExt'] : '';
        EditCPCellNumber = json[i]['EditCPCellNumber'] ? json[i]['EditCPCellNumber'] : '';
        EditCpFax = json[i]['EditCpFax'] ? json[i]['EditCpFax'] : '';
        EditCPEmail = json[i]['EditCPEmail'] ? json[i]['EditCPEmail'] : '';
        EditEmail = json[i]['EditEmail'] ? json[i]['EditEmail'] : '';






        it('Add Hospital', async () => {

            await hospital.AddHospital(HospitalName, StreetAddress, Apartment, City);
            await $("//label[.='State']/following::ng-select").click();

            list = await $$("//div[@role='option']")
            for (let i = 0; i < list.length; i++) {
                const element = await list[i];
                if (await element.getText() === 'CA') {
                    await element.click();
                    break;
                }

            }
            await hospital.AddHospitalData(ZipCode, PhoneNumber, Extension, Fax, Email);
            await $("//span[.='Department(s)']").click();
            await $("//input[@placeholder='Search']").setValue("depart");
            await browser.pause(3000);
            list4 = await $$("//li[@class='multiselect-item-checkbox']");

           for (let i = 0; i < list4.length; i++) {
                const element = await list4[i];

                if (await element.getText() === 'depart') {
                    await element.click();


                    break;
                }

            }
            await hospital.AddContactPersonData(FirstName, MiddleName, LastName, CPPhoneNo, CPExtension, CPCellNo, CPFax, CPEmail)
            await browser.pause(5000);
        });
    }
    it('Filter Hospital', async () => {
        await hospital.FilterHospital(HospitalName, PhoneNumber, StreetAddress, Email);
        await hospital.PlusIcon.click();
        await browser.pause(3000);
        await hospital.FilterData(Fax);


    });

    it('Edit Hospital', async () => {
        await hospital.EditHospital(EditHospitalName, EditStreetAddress, EditApartment, EditCity);
        await $("//label[.='State']/following::ng-select").click();

        list = await $$("//div[@role='option']")
        for (let i = 0; i < list.length; i++) {
            const element = await list[i];
            if (await element.getText() === 'CO') {
                await element.click();
                break;
            }

        }
        await hospital.EditHospitalData(EditZipCode, EditPhoneNumber, EditExtension, EditFax, EditEmail);
        await hospital.EditContactPersonData(EditFirstName, EditMiddleName, EditLastName, EditCPPhoneNo, EditCPExt, EditCPCellNumber, EditCpFax, EditCPEmail);
        await browser.pause(5000);

    });


});
