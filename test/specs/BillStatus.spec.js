const icdCode = require("../pageobjects/BillStatus.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['BillStatus']
let json = utils.sheet_to_json(worksheet);
let BillStatusName;
let Description;
let Comments;
let EditBillStatusName;
let EditDescription;
let EditComments;
describe('Bill Status Page', () => {
    it('Should Login with valid Credentials', async () => {

        await browser.url(configData.baseURl);
       await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await icdCode.MainMenu();
        await browser.pause(3000);

    });
    for (let i = 0; i < json.length; i++) {

        BillStatusName = json[i]['BillStatusName'] ? json[i]['BillStatusName'] : '';
        Description = json[i]['Description'] ? json[i]['Description'] : '';
        Comments = json[i]['Comments'] ? json[i]['Comments'] : '';
        EditBillStatusName = json[i]['EditBillStatusName'] ? json[i]['EditBillStatusName'] : '';
        EditDescription = json[i]['EditDescription'] ? json[i]['EditDescription'] : '';
        EditComments = json[i]['EditComments'] ? json[i]['EditComments'] : '';


        it('Add Bill Status', async () => {
            await icdCode.AddBillStatus(BillStatusName, Description, Comments);



        });
    }
    it('Filter Bill Status', async () => {
        await icdCode.FilterBillStatus(BillStatusName, Description, Comments)


    });
    it('Edit  Bill Status', async () => {
        await icdCode.EditBillStatus(EditBillStatusName, EditDescription, EditComments);
        await browser.pause(5000);




    });

})


