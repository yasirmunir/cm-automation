const loginPage = require("../pageobjects/LoginPageObject.page");
const PracPage = require("../pageobjects/PracticePageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['Practice'];
let json = utils.sheet_to_json(worksheet);
let PracticeNameData;
let StreetAddress;
let Suite;
let City;
let Zip;
let PhoneNo;
let Extension;
let AltPhoneNo;
let Fax;
let Email;
let NpiNumber;
let TinNumber;
let SignatureTitle;
let LocationName;
let LocationPhoneNo;
let LocationFax;
let LocationEmail;
let LocationStreetAddress;
let LocationSuite;
let LocationCity;
let LocationZip;
let EditPracticeNameData;
let EditStreetAddress;
let EditSuite;
let EditCity;
let EditZip;
let EditPhoneNo;
let EditExtension;
let EditAltPhoneNo;
let EditFax;
let EditEmail;
let EditNpiNumber;
let EditTinNumber;
let EditSignatureTitle;

let list3;

describe('Practice Page', () => {
    it('Login with valid Credentials', async () => {
        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await PracPage.MainMenu();

    });
    for (let i = 0; i < json.length; i++) {

        PracticeNameData= json[i]['PracticeNameData'] ? json[i]['PracticeNameData'] : '';
        StreetAddress = json[i]['StreetAddress'] ? json[i]['StreetAddress'] : '';
        Suite = json[i]['Suite'] ? json[i]['Suite'] : '';
        City = json[i]['City'] ? json[i]['City'] : '';
        Zip = json[i]['Zip'] ? json[i]['Zip'] : '';
        PhoneNo = json[i]['PhoneNo'] ? json[i]['PhoneNo'] : '';
        Extension = json[i]['Extension'] ? json[i]['Extension'] : '';
        AltPhoneNo = json[i]['PhoneNo'] ? json[i]['PhoneNo'] : '';
        Fax = json[i]['Fax'] ? json[i]['Fax'] : '';
        Email = json[i]['Email'] ? json[i]['Email'] : '';
        NpiNumber = json[i]['NpiNumber'] ? json[i]['NpiNumber'] : '';
        TinNumber = json[i]['TinNumber'] ? json[i]['TinNumber'] : '';
        SignatureTitle = json[i]['SignatureTitle'] ? json[i]['SignatureTitle'] : '';
        LocationName = json[i]['LocationName'] ? json[i]['LocationName'] : '';
        LocationPhoneNo = json[i]['LocationPhoneNo'] ? json[i]['LocationPhoneNo'] : '';
        LocationFax = json[i]['LocationFax'] ? json[i]['LocationFax'] : '';
        LocationEmail = json[i]['LocationEmail'] ? json[i]['LocationEmail'] : '';
        LocationStreetAddress = json[i]['LocationStreetAddress'] ? json[i]['LocationStreetAddress'] : '';
        LocationSuite = json[i]['LocationSuite'] ? json[i]['LocationSuite'] : '';
        LocationCity = json[i]['LocationCity'] ? json[i]['LocationCity'] : '';
        LocationZip = json[i]['LocationZip'] ? json[i]['LocationZip'] : '';
        EditPracticeNameData= json[i]['EditPracticeNameData'] ? json[i]['EditPracticeNameData'] : '';
        EditStreetAddress= json[i]['EditStreetAddress'] ? json[i]['EditStreetAddress'] : '';
        EditSuite = json[i]['EditSuite'] ? json[i]['EditSuite'] : '';
        EditCity = json[i]['EditCity'] ? json[i]['EditCity'] : '';
        EditZip = json[i]['EditZip'] ? json[i]['EditZip'] : '';
        EditPhoneNo = json[i]['EditPhoneNo'] ? json[i]['EditPhoneNo'] : '';
        EditExtension = json[i]['EditExtension'] ? json[i]['EditExtension'] : '';
        EditAltPhoneNo = json[i]['EditPhoneNo'] ? json[i]['EditPhoneNo'] : '';
        EditFax = json[i]['EditFax'] ? json[i]['EditFax'] : '';
        EditEmail = json[i]['EditEmail'] ? json[i]['EditEmail'] : '';
        EditNpiNumber = json[i]['EditNpiNumber'] ? json[i]['EditNpiNumber'] : '';
        EditTinNumber = json[i]['EditTinNumber'] ? json[i]['EditTinNumber'] : '';
        EditSignatureTitle = json[i]['EditSignatureTitle'] ? json[i]['EditSignatureTitle'] : '';
        


        it('Add New Practice', async () => {
            await PracPage.AddNewPractice(PracticeNameData, StreetAddress, Suite, City);
            await $("//label[.='State']/following::ng-select").click();

            list = await $$("//div[@role='option']")
            for (let i = 0; i < list.length; i++) {
                const element = await list[i];
                if (await element.getText() === 'CA') {
                    await element.click();
                    break;
                }

            }
            await PracPage.AddPracticeData(Zip, PhoneNo, Extension, AltPhoneNo, Fax, Email, NpiNumber, TinNumber, SignatureTitle);
            await PracPage.AddLocation.scrollIntoView();
            await PracPage.AddLocation.click();
            await PracPage.AddLocationData(LocationName, LocationPhoneNo, LocationFax, LocationEmail, LocationStreetAddress, LocationSuite, LocationCity);
            await $("(//label[.='State'])[3]/following::ng-select").click();
            list = await $$("//div[@role='option']")
            for (let i = 0; i < list.length; i++) {
                const element = await list[i];
                if (await element.getText() === 'CA') {
                    await element.click();
                    break;
                }

            }
            await PracPage.LocationZip.setValue(LocationZip);
            //--------------Select Region-------------------//
            const selectRegion = await $("//select[@formcontrolname='region_id']");
            await selectRegion.selectByVisibleText('II');
            //--------Select Place Of Service--------------------//
            await $("//label[text()=' Place of Service* ']/following::ng-select").click();
            await browser.pause(6000);
            list3 = await $$("//div[@role='option']")
            for (let i = 0; i < list3.length; i++) {
                const element = await list3[i];
                if (await element.getText() === 'Assisted Living Facility') {
                    await element.click();
                    break;
                }

            }
            await PracPage.SelectDays.click();
            await PracPage.SaveAndContinue.click();
            await browser.pause(5000);


        });
    

    it('Filter Practice', async () => {
        await PracPage.FilterPractice(PracticeNameData,"3211468377");

        await browser.pause(5000);

    });


it('Edit Practice', async () => {

    await PracPage.EditNewPractice(EditPracticeNameData,EditStreetAddress);
    
    await $("//label[.='State']/following::ng-select").click();

    list = await $$("//div[@role='option']")
    for (let i = 0; i < list.length; i++) {
        const element = await list[i];
        if (await element.getText() === 'CA') {
            await element.click();
            break;
        }

    }
   await PracPage.EditPracticeData(EditZip, EditPhoneNo, EditExtension,EditAltPhoneNo, EditFax, EditEmail, EditNpiNumber, EditTinNumber, EditSignatureTitle);
    await PracPage.Update.scrollIntoView();
    await PracPage.Update.click();
    
    await browser.pause(10000);
    
});
    }


})
