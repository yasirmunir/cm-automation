const app = require("../pageobjects/Appiontment.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");


describe('Week View Appointment', () => {

    it('Create Week View Appointment', async () => {
        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await app.MainMenu();
        await browser.pause(10000);


        //-------------------------------------select Practice Location-----------------------------------------//
        let PracticeLocationArea = await app.LocationArea


        for (let i = 0; i < PracticeLocationArea.length; i++) {
            const element = await PracticeLocationArea[i];
            if (await element.getText() === '7780 Spencerville Rd ,Lima,OH 45806') {
                await element.click();
                break;
            }

        }

        //----------------------------------------Select Provider----------------------------------------------//
        await app.Provider.click();
        await browser.pause(5000);
        let SelectProvider = await app.ProviderList
        for (let i = 0; i < SelectProvider.length; i++) {
            const element = await SelectProvider[i];
            if (await element.getText() === 'MR Test Provider') {
                await element.click();
                break;
            }

        }
        await app.MonthView.click();
        // await Hbot.PreviousDate.click();
        await browser.pause(8000);

        //---------------------------------------Select Day from  Week---------------------------------------//
        await app.WeekView.click();
        //await Hbot.PreviousDate.click();
        await $("(//div[@class='ng-star-inserted'])[218]").doubleClick();
        await browser.pause(6000);


        await browser.pause(8000);
    });
});
