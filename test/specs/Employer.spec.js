const employer = require("../pageobjects/Employer.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['Employer'];
let json = utils.sheet_to_json(worksheet);
let EmployerName;
let StreetAddress;
let Suite;
let City;
let Zip;
let PhoneNo;
let Extension;
let Email;
let Fax;
let FirstName;
let MiddleName;
let LastName;
let CPPhoneNo;
let CPExtension;
let CPEmail;
let CPFax;
let EditEmployerName;
let EditStreetAddress;
let EditSuite;
let EditCity;
let EditZip;
let EditPhoneNo;
let EditExtension;
let EditEmail;
let EditFax;
let EditFirstName;
let EditMiddleName;
let EditLastName;
let EditCPPhoneNo;
let EditCPExtension;
let EditCPEmail;
let EditCPFax;


describe('Employer Page', () => {
    it('Should Login with valid Credentials', async () => {

        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await employer.MianMenu();
        await browser.pause(3000);

    });
    for (let i = 0; i < json.length; i++) {

        EmployerName = json[i]['EmployerName'] ? json[i]['EmployerName'] : '';
        StreetAddress = json[i]['StreetAddress'] ? json[i]['StreetAddress'] : '';
        Suite = json[i]['Suite'] ? json[i]['Suite'] : '';
        City = json[i]['City'] ? json[i]['City'] : '';
        Zip = json[i]['Zip'] ? json[i]['Zip'] : '';
        PhoneNo = json[i]['PhoneNo'] ? json[i]['PhoneNo'] : '';
        Extension = json[i]['Extension'] ? json[i]['Extension'] : '';
        Email = json[i]['Email'] ? json[i]['Email'] : '';
        Fax = json[i]['Fax'] ? json[i]['Fax'] : '';
        FirstName = json[i]['FirstName'] ? json[i]['FirstName'] : '';
        MiddleName = json[i]['MiddleName'] ? json[i]['MiddleName'] : '';
        LastName = json[i]['LastName'] ? json[i]['LastName'] : '';
        CPPhoneNo = json[i]['CPPhoneNo'] ? json[i]['CPPhoneNo'] : '';
        CPExtension = json[i]['CPExtension'] ? json[i]['CPExtension'] : '';
        CPEmail = json[i]['CPEmail'] ? json[i]['CPEmail'] : '';
        CPFax = json[i]['CPFax'] ? json[i]['CPFax'] : '';
        EditEmployerName = json[i]['EditEmployerName'] ? json[i]['EditEmployerName'] : '';
        EditStreetAddress = json[i]['EditStreetAddress'] ? json[i]['EditStreetAddress'] : '';
        EditSuite = json[i]['EditSuite'] ? json[i]['EditSuite'] : '';
        EditCity = json[i]['EditCity'] ? json[i]['EditCity'] : '';
        EditZip = json[i]['EditZip'] ? json[i]['EditZip'] : '';
        EditPhoneNo = json[i]['EditPhoneNo'] ? json[i]['EditPhoneNo'] : '';
        EditExtension = json[i]['EditExtension'] ? json[i]['EditExtension'] : '';
        EditEmail = json[i]['EditEmail'] ? json[i]['EditEmail'] : '';
        EditFax = json[i]['EditFax'] ? json[i]['EditFax'] : '';
        EditFirstName = json[i]['EditFirstName'] ? json[i]['EditFirstName'] : '';
        EditMiddleName = json[i]['EditMiddleName'] ? json[i]['EditMiddleName'] : '';
        EditLastName = json[i]['EditLastName'] ? json[i]['EditLastName'] : '';
        EditCPPhoneNo = json[i]['EditCPPhoneNo'] ? json[i]['EditCPPhoneNo'] : '';
        EditCPExtension = json[i]['EditCPExtension'] ? json[i]['EditCPExtension'] : '';
        EditCPEmail = json[i]['EditCPEmail'] ? json[i]['EditCPEmail'] : '';
        EditCPFax = json[i]['EditCPFax'] ? json[i]['EditCPFax'] : '';


        it('Add Employer', async () => {


            await employer.AddEmployer(EmployerName, StreetAddress, Suite, City);
            await $("//label[.='State']/following::ng-select").click();

            list = await $$("//div[@role='option']");
            for (let i = 0; i < list.length; i++) {
                const element = await list[i];

                if (await element.getText() === 'CA') {
                    await element.click();
                    break;
                }

            }

            await employer.AddEmployerData(Zip, PhoneNo, Extension, Email, Fax);
            await employer.AddContactPersonData(FirstName, MiddleName, LastName, CPPhoneNo, CPExtension, CPEmail, CPFax);
            await browser.pause(5000);


        });
    }
    it('Filter Employer', async () => {
        await employer.FilterEmployer(EmployerName, StreetAddress, City, "CA");
        await employer.PlusIcon.click();
        await employer.FilterEmployerData(Zip, PhoneNo, Extension, Email, Fax, "Yasir Munir Munir", Suite);
        await browser.pause(5000);
    });

    it('Edit Employer', async () => {
        await employer.Edit.click();
        await employer.EditEmployer(EditEmployerName, EditStreetAddress, EditSuite, EditCity);
        await $("//label[.='State']/following::ng-select").click();

        list = await $$("//div[@role='option']");
        for (let i = 0; i < list.length; i++) {
            const element = await list[i];

            if (await element.getText() === 'CA') {
                await element.click();
                break;
            }

        }
        await employer.EditEmployerData(EditZip, EditPhoneNo, EditExtension, EditEmail, EditFax);
        await employer.EditContactPersonData(EditFirstName, EditMiddleName, EditLastName, EditCPPhoneNo, EditCPExtension, EditCPEmail, EditCPFax);
        await browser.pause(5000);


    });




})
