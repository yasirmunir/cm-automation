const Employment = require("../pageobjects/UserEmploymentType.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['UserEmploymentType'];
let json = utils.sheet_to_json(worksheet);
let EmploymentTypeName;
let Comments;
let EditEmploymentTypeName;
let EditComments;


describe('Employment Type  Page', () => {
    it('User should login with valid credentials', async () => {
        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await Employment.MainMenu();
        await browser.pause(3000);

    });
    for (let i = 0; i < json.length; i++) {

        EmploymentTypeName = json[i]['EmploymentTypeName'] ? json[i]['EmploymentTypeName'] : '';
        Comments = json[i]['Comments'] ? json[i]['Comments'] : '';
        EditEmploymentTypeName = json[i]['EditEmploymentTypeName'] ? json[i]['EditEmploymentTypeName'] : '';
        EditComments= json[i]['EditComments'] ? json[i]['EditComments'] : '';

        it('Add Employment Type', async () => {
            await Employment.AddEmploymentType(EmploymentTypeName, Comments);
            await browser.pause(3000);

        });
        it('Filter Employment Type', async () => {
            await Employment.FilterEmploymentType(EmploymentTypeName, Comments);
            await browser.pause(3000);

        });
        it('Edit User Employment Type', async () => {
            await Employment.EditEmploymentType(EditEmploymentTypeName, EditComments);
            await browser.pause(3000);

        });
    }



});
