const icdCode = require("../pageobjects/PaymentStatus.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['PaymentStatus']
let json = utils.sheet_to_json(worksheet);
let PaymentStatusName;
let Description;
let Comments;
let EditPaymentStausName;
let EditDescription;
let EditComments;
describe('Payment Status Page', () => {
    it('Should Login with valid Credentials', async () => {

        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await icdCode.MainMenu();
        await browser.pause(3000);

    });
    for (let i = 0; i < json.length; i++) {

        PaymentStatusName = json[i]['PaymentStatusName'] ? json[i]['PaymentStatusName'] : '';
        Description = json[i]['Description'] ? json[i]['Description'] : '';
        Comments = json[i]['Comments'] ? json[i]['Comments'] : '';
        EditPaymentStausName = json[i]['EditPaymentStausName'] ? json[i]['EditPaymentStausName'] : '';
        EditDescription = json[i]['EditDescription'] ? json[i]['EditDescription'] : '';
        EditComments = json[i]['EditComments'] ? json[i]['EditComments'] : '';


        it('Add Payment Status', async () => {
            await icdCode.AddPaymentStatus(PaymentStatusName, Description, Comments);



        });
    }
    it('Filter Paymnet Status', async () => {
        await icdCode.FilterPaymentStatus(PaymentStatusName, Description, Comments)


    });
    it('Edit Payment Status', async () => {
        await icdCode.EditPaymentStatus(EditPaymentStausName, EditDescription, EditComments);
        await browser.pause(5000);




    });

})


