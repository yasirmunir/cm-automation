const icdCode = require("../pageobjects/PaymentBy.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['PaidBy']
let json = utils.sheet_to_json(worksheet);
let PaidByNameData;
let Description;
let Comments;
let EditPaidByNameData;
let EditDescription;
let EditComments;
describe('Payment By Page', () => {
    it('Should Login with valid Credentials', async () => {

        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await icdCode.MainMenu();
        await browser.pause(3000);

    });
    for (let i = 0; i < json.length; i++) {

        PaidByNameData= json[i]['PaidByNameData'] ? json[i]['PaidByNameData'] : '';
        Description = json[i]['Description'] ? json[i]['Description'] : '';
        Comments = json[i]['Comments'] ? json[i]['Comments'] : '';
        EditPaidByNameData = json[i]['EditPaidByNameData'] ? json[i]['EditPaidByNameData'] : '';
        EditDescription = json[i]['EditDescription'] ? json[i]['EditDescription'] : '';
        EditComments = json[i]['EditComments'] ? json[i]['EditComments'] : '';


        it('Add Payment By', async () => {
            await icdCode.AddPaymentBy(PaidByNameData, Description, Comments);
            await browser.pause(5000);



        });
    }
    it('Filter Payment By', async () => {
        await icdCode.FilterPaymentBy(PaidByNameData, Description, Comments)
        await browser.pause(5000);


    });
    it('Edit  Payment By', async () => {
        await icdCode.EditPaymentBy(EditPaidByNameData, EditDescription, EditComments);
        await browser.pause(5000);




    });

})


