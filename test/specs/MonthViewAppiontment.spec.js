const app = require("../pageobjects/Appiontment.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");


describe('Month View Appointment', () => {

    it('Create Month View Appointment', async () => {
        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await app.MainMenu();
        await browser.pause(10000);

        
            //-------------------------------------select Practice Location-----------------------------------------//
            let PracticeLocationArea = await app.LocationArea


            for (let i = 0; i < PracticeLocationArea.length; i++) {
                const element = await PracticeLocationArea[i];
                if (await element.getText() ==='7780 Spencerville Rd ,Lima,OH 45806') {
                    await element.click();
                    break;
                }

            }

              //----------------------------------------Select Provider----------------------------------------------//
              await app.Provider.click();
              await browser.pause(5000);
              let SelectProvider = await app.ProviderList
              for (let i = 0; i < SelectProvider.length; i++) {
                  const element = await SelectProvider[i];
                  if (await element.getText() ==='MR Test Provider') {
                      await element.click();
                      break;
                  }
  
              }
              await app.MonthView.click();
             // await Hbot.PreviousDate.click();
              await browser.pause(8000);
              
            //---------------------------------------Select Day from  Month---------------------------------------//

            await app.MonthView.click();
           await app.PreviousDate.click();
            //await browser.pause(6000);
            let SelectMonthDate = await app.CalenderDay
            for (let i = 0; i < SelectMonthDate.length; i++) {
                const element = await SelectMonthDate[i];
                if (await element.getText() == '16') {
                    await element.doubleClick();
                    break;
                }

            }
            await browser.pause(8000);
                        //-----------------------------Create Appointment--------------------------------------------------------//
                        await browser.pause(6000);

                        await app.CaseNo.setValue('3931');
                        await browser.pause(4000);
                        let SelectCase = await app.Case
                        for (let i = 0; i < SelectCase.length; i++) {
                            const element = await SelectCase[i];
                            if (await element.getText() == '3931') {
                                await element.click();
                                break;
                            }
            
                        }
            
                        await browser.pause(9000);
            
                        
                        await app.Comments.setValue("Test");
                        await app.SaveAndContinue.click();
                        await browser.pause(10000);
        
    });
    
});

