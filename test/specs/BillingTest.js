const bill = require("../pageobjects/BillingObject.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['CaseStatus']
let json = utils.sheet_to_json(worksheet);
let CaseStatusName;

describe('Billing Regression Testing', () => {
    it('Login with valid credentials', async () => {
        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await bill.MainMenu();
        await bill.Status.click();
        await browser.pause(6000);
        let BillStatus = await $$("//div[@role='option']")
        for (let i = 0; i < BillStatus.length; i++) {
            const element = await BillStatus[i];
            if (await element.getText() === 'Un Finalized') {
                await element.click();
                break;
            }

        }
        await bill.Billable.click();
        await browser.pause(6000);
        let BillAbleStatus = await $$("//div[@role='option']")
        for (let i = 0; i < BillAbleStatus.length; i++) {
            const element = await BillAbleStatus[i];
            if (await element.getText() === 'Yes') {
                await element.click();
                break;
            }

        }
        await bill.Filter.click();
        await bill.CopyRight.scrollIntoView();
        await browser.pause(10000);
        await bill.Checkbox.click();
        await bill.CreateBill.click();
        await browser.pause(8000);
        //--------------------------------------------------Regression ICD CODE------------------------------------------//

        for( let k=0; k < 250; k++) {

        
        await bill.ICDCode.setValue("ICD");
        await browser.pause(5000);
        let SelectICD = await $$("//span[@class='ng-star-inserted']");
        for (let i = 0; i < SelectICD.length; i++) {
            const element = await SelectICD[i];
            if (await element.getText() === "NoumanICD-test") {
                await element.click();
                break;
            }
        }
        await browser.pause(3000);
        
        await bill.CancelIcon.click();
    }

    await console.log("hello");
        
   
    
});
});
