const firm = require("../pageobjects/Firm.page")
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['AttorneyFirm'];
let json = utils.sheet_to_json(worksheet);
/*
let FirmName;
let LocationName;
let StreetAddress;
let Suite;
let City;
let Zip;
let PhoneNo;
let Extension;
let CellNo;
let Fax;
let Email;
let FirstName;
let MiddleName;
let LastName;
let CPPhoneNo;
let CPExtension;
let CPCellNo;
let CPFax;
let CPEmail;
let Comments;
let State;
let EditFirmName;
*/


describe('Firm Page', () => {

    it('Should Login with valid Credentials', async () => {

        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await firm.MainMenu();
        await browser.pause(3000);


    });
    for (let i = 0; i < json.length; i++) {
        /*

        FirmName = json[i]['FirmName'] ? json[i]['FirmName'] : '';
        LocationName = json[i]['LocationName'] ? json[i]['LocationName'] : '';
        StreetAddress = json[i]['StreetAddress'] ? json[i]['StreetAddress'] : '';
        Suite = json[i]['Suite'] ? json[i]['Suite'] : '';
        City = json[i]['City'] ? json[i]['City'] : '';
        Zip = json[i]['Zip'] ? json[i]['Zip'] : '';
        PhoneNo = json[i]['PhoneNo'] ? json[i]['PhoneNo'] : '';
        Extension = json[i]['Extension'] ? json[i]['Extension'] : '';
        CellNo = json[i]['CellNo'] ? json[i]['CellNo'] : '';
        Fax = json[i]['Fax'] ? json[i]['Fax'] : '';
        Email = json[i]['Email'] ? json[i]['Email'] : '';
        FirstName = json[i]['FirstName'] ? json[i]['FirstName'] : '';
        MiddleName = json[i]['MiddleName'] ? json[i]['MiddleName'] : '';
        LastName = json[i]['LastName'] ? json[i]['LastName'] : '';
        CPPhoneNo = json[i]['CPPhoneNo'] ? json[i]['CPPhoneNo'] : '';
        CPExtension = json[i]['CPExtension'] ? json[i]['CPExtension'] : '';
        CPCellNo = json[i]['CPCellNo'] ? json[i]['CPCellNo'] : '';
        CPFax = json[i]['CPFax'] ? json[i]['CPFax'] : '';
        CPEmail = json[i]['CPEmail'] ? json[i]['CPEmail'] : '';
        Comments = json[i]['Comments'] ? json[i]['Comments'] : '';
        State = json[i]['State'] ? json[i]['State'] : '';
        EditFirmName= json[i]['EditFirmName'] ? json[i]['EditFirmName'] : '';
        */

        it('Add Firm', async () => {
            await browser.pause(3000);
            await firm.AddNew.click();
            await browser.pause(3000);
            await firm.AddFirm(json[i]['FirmName']);
            await firm.AddLocation(json[i]['LocationName'],json[i]['Suite']);
            await firm.StreetAddress.setValue(json[i]['StreetAddress']);
            await browser.pause(3000);
            await $("span.pac-icon.pac-icon-marker").click();
            /*
            await $("(//label[text()='State'])[2]/following::ng-select").click();
            list = await $$("//div[@role='option']");
            for (let i = 0; i < list.length; i++) {
                const element = await list[i];

                if (await element.getText() === 'CA') {
                    await element.click();
                    break;
                }

            }
            */
            await firm.AddLocationData(json[i]['PhoneNo'],json[i]['Extension'], json[i]['CellNo'], json[i]['Fax'], json[i]['Email']);
            await firm.AddContactPersonData(json[i]['FirstName'], json[i]['MiddleName'],json[i]['LastName'], json[i]['CPPhoneNo'], json[i]['CPExtension'], json[i]['CPCellNo'], json[i]['CPFax'], json[i]['CPEmail'], json[i]['Comments']);
           
            await browser.pause(5000);



        });
    }
    /*
    
    it('Filter Firm', async () => {
        await firm.FilterFirm(FirmName, LocationName, StreetAddress, City);
        await firm.PlusIcon.click();
        await browser.pause(2000);
        await firm.FilterData(State, Zip, PhoneNo, Fax, Email);
        FirmName="";
        await browser.pause(5000);

    });
    it('Edit Firm', async () => {
        
        
        await firm.EditFirm(EditFirmName);
        await browser.pause(5000);
         

    });
    */


});
