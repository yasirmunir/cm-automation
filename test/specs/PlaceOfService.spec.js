const icdCode = require("../pageobjects/PlaceOfService.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['PlaceOfService']
let json = utils.sheet_to_json(worksheet);
let CodeData;
let NameData;
let Description;
let Comments;
let EditCodeData;
let EditNameData;
let EditDescription;
let EditComments;
describe('Place Of Service Page', () => {
    it('Should Login with valid Credentials', async () => {

        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await icdCode.MainMenu();
        await console.log(json);
        await browser.pause(3000);

    });
    for (let i = 0; i < json.length; i++) {

        CodeData = json[i]['CodeData'] ? json[i]['CodeData'] : '';
        NameData = json[i]['NameData'] ? json[i]['NameData'] : '';
        Description = json[i]['Description'] ? json[i]['Description'] : '';
        Comments = json[i]['Comments'] ? json[i]['Comments'] : '';
        EditCodeData = json[i]['EditCodeData'] ? json[i]['EditCodeData'] : '';
        EditNameData = json[i]['EditNameData'] ? json[i]['EditNameData'] : '';
        EditDescription = json[i]['EditDescription'] ? json[i]['EditDescription'] : '';
        EditComments = json[i]['EditComments'] ? json[i]['EditComments'] : '';


        it('Add Place Of Service', async () => {
            await icdCode.AddPlaceOfService(CodeData, NameData, Description, Comments);





        });
    }
    it('Filter Place Of Service', async () => {
        await icdCode.FilterPlaceOfService(CodeData, NameData, Description, Comments)




    });
    it('Edit Place of Service', async () => {
        await icdCode.EditPlaceOfService(EditCodeData, EditNameData, EditDescription, EditComments);
        await browser.pause(5000);




    });

})


