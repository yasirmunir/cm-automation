const path1= require('path');
const Hbot = require("../pageobjects/HbotAppiontment.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['ManualAppiontment'];
let json = utils.sheet_to_json(worksheet);
let Location;
let Doctor;
let CaseID;
let Comments;




describe('Hbot Appiontment test', () => {


    for (let i = 0; i < json.length; i++) {

        Location = json[i]['Location'] ? json[i]['Location'] : '';
        Doctor = json[i]['Doctor'] ? json[i]['Doctor'] : '';
        CaseID = json[i]['CaseID'] ? json[i]['CaseID'] : '';
        Comments = json[i]['Comments'] ? json[i]['Comments'] : '';

        it('Should Login with valid Credentials', async () => {

            await browser.url(configData.baseURl);
            await browser.maximizeWindow();
            await loginPage.Login(configData.username, configData.password);
            await Hbot.MainMenu();

            await browser.pause(5000);
            let PracticeLocationArea = await $$("//span[@tooltipclass='table-custom-tooltip']");


            for (let i = 0; i < PracticeLocationArea.length; i++) {
                const element = await PracticeLocationArea[i];
                if (await element.getText() == Location) {
                    await element.click();
                    break;
                }

            }
            await Hbot.Provider.click();
            await browser.pause(4000);
            let SelectProvider = await $$("//span[@tooltipclass='table-custom-tooltip']");


            for (let i = 0; i < SelectProvider.length; i++) {
                const element = await SelectProvider[i];
                if (await element.getText() === Doctor) {
                    await element.click();
                    break;
                }

            }

            await Hbot.MonthView.click();
            await Hbot.PreviousDate.click();
            await browser.pause(6000);
            let SelectMonthDate = await $$("//span[.=contains(@class,'cal-day-number')]");
            for (let i = 0; i < SelectMonthDate.length; i++) {
                const element = await SelectMonthDate[i];
                if (await element.getText() === "13") {
                    await element.doubleClick();
                    break;
                }

            }

            await browser.pause(6000);
            await Hbot.CaseNo.setValue("2518");
            await browser.pause(4000);
            let SelectCase = await $$("//a[@class='ng-star-inserted']");
            for (let i = 0; i < SelectCase.length; i++) {
                const element = await SelectCase[i];
                if (await element.getText() === "2518") {
                    await element.click();
                    break;
                }

            }

            await browser.pause(9000);
            /*
            const AppiontmentType = await $("//label[.='Apt. Type']/following::select");
            await AppiontmentType.selectByVisibleText("FU");
            */
            const selectBillable = await $("//select[@formcontrolname='billable']");
            await selectBillable.selectByVisibleText("Yes");
            await Hbot.Comments.setValue(Comments);
            await Hbot.SaveAndContinue.click();
            await browser.pause(9000);
            await $("(//div[@class='text-truncate'])[10]").click();
            await browser.pause(3000);
            await Hbot.EditEval.click();
            await browser.pause(10000);
            const input =await $("//label[.=' Upload ']");
            
            
            
            const submitButton=await $("//button[.=' Upload Document']");
            const filePath= await path1.join(__dirname,'../testData/pat.pdf');
           const remoteFilePath = await browser.uploadFile(filePath);
           await input.setValue(remoteFilePath);
           await submitButton.click();
           await browser.pause(5000);

            /*
            await browser.pause(3000);
            await Hbot.Evaluation();
            await browser.pause(3000);
            const selectMask = await $("//select[@container='body']");
            await selectMask.selectByVisibleText("Yes");
            const selectPSIEars = await $("(//select[@container='body'])[2]");
            await selectPSIEars.selectByVisibleText("Yes");
            await Hbot.NoteComments.scrollIntoView();
            await Hbot.NoteComments.setValue("Test");
            await browser.pause(4000);
            await $("//input[@placeholder='ICD Codes:']").setValue("ICD");
            let SelectICD = await $$("//span[@class='ng-star-inserted']");
            await browser.pause(4000);

            for (let i = 0; i < SelectICD.length; i++) {
                const element = await SelectICD[i];
                if (await element.getText() === "NoumanICD-test") {
                    await element.click();
                    break;
                }

            }
            await browser.pause(4000);
            await $("//input[@id='mat-chip-list-input-3']").setValue("CPT");
            let SelectCPT = await $$("//span[@class='ng-star-inserted']");
            for (let i = 0; i < SelectCPT.length; i++) {
                const element = await SelectCPT[i];
                if (await element.getText() === "CPT1") {
                    await element.click();
                    break;
                }

            }
            await $("//button[.=' Preview Report ']").scrollIntoView();
            await $("//button[.=' Preview Report ']").click();
            await browser.pause(7000);
            await $("(//button[.=' Finalize Visit '])[2]").click();
            */
            await browser.pause(10000);

        });
    }



});




