const icdCode = require("../pageobjects/FeeType.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['FeeType'];
let json = utils.sheet_to_json(worksheet);
let FeeTypeName;
let Description;
let Comments;
let EditFeeTypeName;
let EditDescription;
let EditComments;
describe('FeeType Page', () => {
    it('Should Login with valid Credentials', async () => {

        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await icdCode.MainMenu();
        await browser.pause(3000);

    });
    for (let i = 0; i < json.length; i++) {

        FeeTypeName = json[i]['FeeTypeName'] ? json[i]['FeeTypeName'] : '';
        Description = json[i]['Description'] ? json[i]['Description'] : '';
        Comments = json[i]['Comments'] ? json[i]['Comments'] : '';
        EditFeeTypeName = json[i]['EditFeeTypeName'] ? json[i]['EditFeeTypeName'] : '';
        EditDescription = json[i]['EditDescription'] ? json[i]['EditDescription'] : '';
        EditComments = json[i]['EditComments'] ? json[i]['EditComments'] : '';


        it('Add FeeType', async () => {
            await icdCode.AddFeeType(FeeTypeName, Description, Comments);



        });
    }
    it('Filter FeeType', async () => {
        await icdCode.FilterFeeType(FeeTypeName, Description, Comments)


    });
    it('Edit FeeType', async () => {
        await icdCode.EditFeeType(EditFeeTypeName, EditDescription, EditComments);
        await browser.pause(5000);




    });

})


