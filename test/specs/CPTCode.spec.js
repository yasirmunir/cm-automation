const icdCode = require("../pageobjects/CPTCode.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['CPTCODE'];
let json = utils.sheet_to_json(worksheet);
let CPTCodeName;
let Description;
let Comments;
let EditCPTCodeName;
let EditDescription;
let EditComments;
describe('CPT Code Page', () => {
    it('Should Login with valid Credentials', async () => {

        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await icdCode.MainMenu();
        await browser.pause(3000);

    }); 
    for (let i = 0; i < json.length; i++) {

        CPTCodeName = json[i]['CPTCodeName'] ? json[i]['CPTCodeName'] : '';
        Description = json[i]['Description'] ? json[i]['Description'] : '';
        Comments = json[i]['Comments'] ? json[i]['Comments'] : '';
        EditCPTCodeName= json[i]['EditCPTCodeName'] ? json[i]['EditCPTCodeName'] : '';
        EditDescription= json[i]['EditDescription'] ? json[i]['EditDescription'] : '';
        EditComments = json[i]['EditComments'] ? json[i]['EditComments'] : '';
        

    it('Add CPT Code', async () => {
        await icdCode.AddCPTCode(CPTCodeName,Description, Comments);
       

        
    });
}
    it('Filter CPTCode', async () => {
        await icdCode.FilterCPTCode(CPTCodeName,Description, Comments)
        
        
    });
    it('Edit CPTCode', async () => {
        await icdCode.EditCPTCode(EditCPTCodeName,EditDescription, EditComments);
        await browser.pause(5000);

       

        
    });
    
})


