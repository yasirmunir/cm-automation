const Hbot = require("../pageobjects/HbotAppiontment.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['HbotWeekAppiontment'];
let json = utils.sheet_to_json(worksheet);
let Location;
let Doctor;
let CaseID;
let Comments;

describe('Hbot Week View Appiontment test', () => {


    for (let i = 0; i < json.length; i++) {

        Location = json[i]['Location'] ? json[i]['Location'] : '';
        Doctor = json[i]['Doctor'] ? json[i]['Doctor'] : '';
        CaseID = json[i]['CaseID'] ? json[i]['CaseID'] : '';
        Comments = json[i]['Comments'] ? json[i]['Comments'] : '';

        it('Should Login with valid Credentials', async () => {

            await browser.url(configData.baseURl);
            await browser.maximizeWindow();
            await loginPage.Login(configData.username, configData.password);
            await Hbot.MainMenu();

            await browser.pause(5000);

            //-------------------------------------select Practice Location-----------------------------------------//
            let PracticeLocationArea = await Hbot.LocationArea


            for (let i = 0; i < PracticeLocationArea.length; i++) {
                const element = await PracticeLocationArea[i];
                if (await element.getText() == Location) {
                    await element.click();
                    break;
                }

            }
            //----------------------------------------Select Provider----------------------------------------------//
            await Hbot.Provider.click();
            await browser.pause(5000);
            let SelectProvider = await Hbot.ProviderList
            for (let i = 0; i < SelectProvider.length; i++) {
                const element = await SelectProvider[i];
                if (await element.getText() === Doctor) {
                    await element.click();
                    break;
                }

            }
            await Hbot.WeekView.click();
            await Hbot.PreviousDate.click();
            await Hbot.PreviousDate.click();
            await Hbot.PreviousDate.click();
            await $("(//div[@class='ng-star-inserted'])[219]").doubleClick();
            await browser.pause(6000);

            await Hbot.CaseNo.setValue(CaseID);
            await browser.pause(4000);
            let SelectCase = await Hbot.Case
            for (let i = 0; i < SelectCase.length; i++) {
                const element = await SelectCase[i];
                if (await element.getText() == CaseID) {
                    await element.click();
                    break;
                }

            }

            await browser.pause(4000);

            const selectBillable = await Hbot.Billable
            await selectBillable.selectByVisibleText("Yes");
            await Hbot.Comments.setValue(Comments);
            await Hbot.SaveAndContinue.click();
            await browser.pause(10000);
            await $("(//div[@class='text-truncate'])[33]").click();
            await browser.pause(4000);
            await Hbot.PatientName.moveTo(30, 60);
            await browser.pause(4000);
            await Hbot.WeekEvaluation.click();



            //-------------------------------------------Start Evaluation Contrdiction Hbot Form------------------------------------------------------//
            await browser.pause(3000);
            await Hbot.Evaluation();
            await browser.pause(3000);
            await Hbot.PSI.setValue("4");
            await Hbot.ATA.setValue("6");
            const selectMask = await Hbot.Mask
            await selectMask.selectByVisibleText("Yes");
            await Hbot.EarPlanes.setValue("Ear");
            await Hbot.TimeStarted.setValue("1234");
            const selectPSIEars = await Hbot.PSIEars
            await selectPSIEars.selectByVisibleText("Yes");
            await Hbot.Time("1234", "4567", "1245", "3456", "6754", "3467");
            await Hbot.TimeData("1234", "2034", "2134", "2734", "1234", "2334", "1234", "2134", "1234", "2134", "1234", "2134", "1234", "2134", "1234", "2134", "1234", "2134");
            await Hbot.NoteComments.scrollIntoView();
            await Hbot.NoteComments.setValue("Test");
            await browser.pause(4000);
            //------------------------------------ADD 6 ICD COdes--------------------------------------------------------//
            await Hbot.ICDCode.setValue("ICD");
            await browser.pause(4000);
            let SelectICDCode = await Hbot.ICDCodeList

            for (let i = 0; i < SelectICDCode.length; i++) {
                const element = await SelectICDCode[i];
                if (await element.getText() === "ICD icd") {
                    await element.click();
                    break;
                }

            }
            //-------------------------2nd ICdCode-----------------------------------------------------------//
            await Hbot.ICDCode.setValue("ICD");
            await browser.pause(4000);
            let SelectICDCode2 = await Hbot.ICDCodeList

            for (let i = 0; i < SelectICDCode2.length; i++) {
                const element = await SelectICDCode2[i];
                if (await element.getText() === "Sidra ICD-10 Code2-ICD-10 Code Description") {
                    await element.click();
                    break;
                }

            }
            //-------------------------3rd ICdCode-----------------------------------------------------------//
            await Hbot.ICDCode.setValue("ICD");
            await browser.pause(4000);
            let SelectICDCode3 = await Hbot.ICDCodeList

            for (let i = 0; i < SelectICDCode3.length; i++) {
                const element = await SelectICDCode3[i];
                if (await element.getText() === "new icd code-asdfdsa") {
                    await element.click();
                    break;
                }

            }
            //-------------------------4th ICdCode-----------------------------------------------------------//
            await Hbot.ICDCode.setValue("ICD");
            await browser.pause(4000);
            let SelectICDCode4 = await Hbot.ICDCodeList

            for (let i = 0; i < SelectICDCode4.length; i++) {
                const element = await SelectICDCode4[i];
                if (await element.getText() === "QA ICD Code-QA Testing") {
                    await element.click();
                    break;
                }

            }
            //-------------------------5th ICdCode-----------------------------------------------------------//
            await Hbot.ICDCode.setValue("ICD");
            await browser.pause(4000);
            let SelectICDCode5 = await Hbot.ICDCodeList

            for (let i = 0; i < SelectICDCode5.length; i++) {
                const element = await SelectICDCode5[i];
                if (await element.getText() === "NoumanICD-test") {
                    await element.click();
                    break;
                }

            }


            await browser.pause(7000);
            //-------------------------------Ist CPT Code------------------------------------------------------------//

            await $("//input[@placeholder='CPT Codes (Maximum 6):']").setValue("CPT");
            await browser.pause(6000);
            let SelectCPTCode = await $$("//span[@class='mat-option-text']/following::span");
            for (let i = 0; i < SelectCPTCode.length; i++) {
                const element = await SelectCPTCode[i];
                if (await element.getText() === "CPT1") {
                    await element.click();
                    break;
                }
            }
            //------------------------------2nd CPT CODE -----------------------------------------------------------//


            await $("//input[@placeholder='CPT Codes (Maximum 6):']").setValue("CPT");
            await browser.pause(3000);
            let SelectCPTCode2 = await $$("//span[@class='mat-option-text']/following::span");
            for (let i = 0; i < SelectCPTCode2.length; i++) {
                const element = await SelectCPTCode2[i];
                if (await element.getText() === "CPT2") {
                    await element.click();
                    break;
                }
            }
            //------------------------------3rd CPT CODE -----------------------------------------------------------//


            await $("//input[@placeholder='CPT Codes (Maximum 6):']").setValue("CPT");
            await browser.pause(3000);
            let SelectCPTCode3 = await $$("//span[@class='mat-option-text']/following::span");
            for (let i = 0; i < SelectCPTCode3.length; i++) {
                const element = await SelectCPTCode3[i];
                if (await element.getText() === "CPT3") {
                    await element.click();
                    break;
                }
            }



            //------------------------------4th CPT CODE -----------------------------------------------------------//


            await $("//input[@placeholder='CPT Codes (Maximum 6):']").setValue("CPT");
            await browser.pause(3000);
            let SelectCPTCode4 = await $$("//span[@class='mat-option-text']/following::span");
            for (let i = 0; i < SelectCPTCode4.length; i++) {
                const element = await SelectCPTCode4[i];
                if (await element.getText() === "CPT4") {
                    await element.click();
                    break;
                }
            }
            //------------------------------5th CPT CODE -----------------------------------------------------------//


            await $("//input[@placeholder='CPT Codes (Maximum 6):']").setValue("CPT");
            await browser.pause(3000);
            let SelectCPTCode5 = await $$("//span[@class='mat-option-text']/following::span");
            for (let i = 0; i < SelectCPTCode5.length; i++) {
                const element = await SelectCPTCode5[i];
                if (await element.getText() === "CPT5") {
                    await element.click();
                    break;
                }
            }
            

            await Hbot.DoctorReviewed.setValue("Very Good Condition");
            await Hbot.ChoosePrevious.click();
            await Hbot.ChooseSignature.click();
            await $("//button[.=' Preview Report ']").scrollIntoView();
            await $("//button[.=' Preview Report ']").click();
            await browser.pause(7000);
            await Hbot.SaveForLater.click();
            await browser.pause(9000);
    
        });
    }
});