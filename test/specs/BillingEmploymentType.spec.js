const icdCode = require("../pageobjects/BillingEmploymentType.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['BillingEmploymentType']
let json = utils.sheet_to_json(worksheet);
let BillingName;
let Comments;
let EditBillingName;
let EditComments;
describe('Billing Employment Type Page', () => {
    it('Should Login with valid Credentials', async () => {

        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await icdCode.MainMenu();
        await browser.pause(3000);
        await console.log(json)

    });
    for (let i = 0; i < json.length; i++) {

        BillingName = json[i]['BillingName'] ? json[i]['BillingName'] : '';
        Comments = json[i]['Comments'] ? json[i]['Comments'] : '';
        EditBillingName = json[i]['EditBillingName'] ? json[i]['EditBillingName'] : '';
        EditComments = json[i]['EditComments'] ? json[i]['EditComments'] : '';


        it('Add Billing Employment Type', async () => {
            await icdCode.AddBillingEmploymentType(BillingName,Comments);
            await browser.pause(5000);

      });
    }
    it('Filter Billing Employment Type', async () => {
        await icdCode.FilterBillingEmploymentType(BillingName,Comments)
        await browser.pause(5000);


    });
    it('Edit Billing Employment Page', async () => {
        await icdCode.EditBillingEmploymentType(EditBillingName,EditComments);
        await browser.pause(5000);




    });

})


