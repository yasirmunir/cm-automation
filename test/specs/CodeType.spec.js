const icdCode = require("../pageobjects/CodeType.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['CodeType'];
let json = utils.sheet_to_json(worksheet);
let CodeTypeName;
let Description;
let Comments;
let EditCodeTypeName;
let EditDescription;
let EditComments;
describe('Code Type Page', () => {
    it('Should Login with valid Credentials', async () => {

        await browser.url(configData.baseURl);
       await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await icdCode.MainMenu();
        await browser.pause(3000);

    }); 
    for (let i = 0; i < json.length; i++) {

        CodeTypeName = json[i]['CodeTypeName'] ? json[i]['CodeTypeName'] : '';
        Description = json[i]['Description'] ? json[i]['Description'] : '';
        Comments = json[i]['Comments'] ? json[i]['Comments'] : '';
        EditCodeTypeName= json[i]['EditCodeTypeName'] ? json[i]['EditCodeTypeName'] : '';
        EditDescription= json[i]['EditDescription'] ? json[i]['EditDescription'] : '';
        EditComments = json[i]['EditComments'] ? json[i]['EditComments'] : '';
        

    it('Add Code Type', async () => {
        await icdCode.AddCodeType(CodeTypeName,Description, Comments);
       

        
    });
}
    it('Filter Code Type', async () => {
        await icdCode.FilterCodeType(CodeTypeName,Description, Comments)
        
        
    });
    it('Edit Code Type', async () => {
        await icdCode.EditCodeType(EditCodeTypeName,EditDescription, EditComments);
        await browser.pause(5000);

       

        
    });
    
})


