const icdCode = require("../pageobjects/VerificationStatus.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['VerificationStatus']
let json = utils.sheet_to_json(worksheet);
let VerificationStatusNameData;
let Description;
let Comments;
let EditVerificationStatusNameData;
let EditDescription;
let EditComments;
describe('Verification Status Page', () => {
    it('Should Login with valid Credentials', async () => {

        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await icdCode.MainMenu();
        await browser.pause(3000);

    });
    for (let i = 0; i < json.length; i++) {

        VerificationStatusNameData = json[i]['VerificationStatusNameData'] ? json[i]['VerificationStatusNameData'] : '';
        Description = json[i]['Description'] ? json[i]['Description'] : '';
        Comments = json[i]['Comments'] ? json[i]['Comments'] : '';
        EditVerificationStatusNameData = json[i]['EditVerificationStatusNameData'] ? json[i]['EditVerificationStatusNameData'] : '';
        EditDescription = json[i]['EditDescription'] ? json[i]['EditDescription'] : '';
        EditComments = json[i]['EditComments'] ? json[i]['EditComments'] : '';


        it('Add Verification Status', async () => {
            await icdCode.AddVerificationStatus(VerificationStatusNameData, Description, Comments);



        });
    }
    it('Filter Verification Status', async () => {
        await icdCode.FilterVerificationStatus(VerificationStatusNameData, Description, Comments)


    });
    it('Edit  Verification Status', async () => {
        await icdCode.EditVerificationStatus(EditVerificationStatusNameData, EditDescription, EditComments);
        await browser.pause(5000);




    });

})


