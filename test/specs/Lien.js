const patient = require("../pageobjects/PatientObject.page");
const personal = require("../pageobjects/PersonalObject.page");
const BasicInfo = require("../pageobjects/BasicInformationObject.page");
const FormFiller = require("../pageobjects/FormFillerObject.object");
const Emergency = require("../pageobjects/EmergencyInfo.page");
const Referral = require("../pageobjects/ReferralsObject.page");
const Insurance = require("../pageobjects/InsuranceObject.page");
const Employer = require("../pageobjects/EmployerObject.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const Accident = require("../pageobjects/AccidentObject.page");
const Injury = require("../pageobjects/InjuryObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['Lien']
let json = utils.sheet_to_json(worksheet);
//---------------------------------------Patient Test Data-------------------------------------//
let FirstName;
let MiddleName;
let LastName;
let SSN;
let HomePhone;
let WorkPhone;
let CellPhone;
let Email;
let Address;
let Suite;
let City;
let State;
let Zip;
//------------------------------Case Info Test Data-------------------------------------//
let PracticeLocation;
let Category;
let PurposeOfVisit;
let CaseType;
let DOA;
//--------------------------Personal Test Data------------------------------------------//
let PersonalSSN;
let Weight;
let Height;
let Inches;
//-----------------------------------Basic Contact Information Test Data-------------------------------//
let BHomePhone;
let BCellPhone;
let BWorkPhone;
let BExtension;
let BStreetAddress;
let BSuite;
let BCity;
let BState;
let BZip;
let BEmail;
let BFax;
//-------------------------------------Form Filler Test Data------------------------------------------//
let FFirstName;
let FMiddleName;
let FLastName;
let FCellPhone;
let FEmail;
let FFax;
let FStreetAddress;
let FSuite;
let FCity;
let FState;
let FZip;
let FRelation;
//------------------------------------Emergency Contact Information Test Data----------------------------------------//
let EFirstName;
let EMiddleName;
let ELastName;
let EDOB;
let EHomePhone;
let ECellPhone;
let EEmail;
let EFax;
let EStreetAddress;
let ESuite;
let ECity;
let EState;
let EZip;
let ERelation;
//----------------------------------- Refferals Test Data------------------------------------------------------------//
let RPFirstName;
let RPMiddleName;
let RPLastName;
let RPClinicName;
let RPEmail;
let RPhoneNo;
let RPExtension;
let RPAddress;
let RPSuite;
let RPCity;
let RPState;
let RPZip;
let PPFirstName;
let PPMiddleName;
let PPLastName;
let PPClinicName;
let PPEmail;
let PPPhoneNo;
let PPExtension;
let PPAddress;
let PPSuite;
let PPCity;
let PPState;
let PPZip;
// ------------------------------------------------------Insurance Test Data---------------------------------------//
let PInsuranceName;
let PMemberID;
let PGroupNo;
let PPriorAuthNo;
let SInsuranceName;
let SPHFirstName;
let SPHMiddleName;
let SPHLastName;
let SPHClaimNo;
let SPHPolicyNo;
let SPHWcbNo;
let SPHPriorAuth;
let TInsuranceName;
let TPHFirstName;
let TPHMiddleName;
let TPHLastName;
let TPHClaimNo;
let TPHPolicyNo;
let TPHWcbNo;
let TPHPriorAuth;
//---------------------------------------------Attorney Test Data------------------------------------------------------//
let FirmName;
let AttorneyName;

//--------------------------------------------Employer Test Data------------------------------------------------//

let PEmployerName;
let PPatientOccupation;
let PHiringDate;
let PFirstName;
let PMiddleName;
let PLastName;



describe('Lien Case Page', () => {
    it('Should Login with valid credentials', async () => {
        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await patient.MainMenu();


    });

    for (let i = 0; i < json.length; i++) {
        //----------------------Patient Info Data---------------------//

        FirstName = json[i]['FirstName'] ? json[i]['FirstName'] : '';
        MiddleName = json[i]['MiddleName'] ? json[i]['MiddleName'] : '';
        LastName = json[i]['LastName'] ? json[i]['LastName'] : '';
        SSN = json[i]['SSN'] ? json[i]['SSN'] : '';
        HomePhone = json[i]['HomePhone'] ? json[i]['HomePhone'] : '';
        WorkPhone = json[i]['WorkPhone'] ? json[i]['WorkPhone'] : '';
        CellPhone = json[i]['CellPhone'] ? json[i]['CellPhone'] : '';
        Email = json[i]['Email'] ? json[i]['Email'] : '';
        Address = json[i]['Address'] ? json[i]['Address'] : '';
        Suite = json[i]['Suite'] ? json[i]['Suite'] : '';
        City = json[i]['City'] ? json[i]['City'] : '';
        State = json[i]['State'] ? json[i]['State'] : '';
        Zip = json[i]['Zip'] ? json[i]['Zip'] : '';
        //---------------Case Info data--------------------------------//
        PracticeLocation = json[i]['PracticeLocation'] ? json[i]['PracticeLocation'] : '';
        Category = json[i]['Category'] ? json[i]['Category'] : '';
        PurposeOfVisit = json[i]['PurposeOfVisit'] ? json[i]['PurposeOfVisit'] : '';
        CaseType = json[i]['CaseType'] ? json[i]['CaseType'] : '';
        DOA = json[i]['DOA'] ? json[i]['DOA'] : '';
        //--------------Personal Data-----------------------------------//
        PersonalSSN = json[i]['PersonalSSN'] ? json[i]['PersonalSSN'] : '';
        Weight = json[i]['Weight'] ? json[i]['Weight'] : '';
        Height = json[i]['Height'] ? json[i]['Height'] : '';
        Inches = json[i]['Inches'] ? json[i]['Inches'] : '';
        //-------------Basic Contact Information  Data-----------------------//
        BHomePhone = json[i]['BHomePhone'] ? json[i]['BHomePhone'] : '';
        BCellPhone = json[i]['BCellPhone'] ? json[i]['BCellPhone'] : '';
        BWorkPhone = json[i]['BWorkPhone'] ? json[i]['BWorkPhone'] : '';
        BExtension = json[i]['BExtension'] ? json[i]['BExtension'] : '';
        BStreetAddress = json[i]['BStreetAddress'] ? json[i]['BStreetAddress'] : '';
        BSuite = json[i]['BSuite'] ? json[i]['BSuite'] : '';
        BCity = json[i]['BCity'] ? json[i]['BCity'] : '';
        BState = json[i]['BState'] ? json[i]['BState'] : '';
        BZip = json[i]['BZip'] ? json[i]['BZip'] : '';
        BEmail = json[i]['BEmail'] ? json[i]['BEmail'] : '';
        BFax = json[i]['BFax'] ? json[i]['BFax'] : '';
        //-------------------------------Form Filler Information Data------------------------------------------//
        FFirstName = json[i]['FFirstName'] ? json[i]['FFirstName'] : '';
        FMiddleName = json[i]['FMiddleName'] ? json[i]['FMiddleName'] : '';
        FLastName = json[i]['FLastName'] ? json[i]['FLastName'] : '';
        FCellPhone = json[i]['FCellPhone'] ? json[i]['FCellPhone'] : '';
        FEmail = json[i]['FEmail'] ? json[i]['FEmail'] : '';
        FFax = json[i]['FFax'] ? json[i]['FFax'] : '';
        FStreetAddress = json[i]['FStreetAddress'] ? json[i]['FStreetAddress'] : '';
        FSuite = json[i]['FSuite'] ? json[i]['FSuite'] : '';
        FCity = json[i]['FCity'] ? json[i]['FCity'] : '';
        FState = json[i]['FState'] ? json[i]['FState'] : '';
        FZip = json[i]['FZip'] ? json[i]['FZip'] : '';
        FRelation = json[i]['FRelation'] ? json[i]['FRelation'] : '';

        //---------------------------------Emergency Conatact Person Data------------------------------------------------//
        EFirstName = json[i]['EFirstName'] ? json[i]['EFirstName'] : '';
        EMiddleName = json[i]['EMiddleName'] ? json[i]['EMiddleName'] : '';
        ELastName = json[i]['ELastName'] ? json[i]['ELastName'] : '';
        EDOB = json[i]['EDOB'] ? json[i]['EDOB'] : '';
        EHomePhone = json[i]['EHomePhone'] ? json[i]['EHomePhone'] : '';
        ECellPhone = json[i]['ECellPhone'] ? json[i]['ECellPhone'] : '';
        EEmail = json[i]['EEmail'] ? json[i]['EEmail'] : '';
        EFax = json[i]['EFax'] ? json[i]['EFax'] : '';
        EStreetAddress = json[i]['EStreetAddress'] ? json[i]['EStreetAddress'] : '';
        ESuite = json[i]['ESuite'] ? json[i]['ESuite'] : '';
        ECity = json[i]['ECity'] ? json[i]['ECity'] : '';
        EState = json[i]['EState'] ? json[i]['EState'] : '';
        EZip = json[i]['EZip'] ? json[i]['EZip'] : '';
        ERelation = json[i]['ERelation'] ? json[i]['ERelation'] : '';
        //------------------------------------Reffrals Data----------------------------------------//
        RPFirstName = json[i]['RPFirstName'] ? json[i]['RPFirstName'] : '';
        RPMiddleName = json[i]['RPMiddleName'] ? json[i]['RPMiddleName'] : '';
        RPLastName = json[i]['RPLastName'] ? json[i]['RPLastName'] : '';
        RPClinicName = json[i]['RPClinicName'] ? json[i]['RPClinicName'] : '';
        RPEmail = json[i]['RPEmail'] ? json[i]['RPEmail'] : '';
        RPhoneNo = json[i]['RPhoneNo'] ? json[i]['RPhoneNo'] : '';
        RPExtension = json[i]['RPExtension'] ? json[i]['RPExtension'] : '';
        RPAddress = json[i]['RPAddress'] ? json[i]['RPAddress'] : '';
        RPSuite = json[i]['RPSuite'] ? json[i]['RPSuite'] : '';
        RPCity = json[i]['RPCity'] ? json[i]['RPCity'] : '';
        RPState = json[i]['RPState'] ? json[i]['RPState'] : '';
        RPZip = json[i]['RPZip'] ? json[i]['RPZip'] : '';
        PPFirstName = json[i]['PPFirstName'] ? json[i]['PPFirstName'] : '';
        PPMiddleName = json[i]['PPMiddleName'] ? json[i]['PPMiddleName'] : '';
        PPLastName = json[i]['PPLastName'] ? json[i]['PPLastName'] : '';
        PPClinicName = json[i]['PPClinicName'] ? json[i]['PPClinicName'] : '';
        PPEmail = json[i]['PPEmail'] ? json[i]['PPEmail'] : '';
        PPPhoneNo = json[i]['PPPhoneNo'] ? json[i]['PPPhoneNo'] : '';
        PPExtension = json[i]['PPExtension'] ? json[i]['PPExtension'] : '';
        PPAddress = json[i]['PPAddress'] ? json[i]['PPAddress'] : '';
        PPSuite = json[i]['PPSuite'] ? json[i]['PPSuite'] : '';
        PPCity = json[i]['PPCity'] ? json[i]['PPCity'] : '';
        PPState = json[i]['PPState'] ? json[i]['PPState'] : '';
        PPZip = json[i]['PPZip'] ? json[i]['PPZip'] : '';
        //----------------------------------Insurance Data--------------------------------------------//
        PInsuranceName = json[i]['PInsuranceName'] ? json[i]['PInsuranceName'] : '';
        PMemberID = json[i]['PMemberID'] ? json[i]['PMemberID'] : '';
        PGroupNo = json[i]['PGroupNo'] ? json[i]['PGroupNo'] : '';
        PPriorAuthNo = json[i]['PPriorAuthNo'] ? json[i]['PPriorAuthNo'] : '';
        NFInsuranceName = json[i]['NFInsuranceName'] ? json[i]['NFInsuranceName'] : '';
        NFPHFirstName = json[i]['NFPHFirstName'] ? json[i]['NFPHFirstName'] : '';
        NFPHMiddleName = json[i]['NFPHMiddleName'] ? json[i]['NFPHMiddleName'] : '';
        NFPHLastName = json[i]['NFPHLastName'] ? json[i]['NFPHLastName'] : '';
        NFPHClaimNo = json[i]['NFPHClaimNo'] ? json[i]['NFPHClaimNo'] : '';
        NFPHPolicyNo = json[i]['NFPHPolicyNo'] ? json[i]['NFPHPolicyNo'] : '';
        NFPHWcbNo = json[i]['NFPHWcbNo'] ? json[i]['NFPHWcbNo'] : '';
        NFPHPriorAuth = json[i]['NFPHPriorAuth'] ? json[i]['NFPHPriorAuth'] : '';
        SInsuranceName = json[i]['SInsuranceName'] ? json[i]['SInsuranceName'] : '';
        SPHFirstName = json[i]['SPHFirstName'] ? json[i]['SPHFirstName'] : '';
        SPHMiddleName = json[i]['SPHMiddleName'] ? json[i]['SPHMiddleName'] : '';
        SPHLastName = json[i]['SPHLastName'] ? json[i]['SPHLastName'] : '';
        SPHClaimNo = json[i]['SPHClaimNo'] ? json[i]['SPHClaimNo'] : '';
        SPHPolicyNo = json[i]['SPHPolicyNo'] ? json[i]['SPHPolicyNo'] : '';
        SPHWcbNo = json[i]['SPHWcbNo'] ? json[i]['SPHWcbNo'] : '';
        SPHPriorAuth = json[i]['SPHPriorAuth'] ? json[i]['SPHPriorAuth'] : '';
        TInsuranceName = json[i]['TInsuranceName'] ? json[i]['TInsuranceName'] : '';
        TPHFirstName = json[i]['TPHFirstName'] ? json[i]['TPHFirstName'] : '';
        TPHMiddleName = json[i]['TPHMiddleName'] ? json[i]['TPHMiddleName'] : '';
        TPHLastName = json[i]['TPHLastName'] ? json[i]['TPHLastName'] : '';
        TPHClaimNo = json[i]['TPHClaimNo'] ? json[i]['TPHClaimNo'] : '';
        TPHPolicyNo = json[i]['TPHPolicyNo'] ? json[i]['TPHPolicyNo'] : '';
        TPHWcbNo = json[i]['TPHWcbNo'] ? json[i]['TPHWcbNo'] : '';
        TPHPriorAuth = json[i]['TPHPriorAuth'] ? json[i]['TPHPriorAuth'] : '';
        //-------------------------------Attorney Data---------------------------------------//
        FirmName = json[i]['FirmName'] ? json[i]['FirmName'] : '';
        AttorneyName = json[i]['AttorneyName'] ? json[i]['AttorneyName'] : '';

        //-----------------------------------Employer Test Data--------------------------------//
        PEmployerName = json[i]['PEmployerName'] ? json[i]['PEmployerName'] : '';
        PPatientOccupation = json[i]['PPatientOccupation'] ? json[i]['PPatientOccupation'] : '';
        PHiringDate = json[i]['PHiringDate'] ? json[i]['PHiringDate'] : '';
        PFirstName = json[i]['PFirstName'] ? json[i]['PFirstName'] : '';
        PMiddleName = json[i]['PMiddleName'] ? json[i]['PMiddleName'] : '';
        PLastName = json[i]['PLastName'] ? json[i]['PLastName'] : '';




        // ------------------------------ADD PATIENT------------------------------//
        it('Add Patient', async () => {
            await patient.AddPatient(FirstName, MiddleName, LastName, "10031988", SSN, HomePhone, WorkPhone, CellPhone, Email, Address, Suite, City);
            const selectState = await $("//select[@container='body']");
            await selectState.selectByVisibleText(State);
            await patient.Zip.setValue(Zip);
            await patient.SaveAndContinue.click();
            await browser.pause(3000);
            await patient.IamNewPatient.click();
            await browser.pause(3000);
            await patient.AddNewCase.click();

        });



        //------------------------CASE INFO----------------------------------------//

        it('Case Info Page', async () => {
            //--------select Practice Location-------------//


            await $('//label[.="Practice*"]/following::ng-select').click();
            await browser.pause(4000);
            let list1 = await $$("//div[@role='option']")
            for (let i = 0; i < list1.length; i++) {
                const element = await list1[i];
                if (await element.getText() === PracticeLocation) {
                    await element.click();
                    break;
                }

            }
            await $("//label[.='Practice*']").click();
            //-----------------select category---------------------------------//
            const selectCategory = await $("//select[@tooltipclass='table-custom-tooltip']");
            await selectCategory.selectByVisibleText(Category);
            await browser.pause(4000);
            //---------------Select Purpose of Visit-------------------------//
            const selectPurposeOfVisit = await $("(//select[@container='body'])[2]");
            await selectPurposeOfVisit.selectByVisibleText(PurposeOfVisit);
            await browser.pause(4000);

            //---------------------select Case Type-------------------------//
            const selectCaseType = await $("(//select[@container='body'])[3]");
            await selectCaseType.selectByVisibleText(CaseType);
            await $("//label[.='DOA * (mm/dd/yyyy)']/following::input").setValue(DOA);
            await $("//button[.='Save & Continue']").click();
            await browser.pause(3000);
            await $("//button[.='Create a New Case']").click();
        });
        //------------------------------ Personal Information Page--------------------------------------//


        it('Personal Information Page', async () => {
            await browser.pause(5000);
            await personal.AddWCPersonalInfo(PersonalSSN, Weight, Height, Inches);

            /*
           await $('//label[.="If yes, What Language?"]/following::ng-select').click();
           await browser.pause(6000);
           let list2 = await $$("//div[@role='option']")
           for (let i = 0; i < list2.length; i++) {
               const element = await list2[i];
               if (await element.getText() === 'Afar') {
                   await element.click();
                   break;
               }
        
           }
           */

            await browser.pause(3000);
            await personal.SaveAndContinue.click();


        });


        //----------------------------- Basic Information Page---------------------------------------------------//
        it('Basic Information Page', async () => {

            await BasicInfo.AddBasicContactInformation(BHomePhone, BCellPhone, BWorkPhone, BExtension, BStreetAddress, BSuite, BCity, BState, BZip, BEmail, BFax);



        });

        //---------------------------------Form Filler Page------------------------------------------------------//
        it('Form Filler Page', async () => {
            await FormFiller.AddFormFillerInfo(FFirstName, FMiddleName, FLastName, FCellPhone, FEmail, FFax, FStreetAddress, FSuite, FCity, FState, FZip);
            const selectRelation = await $("//select[@container='body']");
            await selectRelation.selectByVisibleText(FRelation);
            await FormFiller.EmergencyContactPerson.click();
            await FormFiller.SaveAndContinue.click();
            await browser.pause(6000);


        });


        //-----------------------------Emergency Contact Person Information----------------------------------------------------------//

        it('Emergency Contact Information', async () => {
            await Emergency.AddEmergencyContactInformation(EFirstName, EMiddleName, ELastName, EDOB, EHomePhone, ECellPhone, EEmail, EFax, EStreetAddress, ESuite, ECity, EState, EZip);
            const selectRel = await $("//select[@container='body']");
            await selectRel.selectByVisibleText(ERelation);
            await Emergency.SaveAndContinue.click();
            await browser.pause(6000);

        });
        //----------------------------Referrals Page---------------------------------------------------------------//

        it('Referrals Page', async () => {
            await Referral.AddRefferedPhysician(RPFirstName, RPMiddleName, RPLastName, RPClinicName, RPEmail, RPhoneNo, RPExtension, RPAddress, RPSuite, RPCity);
            const selectState = await $("//select[@container='body']");
            await selectState.selectByVisibleText(RPState);
            await Referral.Zip.setValue(RPZip);
            await Referral.PrimaryPhysician.click();
            await Referral.AddPrimaryPhysician(PPFirstName, PPMiddleName, PPLastName, PPClinicName, PPEmail, PPPhoneNo, PPExtension, PPAddress, PPSuite, PPCity);
            const PrimaryselectState = await $("(//select[@container='body'])[2]");
            await PrimaryselectState.selectByVisibleText(PPState);
            await Referral.PZip.setValue(PPZip);
            await Referral.SaveAndContinue.click();
            await browser.pause(6000);

        });



        // ------------------------------------Select Insurance-----------------------------------------------------------//


        it('Insurance Page', async () => {

            //-----------------------Private Health Insurance-----------------------------------------------------------//

            await Insurance.AddPrivateHealthInsurance();
            await $("//ng-select[@role='listbox']").click();
            await browser.pause(4000);
            let PrivateInsuranceName = await $$("//div[@role='option']")
            for (let i = 0; i < PrivateInsuranceName.length; i++) {
                const element = await PrivateInsuranceName[i];
                if (await element.getText() === PInsuranceName) {
                    await element.click();
                    break;
                }

            }
            await Insurance.AddPrivateHealthInsuranceData(PMemberID, PGroupNo, PPriorAuthNo);

            //-----------------------------------------Secondry Insurance--------------------------------------------------------//

            await $("//h6[.=' Secondary Insurance Information']").scrollIntoView();
            await $("//span[.='Do you have secondary insurance?*']/following::span").click();
            await $("(//span[@class='ng-arrow-wrapper'])[3]").click();
            await browser.pause(6000);
            let SecondryInsuranceName = await $$("//div[@role='option']")
            for (let i = 0; i < SecondryInsuranceName.length; i++) {
                const element = await SecondryInsuranceName[i];
                if (await element.getText() === SInsuranceName) {
                    await element.click();
                    break;
                }

            }



            await $("//label[.='First Name']").scrollIntoView();
            await Insurance.AddDrugSecondryPolicyHolderInformation(SPHFirstName, SPHMiddleName, SPHLastName, SPHClaimNo, SPHPolicyNo, SPHWcbNo, SPHPriorAuth);
            //-----------------------------------------Select Secondry Adjuster--------------------------------------------------------//
            await $("(//span[@class='ng-arrow-wrapper'])[6]").click();
            await browser.pause(5000);

            let SecondryAdjuster = await $$("//div[@role='option']")

            for (let i = 0; i < SecondryAdjuster.length; i++) {
                const element = await SecondryAdjuster[i];
                if (await element.getText() === 'Rajendra  Vakil') {
                    await element.click();
                    break;
                }

            }




            //----------------------------------------Tertiary Insurance Information---------------------------------------------------//


            await Insurance.TertiaryInsurance.click();



            await $("(//span[@class='ng-arrow-wrapper'])[7]").click();
            await browser.pause(6000);
            let TertiaryInsuranceName = await $$("//div[@role='option']")
            for (let i = 0; i < TertiaryInsuranceName.length; i++) {
                const element = await TertiaryInsuranceName[i];
                if (await element.getText() === TInsuranceName) {
                    await element.click();
                    break;
                }

            }


            await browser.pause(6000);
            await $('(//label[.="First Name"])[2]').scrollIntoView();
            await Insurance.AddDrugTertiaryPolicyHolderInformation(TPHFirstName, TPHMiddleName, TPHLastName, TPHClaimNo, TPHPolicyNo, TPHWcbNo, TPHPriorAuth);

            //-----------------------------------------Select Tertiary Adjuster-------------------------------------------//
            await $("(//span[@class='ng-arrow-wrapper'])[10]").click();
            await browser.pause(5000);

            let TertiaryAdjuster = await $$("//div[@role='option']")

            for (let i = 0; i < TertiaryAdjuster.length; i++) {
                const element = await TertiaryAdjuster[i];
                if (await element.getText() === 'Kelly  Linden') {
                    await element.click();
                    break;
                }

            }

            await Insurance.SaveAndContinue.click();
            await browser.pause(5000);
        });
        //-----------------------------------------------Attorney Page--------------------------------------------------//

        it('Attorney Page', async () => {
            //---------------------Select Firm Information----------------------------------------//
            await browser.pause(4000);
            await $("//ng-select[@role='listbox']").click();
            await browser.pause(7000);
            let Firm = await $$("//div[@role='option']")
            for (let i = 0; i < Firm.length; i++) {
                const element = await Firm[i];
                if (await element.getText() === FirmName) {
                    await element.click();
                    break;
                }

            }
            await browser.pause(5000);

            //-------------------------------Select Attorney Information----------------------------------//
            await $("(//ng-select[@role='listbox'])[3]").click();
            await browser.pause(7000);
            let Attorney = await $$("//div[@role='option']")
            for (let i = 0; i < Attorney.length; i++) {
                const element = await Attorney[i];
                if (await element.getText() === AttorneyName) {
                    await element.click();
                    break;
                }

            }
            await $("//button[.='Save & Continue']").click();
            await browser.pause(12000);

        });




        //---------------------------------------------------Employer Information---------------------------------------------------------//


        it('Employer Page', async () => {

            await Employer.CurrentlyEmployed.click();


            //---------------------------Primary Employer Information-----------------------------------------------//

            await $("(//ng-select[@role='listbox'])[2]").click();
            await browser.pause(5000);


            let PrimaryEmployer = await $$("//div[@role='option']")

            for (let i = 0; i < PrimaryEmployer.length; i++) {
                const element = await PrimaryEmployer[i];
                if (await element.getText() === '12312') {
                    await element.click();
                    break;
                }

            }

            await Employer.PatientOccupation.setValue(PPatientOccupation);
            await Employer.HiringDate.setValue(PHiringDate);
            await Employer.LooseEmployment.click();
            await Employer.FirstName.setValue(PFirstName);
            await Employer.MiddleName.setValue(PMiddleName);
            await Employer.LastName.setValue(PLastName);
            await Employer.SaveAndContinue.click();
            await browser.pause(4000);
            await Employer.EmployerSaveAndContinue.click();
            await browser.pause(12000);

        });
    }
    //--------------------------------------------------------Accident Page----------------------------------------------//
    it('Accident Page', async () => {
        await Accident.AddLienAccidentInfo("I Was Dancing");
        await browser.pause(10000);

    });
    //--------------------------------------------- Injury---------------------------------------------------------//

    it('Injury Page', async () => {
        await browser.pause(5000);
        await Injury.AddInjuryInfo();
        await browser.pause(15000);

    });

    //---------------------------------------Marketting-------------------------------------------------------------------//
    it('Marketting Page', async () => {
        await browser.pause(5000);
        const RefferedBy = await $("//select[@container='body']");
        await browser.pause(3000);
        await RefferedBy.selectByVisibleText('Patient');
        await $("//label[.='Enter Name']/following::input").setValue("Yasir");
        const Advertisement = await $("(//select[@container='body'])[2]");
        await Advertisement.selectByVisibleText('News');
        await $("//button[.='Save & Continue']").click();
        await browser.pause(19000);
        await $("//app-case-edit[@class='ng-star-inserted']//div[@class='ng-star-inserted'][normalize-space()='Yes']").click();
        await browser.pause(15000);
        await $("(//span[.='×'])[2]").click();
        await browser.pause(5000);

    });


});


