const desig = require("../pageobjects/Designation.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['Designation'];
let json = utils.sheet_to_json(worksheet);
let DesignationName;
let Comments;
let EditDesignationName;
let EditComments;

describe('Designation Page', () => {
    it('Should Login with valid credentials', async () => {
        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await desig.MainMenu();
        await browser.pause(3000);

    });
    for (let i = 0; i < json.length; i++) {

        DesignationName = json[i]['DesignationName'] ? json[i]['DesignationName'] : '';
        Comments = json[i]['Comments'] ? json[i]['Comments'] : '';
        EditDesignationName = json[i]['EditDesignationName'] ? json[i]['EditDesignationName'] : '';
        EditComments = json[i]['EditComments'] ? json[i]['EditComments'] : '';
    it('Add Designation Functionality', async () => {
        await desig.AddDesignation(DesignationName, Comments);
        //await browser.pause(3000);

    });
    it('Filter Designation Functionality', async () => {
        await desig.FilterDesignation(DesignationName, Comments);
        //await browser.pause(3000);

    });
    it('Edit Designation Functionality', async () => {
        await desig.EditDesignation(EditDesignationName, EditComments);
        // await browser.pause(5000);

    });
}

})
