const icdCode = require("../pageobjects/BillingTitle.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['BillingTitle']
let json = utils.sheet_to_json(worksheet);
let TitleName;
let Comments;
let EditTitleName;
let EditComments;
describe('Billing Title Page', () => {
    it('Should Login with valid Credentials', async () => {

        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await icdCode.MainMenu();
        await browser.pause(3000);
        await console.log(json)

    });
    for (let i = 0; i < json.length; i++) {

        TitleName = json[i]['TitleName'] ? json[i]['TitleName'] : '';
        Comments = json[i]['Comments'] ? json[i]['Comments'] : '';
        EditTitleName = json[i]['EditTitleName'] ? json[i]['EditTitleName'] : '';
        EditComments = json[i]['EditComments'] ? json[i]['EditComments'] : '';


        it('Add Billing Title', async () => {
            await icdCode.AddBillingTitle(TitleName,Comments);

      });
    }
    it('Filter Billing Title', async () => {
        await icdCode.FilterBillingTitle(TitleName,Comments)


    });
    it('Edit Billing Title', async () => {
        await icdCode.EditBillingTitle(EditTitleName,EditComments);
        await browser.pause(5000);

 });

})


