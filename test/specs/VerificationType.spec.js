const icdCode = require("../pageobjects/VerificationType.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['VerificationType'];
let json = utils.sheet_to_json(worksheet);
let VerificationTypeName;
let Description;
let Comments;
let EditVerificationTypeName;
let EditDescription;
let EditComments;
describe('Verification Type Page', () => {
    it('Should Login with valid Credentials', async () => {

        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await icdCode.MainMenu();
        await browser.pause(3000);

    }); 
    for (let i = 0; i < json.length; i++) {

        VerificationTypeName = json[i]['VerificationTypeName'] ? json[i]['VerificationTypeName'] : '';
        Description = json[i]['Description'] ? json[i]['Description'] : '';
        Comments = json[i]['Comments'] ? json[i]['Comments'] : '';
        EditVerificationTypeName= json[i]['EditVerificationTypeName'] ? json[i]['EditVerificationTypeName'] : '';
        EditDescription= json[i]['EditDescription'] ? json[i]['EditDescription'] : '';
        EditComments = json[i]['EditComments'] ? json[i]['EditComments'] : '';
        

    it('Add Verification Type', async () => {
        await icdCode.AddVerificationType(VerificationTypeName,Description, Comments);
       

        
    });
}
    it('Filter Verification Type', async () => {
        await icdCode.FilterVerificationType(VerificationTypeName,Description, Comments)
        
        
    });
    it('Edit Verification Type', async () => {
        await icdCode.EditVerificationType(EditVerificationTypeName,EditDescription, EditComments);
        await browser.pause(5000);

       

        
    });
    
})


