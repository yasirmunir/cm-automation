const icdCode = require("../pageobjects/DenialStatus.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['DenialStatus']
let json = utils.sheet_to_json(worksheet);
let DenialStatusNameData;
let Description;
let Comments;
let EditDenialStatusNameData;
let EditDescription;
let EditComments;
describe('Denial Status Page', () => {
    it('Should Login with valid Credentials', async () => {

        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await icdCode.MainMenu();
        await browser.pause(3000);

    });
    for (let i = 0; i < json.length; i++) {

        DenialStatusNameData = json[i]['DenialStatusNameData'] ? json[i]['DenialStatusNameData'] : '';
        Description = json[i]['Description'] ? json[i]['Description'] : '';
        Comments = json[i]['Comments'] ? json[i]['Comments'] : '';
        EditDenialStatusNameData = json[i]['EditDenialStatusNameData'] ? json[i]['EditDenialStatusNameData'] : '';
        EditDescription = json[i]['EditDescription'] ? json[i]['EditDescription'] : '';
        EditComments = json[i]['EditComments'] ? json[i]['EditComments'] : '';


        it('Add Denial Status', async () => {
            await icdCode.AddDenialStatus(DenialStatusNameData, Description, Comments);



        });
    }
    it('Filter Denial Status', async () => {
        await icdCode.FilterDenialStatus(DenialStatusNameData, Description, Comments)


    });
    it('Edit  Denial Status', async () => {
        await icdCode.EditDenialStatus(EditDenialStatusNameData, EditDescription, EditComments);
        await browser.pause(5000);




    });

})


