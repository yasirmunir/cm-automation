const Fee = require("../pageobjects/FeeSchedule.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");

describe('Fee Schedule Page', () => {

    it('User should login with valid credentials', async () => {
        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await Fee.MainMenu();
        await browser.pause(3000);


    });
    it('Add Fee Schedule', async () => {

        //---------------------------Select Code Type---------------------------------------//


        await Fee.ClickCodeType.click();
        await browser.pause(4000);
        let Code = await Fee.SelectCodeType
        for (let i = 0; i < Code.length; i++) {
            const element = await Code[i];
            if (await element.getText() === "CPT") {
                await element.click();
                break;
            }

        }


        //------------------------------------- Select Code Name-------------------------------------------//
        await Fee.ClickCodeName.click();
        await browser.pause(8000);
        let Code1 = await Fee.SelectCodeName
        for (let i = 0; i < Code1.length; i++) {
            const element = await Code1[i];
            if (await element.getText() === "Test Code 2224 - Test Code Des..") {
                await element.click();
                break;
            }

        }
  
        await Fee.BasePrice.setValue("100");
        await Fee.ExpectedReimbursement.setValue("50");
        await Fee.Units.setValue("40");
        //-----------------------------Select Fee Type----------------------------------------------------//
        await Fee.ClickFeeType.click();
        await browser.pause(4000);
        let Code2 = await Fee.SelectFeeType
        for (let i = 0; i < Code2.length; i++) {
            const element = await Code2[i];
            if (await element.getText() === "Test Fee Name 2118") {
                await element.click();
                break;
            }

        }
        //----------------------------Select Case Type------------------------------------------------//

        await Fee.ClickCaseType.click();
        await browser.pause(4000);
        let Code3 = await Fee.SelectCaseType
        for (let i = 0; i < Code3.length; i++) {
            const element = await Code3[i];
            if (await element.getText() === "CASE 1") {
                await element.click();
                break;
            }


        }
        
        //----------------------------Select Visit Type--------------------------------------------------------//
        await Fee.ClickVisitType.click();
        await browser.pause(4000);
        let Code4 = await Fee.SelectVisitType
        for (let i = 0; i < Code4.length; i++) {
            const element = await Code4[i];
            if (await element.getText() === "3443f") {
                await element.click();
                break;
            }
        }
        //---------------------------Select Provider------------------------------------------------------------------//
        await Fee.ClickProviver.click();
        await browser.pause(4000);
        let Code5 = await Fee.SelectVisitType
        for (let i = 0; i < Code5.length; i++) {
            const element = await Code5[i];
            if (await element.getText() === "Ahmed Elsayed Hassan") {
                await element.click();
                break;
            }
        }
        //---------------------------------------Select Specialty---------------------------------------------------------//

        await Fee.ClickSpecialty.click();
        await browser.pause(4000);
        let Code6 = await Fee.SelectSpecialty
        for (let i = 0; i < Code6.length; i++) {
            const element = await Code6[i];
            if (await element.getText() == "123123123") {
                await element.click();
                break;
            }
        }

        //------------------------------------- Select Modifier----------------------------------------------------------//

        await Fee.ClickModifiers.click();
        await browser.pause(4000);
        let Code7 = await Fee.SelectModifiers
        for (let i = 0; i < Code7.length; i++) {
            const element = await Code7[i];
            if (await element.getText() === "ccc") {
                await element.click();
                break;
            }
        }

        //-----------------------------------Modifier Category 1 ---------------------------------------------------------//

        await Fee.ClickModifierCategoryOne.click();
        await browser.pause(4000);
        let Code8 = await Fee.SelectModifiersCategoryOne
        for (let i = 0; i < Code8.length; i++) {
            const element = await Code8[i];
            if (await element.getText() === "ccc") {
                await element.click();
                break;
            }
        }

        //--------------------------------Modifier Category 2-----------------------------------------------------------//

        await Fee.ClickModifierCategoryTwo.click();
        await browser.pause(4000);
        let Code9 = await Fee.SelectModifiersCategoryTwo
        for (let i = 0; i < Code9.length; i++) {
            const element = await Code9[i];
            if (await element.getText() === "ccc") {
                await element.click();
                break;
            }
        }

        //--------------------------Practice Location--------------------------------------------------------------//

        await Fee.ClickPractice.click();
        await browser.pause(4000);
        let Code10 = await Fee.SelectPractice
        for (let i = 0; i < Code10.length; i++) {
            const element = await Code10[i];
            if (await element.getText() === "CitiMed - Queens - Woodhaven") {
                await element.click();
                break;
            }
        }

        //--------------------------------------Place of Service--------------------------------------------------//

        await Fee.ClickPlaceOfService.click();
        await browser.pause(4000);
        let Code11 = await Fee.SelectPlaceOfService
        for (let i = 0; i < Code11.length; i++) {
            const element = await Code11[i];
            if (await element.getText() === "Ambulance – Land") {
                await element.click();
                break;
            }
        }

        //------------------------------------Region------------------------------------------------------------------//

        await Fee.ClickRegion.click();
        await browser.pause(4000);
        let Code12 = await Fee.SelectRegion
        for (let i = 0; i < Code12.length; i++) {
            const element = await Code12[i];
            if (await element.getText() === "III") {
                await element.click();
                break;
            }
        }

        //----------------------------------------Employer--------------------------------------------------------------//

        await Fee.ClickEmployer.click();
        await browser.pause(4000);
        let Code13 = await Fee.SelectEmployer
        for (let i = 0; i < Code13.length; i++) {
            const element = await Code13[i];
            if (await element.getText() === "Aamir 1002") {
                await element.click();
                break;
            }
        }

        await Fee.EmployerStartDate.setValue("11112020");
        await Fee.EmployerEndDate.setValue("11102021");

        //----------------------------------------------Insurance---------------------------------------------------------//
        await Fee.ClickInsurance.click();
        await browser.pause(4000);
        let Code14 = await Fee.SelectInsurance
        for (let i = 0; i < Code14.length; i++) {
            const element = await Code14[i];
            if (await element.getText() === "Silver275441111") {
                await element.click();
                break;
            }
        }
        await Fee.InsuranceStartDate.setValue("11112020");
        await Fee.InsuranceEndDate.setValue("11102021");
      
        //----------------------------------------------Plan Name-------------------------------------------------------//
        await Fee.ClickPlanName.click();
        await browser.pause(4000);
        let Code15 = await Fee.SelectPlanName
        for (let i = 0; i < Code15.length; i++) {
            const element = await Code15[i];
            if (await element.getText() === "Test Data 1") {
                await element.click();
                break;
            }
        }
        //-------------------------------------Comments--------------------------------------------------//

        await Fee.Comments.setValue("Test Fee Schedule");
        await Fee.SaveAndContinue.click();
        await console.log("Fee Schedule has saved Successfully");
        await browser.pause(5000);

    });
    it('Filter Fee Schedule', async () => {

        //--------------------------------Filter By Code Name-----------------------------------------//

        //await Fee.FilterCodeName.setValue("99201 - Office visit E/M new patient L1");
        //------------------------------Filter By Fee Type--------------------------------------------//
        await Fee.ClickFilterFeeType.click();
        await browser.pause(4000);
        let Code16 = await Fee.FilterSelectFeeType
        for (let i = 0; i < Code16.length; i++) {
            const element = await Code16[i];
            if (await element.getText() === "Test Fee Name 2118") {
                await element.click();
                break;
            }
        }
        //----------------------------------------Filter By Base Price-------------------------------------------//
        await Fee.FilterBasePrice.setValue("1.00");

        await Fee.PlusIcon.click();
        await browser.pause(3000);
        //---------------------------------------Filter By Units----------------------------------------------------//

        await Fee.FilterUnits.setValue("40");

        //-------------------------------------Filter By Modifiers-------------------------------------------//
        /*
        await Fee.FilterClickModifier.click();
        await browser.pause(4000);
        let Code17= await Fee.FilterSelectModifier
       for (let i = 0; i < Code17.length; i++) {
           const element = await Code17[i];
           if (await element.getText() === "ccc") {
               await element.click();
               break;
           }
      }
         */
        //---------------------------------------------Filter By provider--------------------------------------------//

        await Fee.ClickFilterProviderName.click();
        await browser.pause(4000);
        let Code18 = await Fee.FilterSelectProviderName
        for (let i = 0; i < Code18.length; i++) {
            const element = await Code18[i];
            if (await element.getText() === "Ahmed Elsayed Hassan") {
                await element.click();
                break;
            }
        }

        //-----------------------------------------Filter By Specialty-------------------------------------------------//


        await Fee.ClickFilterSpecialty.click();
        await browser.pause(4000);
        let Code19 = await Fee.FilterSelectSpecialty
        for (let i = 0; i < Code19.length; i++) {
            const element = await Code19[i];
            if (await element.getText() == "123123123") {
                await element.click();
                break;

            }
        }
        //------------------------------------Filter By Practice Location--------------------------------------------------//

        await Fee.ClickFilterPracticeLocation.click();
        await browser.pause(4000);
        let Code20 = await Fee.FilterSelectPracticeLocation
        for (let i = 0; i < Code20.length; i++) {
            const element = await Code20[i];
            if (await element.getText() === "CitiMed - Queens - Woodhaven") {
                await element.click();
                break;

            }
        }

        //-------------------------Filter By Employer-----------------------------------------------------------//

/*
        await Fee.ClickFilterEmployer.click();
        await browser.pause(4000);
        let Code21 = await Fee.FilterSelectEmployer
        for (let i = 0; i < Code21.length; i++) {
            const element = await Code21[i];
            if (await element.getText() === "Aamir 1002") {
                await element.click();
                break;

            }
        }
        */
        
        await Fee.Filter.click();
        await browser.pause(3000);
       

    });
    

    it('Edit Fee schedule', async () => {

        await Fee.Edit.click();
        await browser.pause(6000);
        await $("(//span[.='×'])[19]").click();
        
        /*
         await Fee.BasePrice.clearValue();
         await Fee.BasePrice.setValue("100");
         await Fee.ExpectedReimbursement.clearValue();
         await Fee.ExpectedReimbursement.setValue("50");
         await Fee.Units.clearValue();
         await Fee.Units.setValue("40");
         await Fee.SaveAndContinue.click();
         */
         
 
        await browser.pause(10000);
        
    });


});
