const icdCode = require("../pageobjects/PaymentType.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['PaymentType'];
let json = utils.sheet_to_json(worksheet);
let PaymentTypeName;
let Description;
let Comments;
let EditPaymentTypeName;
let EditDescription;
let EditComments;
describe('Payment Type Page', () => {
it('Should Login with valid Credentials', async () => {

await browser.url(configData.baseURl);
await browser.maximizeWindow();
await loginPage.Login(configData.username, configData.password);
await icdCode.MainMenu();
await browser.pause(3000);

});
for (let i = 0; i < json.length; i++) {

PaymentTypeName = json[i]['PaymentTypeName'] ? json[i]['PaymentTypeName'] : '';
Description = json[i]['Description'] ? json[i]['Description'] : '';
Comments = json[i]['Comments'] ? json[i]['Comments'] : '';
EditPaymentTypeName = json[i]['EditPaymentTypeName'] ? json[i]['EditPaymentTypeName'] : '';
EditDescription = json[i]['EditDescription'] ? json[i]['EditDescription'] : '';
EditComments = json[i]['EditComments'] ? json[i]['EditComments'] : '';


it('Add Payment Type', async () => {
await icdCode.AddPaymentType(PaymentTypeName, Description, Comments);



});
}
it('Filter Payment Type', async () => {
await icdCode.FilterPaymentType(PaymentTypeName, Description, Comments)


});
it('Edit Payment Type', async () => {
await icdCode.EditPaymentType(EditPaymentTypeName, EditDescription, EditComments);
await browser.pause(5000);




});

})


