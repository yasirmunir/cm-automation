const Dept = require("../pageobjects/Department.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['Department'];
let json = utils.sheet_to_json(worksheet);
let DepartmentName;
let Comments;
let EditDepartmentName;
let EditComments;
describe('Deparment Page', () => {
    it('User should login with valid credentials', async () => {
        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await Dept.MainMenu();
        await browser.pause(3000);
    });

    for (let i = 0; i < json.length; i++) {

        DepartmentName = json[i]['DepartmentName'] ? json[i]['DepartmentName'] : '';
        Comments = json[i]['Comments'] ? json[i]['Comments'] : '';
        EditDepartmentName = json[i]['EditDepartmentName'] ? json[i]['EditDepartmentName'] : '';
        EditComments = json[i]['EditComments'] ? json[i]['EditComments'] : '';

        it('Add Department', async () => {
            await Dept.AddDepartment(DepartmentName, Comments);
            // await browser.pause(3000);

        });

        it('Filter Department', async () => {
            await Dept.FilterDepartment(DepartmentName, Comments);
            // await browser.pause(3000);

        });
        it('Edit Department', async () => {
            await Dept.EditDepartment(EditDepartmentName, EditComments)
            //  await browser.pause(3000);


        });
    }

})
