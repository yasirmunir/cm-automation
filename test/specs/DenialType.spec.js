const icdCode = require("../pageobjects/DenialType.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['DenialType']
let json = utils.sheet_to_json(worksheet);
let DenialTypeName;
let Description;
let Comments;
let EditDenialTypeName;
let EditDescription;
let EditComments;
describe('Denial Type Page', () => {
    it('Should Login with valid Credentials', async () => {

        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await icdCode.MainMenu();
        await browser.pause(3000);

    });
    for (let i = 0; i < json.length; i++) {

        DenialTypeName = json[i]['DenialTypeName'] ? json[i]['DenialTypeName'] : '';
        Description = json[i]['Description'] ? json[i]['Description'] : '';
        Comments = json[i]['Comments'] ? json[i]['Comments'] : '';
        EditDenialTypeName = json[i]['EditDenialTypeName'] ? json[i]['EditDenialTypeName'] : '';
        EditDescription = json[i]['EditDescription'] ? json[i]['EditDescription'] : '';
        EditComments = json[i]['EditComments'] ? json[i]['EditComments'] : '';


        it('Add Denial Type', async () => {
            await icdCode.AddDenialType(DenialTypeName, Description, Comments);



        });
    }
    it('Filter Denial Type', async () => {
        await icdCode.FilterDenialType(DenialTypeName, Description, Comments)


    });
    it('Edit  Denial Type', async () => {
        await icdCode.EditDenialType(EditDenialTypeName, EditDescription, EditComments);
        await browser.pause(5000);




    });

})


