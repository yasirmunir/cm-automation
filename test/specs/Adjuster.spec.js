const Adjuster = require("../pageobjects/AdjusterPageObject.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/Data.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['AdjusterInformation'];
let json = utils.sheet_to_json(worksheet);
/*
let FirstName;
let MiddleName;
let LastName;
let StreetAddress;
let Suite;
let City;
let Zip;
let PhoneNumber;
let Extension;
let CellNo;
let Fax;
let Email;
let Comments;
let InsuranceName;
let AdjusterName;
let EditFirstName;
let EditMiddleName;
let EditLastName;
let EditStreetAddress;
let EditSuite;
let EditCity;
let EditZip;
let EditPhoneNumber;
let EditExtension;
let EditCellNo;
let EditFax;
let EditEmail;
let EditComments;
*/


describe('Adjsuter Information Page', () => {

    it('Should Login with valid Credentials', async () => {

       await browser.url(configData.baseURl);
       await browser.maximizeWindow();
       await loginPage.Login(configData.username, configData.password);
       await Adjuster.MainMenu();
    });

    for (let i = 0; i < json.length; i++) {
        /*

        FirstName = json[i]['FirstName'] ? json[i]['FirstName'] : '';
        MiddleName = json[i]['MiddleName'] ? json[i]['MiddleName'] : '';
        LastName = json[i]['LastName'] ? json[i]['LastName'] : '';
        StreetAddress = json[i]['StreetAddress'] ? json[i]['StreetAddress'] : '';
        Suite = json[i]['Suite'] ? json[i]['Suite'] : '';
        City = json[i]['City'] ? json[i]['City'] : '';
        Zip = json[i]['Zip'] ? json[i]['Zip'] : '';
        PhoneNumber = json[i]['PhoneNumber'] ? json[i]['PhoneNumber'] : '';
        Extension = json[i]['Extension'] ? json[i]['Extension'] : '';
        CellNo = json[i]['CellNo'] ? json[i]['CellNo'] : '';
        Fax = json[i]['Fax'] ? json[i]['Fax'] : '';
        Email = json[i]['Email'] ? json[i]['Email'] : '';
        Comments = json[i]['Comments'] ? json[i]['Comments'] : '';
        InsuranceName = json[i]['InsuranceName'] ? json[i]['InsuranceName'] : '';
        AdjusterName = json[i]['AdjusterName'] ? json[i]['AdjusterName'] : '';
        EditFirstName = json[i]['EditFirstName'] ? json[i]['EditFirstName'] : '';
        EditMiddleName = json[i]['EditMiddleName'] ? json[i]['EditMiddleName'] : '';
        EditLastName = json[i]['EditLastName'] ? json[i]['EditLastName'] : '';
        EditStreetAddress = json[i]['EditStreetAddress'] ? json[i]['EditStreetAddress'] : '';
        EditSuite = json[i]['EditSuite'] ? json[i]['EditSuite'] : '';
        EditCity = json[i]['EditCity'] ? json[i]['EditCity'] : '';
        EditZip = json[i]['EditZip'] ? json[i]['EditZip'] : '';
        EditPhoneNumber = json[i]['EditPhoneNumber'] ? json[i]['EditPhoneNumber'] : '';
        EditExtension = json[i]['EditExtension'] ? json[i]['EditExtension'] : '';
        EditCellNo = json[i]['EditCellNo'] ? json[i]['EditCellNo'] : '';
        EditFax = json[i]['EditFax'] ? json[i]['EditFax'] : '';
        EditEmail = json[i]['EditEmail'] ? json[i]['EditEmail'] : '';
        EditComments = json[i]['EditComments'] ? json[i]['EditComments'] : '';
   */

        it('Add Adjuster Information', async () => {
            await browser.pause(3000);
            await Adjuster.AddNew.click();

            await Adjuster.SelectAdjuster.click();
            await browser.pause(3000);
            list = await $$("//div[@role='option']/following::span")
            

            for (let i = 0; i < list.length; i++) {
                const element = await list[i];
                if (await element.getText() === 'insur') {
                    await element.click();
                    break;
                }
            }

            await Adjuster.AddAdjusterInformation(json[i]['FirstName'], json[i]['MiddleName'], json[i]['LastName'],  json[i]['Suite']);
            await Adjuster.StreetAddress.setValue(json[i]['StreetAddress']);
            await browser.pause(4000);
            await $("span.pac-icon.pac-icon-marker").click();
            /*
            await $("//label[.='State']/following::ng-select").click();
            await browser.pause(3000);

            list = await $$("//div[@role='option']")
            for (let i = 0; i < list.length; i++) {
                const element = await list[i];
                if (await element.getText() === 'CA') {
                    await element.click();
                    break;
                }

            }*/
            await Adjuster.AddAdjusterData( json[i]['PhoneNumber'], json[i]['Extension'], json[i]['CellNo'], json[i]['Fax'], json[i]['Email'], json[i]['Comments'])
            await browser.pause(5000);



        });
    }
    /*
    it('Filter Adjuster', async () => {
        await Adjuster.FilterAdjuster(InsuranceName, AdjusterName, PhoneNumber, Fax, Email);
        await browser.pause(5000);

    });
    it('Edit Adjuster', async () => {
        await Adjuster.Edit.click();

        await Adjuster.EditAdjusterInformation(EditFirstName, EditMiddleName, EditLastName, EditStreetAddress, EditSuite, EditCity);
        await $("//label[.='State']/following::ng-select").click();
        await browser.pause(3000);

        list = await $$("//div[@role='option']")
        for (let i = 0; i < list.length; i++) {
            const element = await list[i];
            if (await element.getText() === 'CA') {
                await element.click();
                break;
            }

        }
        await Adjuster.EditAdjusterData(EditZip, EditPhoneNumber, EditExtension, EditCellNo, EditFax, EditEmail, EditComments)
        await browser.pause(5000);


    });
*/

})
