const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const visitPage = require("../pageobjects/VisitPageObject.page");

const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/Data.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['VisitType'];
let json = utils.sheet_to_json(worksheet);






describe('Visit Type Page', () => {

    it('Login with Valid Credentials', async () => {
        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await browser.pause(5000);
        await visitPage.MainPage();
    });

    for (let i = 0; i < json.length; i++) {

        it('Add New Visit', async () => {
            
            await browser.pause(2000);
            await visitPage.AddVisitType(json[i]['VisitTypeName'], json[i]['Comments']);
            await browser.pause(2000);
            

           await console.log(json);
            
            
            

        });
        

    
          
    
    it('Filter Visit Type',async () => {
        await visitPage.FilterVisitType(json[i]['VisitTypeName'], json[i]['Comments']);
        await browser.pause(2000);

    });
    it('Edit Visit Type', async () => {

       await visitPage.EditVisitType(json[i]['EditVisitTypeName'], json[i]['EditComments'])
       await browser.pause(5000);
       


    });



}


});


