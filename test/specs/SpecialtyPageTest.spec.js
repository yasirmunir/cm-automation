const Specialty = require("../pageobjects/SpecialtyPageObject.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/Data.xlsx";
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['Specialty'];
let json = utils.sheet_to_json(worksheet);
let SpecialtyName;
let TimeSlot;
let OverBooking;
let Comments;
let EditSpecialtyName;
let EditTimeSlot;
let EditOverBooking;
let EditComments;

describe('Specialty Page', () => {
    it('Should Login with valid Credentials', async () => {


        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await Specialty.MainPage();
    });

    for (let i = 0; i < json.length; i++) {

        SpecialtyName = json[i]['SpecialtyName'] ? json[i]['SpecialtyName'] : '';
        TimeSlot = json[i]['TimeSlot'] ? json[i]['TimeSlot'] : '';
        OverBooking = json[i]['OverBooking'] ? json[i]['OverBooking'] : '';
        Comments = json[i]['Comments'] ? json[i]['Comments'] : '';
        EditSpecialtyName = json[i]['EditSpecialtyName'] ? json[i]['EditSpecialtyName'] : '';
        EditTimeSlot = json[i]['EditTimeSlot'] ? json[i]['EditTimeSlot'] : '';
        EditOverBooking = json[i]['EditOverBooking'] ? json[i]['EditOverBooking'] : '';
        EditComments = json[i]['EditComments'] ? json[i]['EditComments'] : '';
        it('Add New Specialty', async () => {
            await Specialty.AddSpecialtyName(SpecialtyName);
            //--------------------- Select Visit Type From DropDown-------------------//
            await $("//label[.='Visit Types*']/following::ng-select").click();
            browser.pause(6000);

            let list = await $$("//div[@role='option']")
            for (let i = 0; i < list.length; i++) {
                const element = await list[i];
                if (await element.getText() === 'aa') {
                    await element.click();
                    break;

                }

            }

            await Specialty.AddSpecialtyData(TimeSlot, OverBooking, Comments);
            // ----------To Add Multiple Set of Data from excel file--------------------//
            /*
            SpecialtyName="";
            TimeSlot="";
            OverBooking="";
            Comments="";
            list="";
            element="";
            */
       });

    }

    it('Filter Specialty',async () => {
        await Specialty.FilterSpecialty(SpecialtyName, TimeSlot, OverBooking);
    });
    it('Edit Specialty', async () => {
        await Specialty.EditSpecialtyName(EditSpecialtyName);
        await $("//label[.='Visit Types*']/following::ng-select").click();

        list = await $$("//div[@role='option']")
        for (let i = 0; i < list.length; i++) {
            const element = await list[i];
            await console.log(element.getText());
            if(await element.getText() === 'Tes322') {
                await element.click();
                break;

            }

        }
        await Specialty.EditSpecialtyData(EditTimeSlot, EditOverBooking, EditComments)
        await browser.pause(5000);


    });

});




