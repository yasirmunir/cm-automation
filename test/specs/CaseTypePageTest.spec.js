const CaseType = require("../pageobjects/CaseTypePageObject.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/Data.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['CaseType'];
let json = utils.sheet_to_json(worksheet);
let CaseTypeName;
let NoOfDays;
let Description;
let Comments;
let EditCaseTypeName;
let EditNoOfDays;
let EditDescription;
let EditComments;



describe('Case Type Page', () => {
    it('Should Login with valid Credentials', async () => {

        await browser.url(configData.baseURl);
       await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await CaseType.MainMenu();
    });

    for (let i = 0; i < json.length; i++) {

        CaseTypeName = json[i]['CaseTypeName'] ? json[i]['CaseTypeName'] : '';
        NoOfDays = json[i]['NoOfDays'] ? json[i]['NoOfDays'] : '';
        Description = json[i]['Description'] ? json[i]['Description'] : '';
        Comments = json[i]['Comments'] ? json[i]['Comments'] : '';
        EditCaseTypeName = json[i]['EditCaseTypeName'] ? json[i]['EditCaseTypeName'] : '';
        EditNoOfDays = json[i]['EditNoOfDays'] ? json[i]['EditNoOfDays'] : '';
        EditDescription = json[i]['EditDescription'] ? json[i]['EditDescription'] : '';
        EditComments = json[i]['EditComments'] ? json[i]['EditComments'] : '';

        it('Add New Case Type', async () => {
            await CaseType.AddCaseType(CaseTypeName, NoOfDays, Description, Comments);


            
            CaseTypeName="";
            NoOfDays="";
            Description="";
            Comments="";
            


        });


    }
    /*
    it('Filter Case Type', async () => {
        await CaseType.FilterCaseType(CaseTypeName, NoOfDays, Description, Comments);

    });
    it('Edit Case Type', async () => {
        await CaseType.EditCaseType(EditCaseTypeName, EditNoOfDays, EditDescription, EditComments);
    });
*/


});




