const Employment = require("../pageobjects/UserEmploymentBy.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['UserEmploymentBy'];
let json = utils.sheet_to_json(worksheet);
let EmploymentByName;
let Comments;
let EditEmploymentByName;
let EditComments;



describe('Employment By Page', () => {

    it('User should login with valid credentials', async () => {
        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await Employment.MainMenu();
        await browser.pause(3000);

    });

    for (let i = 0; i < json.length; i++) {

        EmploymentByName = json[i]['EmploymentByName'] ? json[i]['EmploymentByName'] : '';
        Comments = json[i]['Comments'] ? json[i]['Comments'] : '';
        EditEmploymentByName = json[i]['EditEmploymentByName'] ? json[i]['EditEmploymentByName'] : '';
        EditComments = json[i]['EditComments'] ? json[i]['EditComments'] : '';



        it('Add Employment By', async () => {
            await Employment.AddEmploymentBy(EmploymentByName, Comments);
            //await browser.pause(3000);

        });
        it('Filter Employment By', async () => {
            await Employment.FilterEmploymentBy(EmploymentByName, Comments);
           // await browser.pause(3000);

        });
        it('Edit Employment By', async () => {
            await Employment.EditEmploymentBy(EditEmploymentByName, EditComments);
           // await browser.pause(3000);

        });
    }

});
