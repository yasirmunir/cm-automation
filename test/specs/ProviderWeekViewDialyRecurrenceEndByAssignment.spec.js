const assignment = require("../pageobjects/ProviderAssignment.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['Assignment'];
let json = utils.sheet_to_json(worksheet);
let PracticeLocationName;
let ProviderName;
let CalenderDate;
let CalenderEndByDate;


describe('Provider Week View Dialy Recurrence End By Assignment', () => {

    it('User should login with valid credentials', async () => {
        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await assignment.MainMenu();
        await browser.pause(3000);


    });

    for (let i = 0; i < json.length; i++) {

        PracticeLocationName = json[i]['PracticeLocationName'] ? json[i]['PracticeLocationName'] : '';
        ProviderName = json[i]['ProviderName'] ? json[i]['ProviderName'] : '';
        CalenderDate = json[i]['CalenderDate'] ? json[i]['CalenderDate'] : '';
        CalenderEndByDate = json[i]['CalenderEndByDate'] ? json[i]['CalenderEndByDate'] : '';


        it('Add Provider Week View Dialy Recurrence End By Assignment', async () => {


            await browser.pause(8000);
            let Location = await assignment.PracticeLocation
            for (let i = 0; i < Location.length; i++) {
                const element = await Location[i];
                if (await element.getText() === PracticeLocationName) {
                    await element.click();
                    break;
                }

            }
            await assignment.ClickProviderUser.click();
            await browser.pause(5000);
            let ProviderUser = await assignment.ProviderList
            for (let i = 0; i < ProviderUser.length; i++) {
                const element = await ProviderUser[i];
                if (await element.getText() === ProviderName) {
                    await element.click();
                    break;
                }

            }

            await assignment.Week.click();
            await browser.pause(3000);
            await assignment.WeekDay.doubleClick();
            await browser.pause(10000);
            await assignment.Recurrence.click();
            await assignment.RangeOfRecurrence.click();
            await assignment.EndBy.click();
            await assignment.ClickEndByCalender.click();
            await browser.pause(4000);
            let CalenderMonth = await assignment.EndByDate

            for (let i = 0; i < CalenderMonth.length; i++) {
                const element = await CalenderMonth[i];
                if (await element.getText() == CalenderEndByDate) {
                    await element.click();
                    break;
                }

            }
            //assignment.EndAfterNumber.setValue()
            await assignment.SaveAssignment.click();
            //---------------------------------------Update Assignment---------------------------------------------//
            await assignment.ClickProviderWeekViewAssignment.click();
            await browser.pause(5000);
            await assignment.EditAssignment.click();
            await browser.pause(5000);
            await assignment.StartTime.click();
            await assignment.StartTimeInput.setValue("06:00 PM");
            await assignment.EndTime.click();
            await assignment.EndTimeInput.setValue("06:30 PM");
            await $("//label[.='Start Date*']").click();
            await assignment.UpdateAssignment.click();
            await browser.pause(5000);






            //-----------------------------------------------Delete Assignment---------------------------------------------//
            await assignment.ClickProviderWeekViewAssignment.click();
            await assignment.DeleteButton.click();
            await assignment.AllSubSequentAssignment.click();
            await browser.pause(5000);
            await assignment.DeleteAssignment.click();


            await browser.pause(10000);







        });
    }




})
