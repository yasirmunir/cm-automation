const assignment = require("../pageobjects/ProviderAssignment.page");
const loginPage = require("../pageobjects/LoginPageObject.page");
const configData = require("../testData/config.page");
const xlsx = require('xlsx');
const utils = xlsx.utils;
const path = "test/testData/CaseTypeData.xlsx"
let workbook = xlsx.readFile(path);
let worksheet = workbook.Sheets['SpecialtyAssignment'];
let json = utils.sheet_to_json(worksheet);
let PracticeLocationName;
let SpecialtyName;
let CalenderDate;
let CalenderEndByDate;


describe('Specialty Month View Monthly Recurrence End By Assignment', () => {

    it('User should login with valid credentials', async () => {
        await browser.url(configData.baseURl);
        await browser.maximizeWindow();
        await loginPage.Login(configData.username, configData.password);
        await assignment.SpecialtyMainMenu();
        await browser.pause(6000);


    });
    for (let i = 0; i < json.length; i++) {

        PracticeLocationName = json[i]['PracticeLocationName'] ? json[i]['PracticeLocationName'] : '';
        SpecialtyName = json[i]['SpecialtyName'] ? json[i]['SpecialtyName'] : '';
        CalenderDate = json[i]['CalenderDate'] ? json[i]['CalenderDate'] : '';
        CalenderEndByDate = json[i]['CalenderEndByDate'] ? json[i]['CalenderEndByDate'] : '';

        it('Add Specialty Month View Monthly Recurrence End By Assignment', async () => {


            await assignment.SpecialtyPracticeLocationButton.click();
            await browser.pause(10000);
            let Location = await assignment.SpecialtyLocation
            for (let i = 0; i < Location.length; i++) {
                const element = await Location[i];
                if (await element.getText() === PracticeLocationName) {
                    await element.click();
                    break;
                }

            }
            await assignment.Specialty.click();
            await browser.pause(5000);

            let SpecialtyUser = await assignment.ChooseSpecialty
            for (let i = 0; i < SpecialtyUser.length; i++) {
                const element = await SpecialtyUser[i];
                if (await element.getText() === SpecialtyName) {
                    await element.click();
                    break;
                }

            }
            await browser.pause(4000);
            let SpecialtyMonth = await assignment.SpecialtyMonthDate
            for (let i = 0; i < SpecialtyMonth.length; i++) {
                const element = await SpecialtyMonth[i];
                if (await element.getText() == CalenderDate) {
                    await element.doubleClick();
                    break;
                }


            }
            await browser.pause(10000);
            await assignment.DoNotAssign.click();
            await assignment.SpecialtyRecurrence.click();
            const WeeklyRecurrence = await assignment.SpecialtyRepeatEvery
            await WeeklyRecurrence.selectByVisibleText("Monthly");
            await assignment.SpecialtySunday.click();
            await assignment.SpecialtyMonday.click();
            await assignment.SpecialtyTuesday.click();
            await assignment.SpecialtyWednesday.click();
            await assignment.SpecialtyThursday.click();
            await assignment.SpecialtyFriday.click();
            await assignment.SpecialtySaturday.click();
            await assignment.SpecialtyRangeOfrecurrence.click();
            await assignment.SpecialtyEndBy.click();
            await assignment.SpecialtyCalender.click();
            await browser.pause(4000);
            let CalenderMonth = await assignment.SpectaltyEndByDate

            for (let i = 0; i < CalenderMonth.length; i++) {
                const element = await CalenderMonth[i];
                if (await element.getText() == CalenderEndByDate) {
                    await element.click();
                    break;
                }

            }
            await assignment.SaveSpecialtyAssignment.click();
            await browser.pause(6000);

            //--------------------------------------------Update Specialty Assignment--------------------------------------------//

            await assignment.SpecialtyClickAssignment.click();
            await browser.pause(5000);
            await assignment.EditSpecialtyAssignment.click();
            await assignment.SpecialtyStartTime.click();
            await assignment.SpecialtyStartTimeInput.setValue("7.00 AM");
            await assignment.SpecialtyEndTime.click();
            await assignment.SpecialtyEndTimeInput.setValue("6.00 PM")
            await browser.pause(5000);
            await assignment.SpecialtyStartDate.click();
            await assignment.UpdateSpecialtyAssignment.click();
            await browser.pause(5000);

            //----------------------------------------Delete Multiple Assignments--------------------------------------------------------//
            await assignment.SpecialtyClickAssignment.click();
            await assignment.SpecialtyDeleteButton.click();
            await assignment.AllSubSequentAssignment.click();
            await browser.pause(5000);
            await assignment.DeleteSpecialtyAssignment.click();
            await browser.pause(10000);

        });
    }



});


